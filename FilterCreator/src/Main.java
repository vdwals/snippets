import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Class to manipulate SN3K.ini for better usage of SN3K while testing process
 * 
 * @author Dennis van der Wals
 * 
 */
public class Main {
	private static final String FILTER = "filter.txt", REGEX_ID = "ID:",
			REGEX_NUMBER = "\\d+", REGEX_SLASH = "/ ", REGEX_QCID = "\\d+.*";

	private static int SIZE = 600;

	private JFrame mainFrame;
	private JButton ok;
	private JTextArea idInputField;

	public static void main(String[] args) {
		Main m = new Main();
		m.go();
	}

	public Main() {
		getMainFrame();
	}

	public void go() {
		getMainFrame().setSize(SIZE, SIZE);
		getMainFrame().setVisible(true);
	}

	private JFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new JFrame();
			mainFrame.setLayout(new BorderLayout(10, 10));
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			JScrollPane p = new JScrollPane(getIdInputField());
			mainFrame.add(p, BorderLayout.CENTER);

			JPanel buttons = new JPanel(new GridLayout(1, 2, 5, 5));
			buttons.add(getOk());

			mainFrame.add(buttons, BorderLayout.SOUTH);
		}
		return mainFrame;
	}

	private JButton getOk() {
		if (ok == null) {
			ok = new JButton("Ok");
			ok.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						TextToFilter(getIdInputField().getText());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		return ok;
	}

	private JTextArea getIdInputField() {
		if (idInputField == null) {
			idInputField = new JTextArea(
					"QC ID:1067/ Spec:FL_NTG5.5_NAVI_A_Navigation/ Description: The system shall provide the ability to define a specific sub...\n"
							+ "QC ID:1068/ Spec:FL_NTG5.5_NAVI_A_Navigation/ Description: The system shall provide the ability to select Interactive 3D...\n"
							+ "QC ID:1071/ Spec:FL_NTG5.5_NAVI_A_Navigation/ Description: The system shall provide the ability to navigate to the selec...\n"
							+ "QC ID:1984/ Spec:FL_NTG5.5_NAVI_A_Navigation/ Description: The system shall accept the names of POIs, that are attached ...");
		}
		return idInputField;
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	private void TextToFilter(String text) throws IOException {
		Scanner scan = new Scanner(text);

		String filter = new String();

		String line;
		while (scan.hasNextLine()) {
			line = scan.nextLine();

			if (Pattern.matches(REGEX_NUMBER, line))
				filter += line + " Or ";

			else if (line.contains(REGEX_ID)) {
				String[] ids = line.split(REGEX_ID);

				for (int i = 0; i < ids.length; i++) {
					if (Pattern.matches(REGEX_QCID, ids[i])) {
						filter += ids[i].split(REGEX_SLASH)[0] + " Or ";
					}
				}
			}

		}
		scan.close();

		filter = filter.substring(0, filter.length() - 4);

		BufferedWriter out = new BufferedWriter(new FileWriter(FILTER));

		// saves language
		out.write(filter);
		out.close();

		getIdInputField().setText(filter);
		getIdInputField().requestFocus();
		getIdInputField().selectAll();

		StringSelection selection = new StringSelection(filter);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
		
	}

}
