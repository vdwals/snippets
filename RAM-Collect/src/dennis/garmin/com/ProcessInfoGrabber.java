package dennis.garmin.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Funktion zum Aufruf von tasklist.
 * 
 * @author vanderwals
 * 
 */
public class ProcessInfoGrabber {
	private static final String CMD = "cmd",
			C = "/c",
			TASK = "tasklist",
			F = "/fo",
			FORMAT = "\"CSV\"",
			PATTERN = "(\\w+.\\w+)(\\s+)(\\d+)(\\s+)(\\w+)(\\s+)(\\d)(\\s+)(\\d+(.\\d+)?) K";

	// "SN3K.exe                      4608 Console                    1       499.112 K"

	private List<String> command;

	/**
	 * Testfunktion
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out
				.println(Pattern
						.matches(
								PATTERN,
								"System Idle Bla                  4608 Console                    1       499.112 K"));

		ProcessInfoGrabber p = new ProcessInfoGrabber();

		try {
			System.out.println(p.getFilteredProcessInfo(4820));
			// for (ProcessInfo pi : p.getAllProcessInfo(null)) {
			// System.out.println(pi);
			// }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Erzeugt einen neuen Aufrufer
	 */
	public ProcessInfoGrabber() {
		command = new ArrayList<String>();
		command.add(CMD);
		command.add(C);
		command.add(TASK);
		command.add(F);
		command.add(FORMAT);
	}

	/**
	 * Erfragt eine Liste �ber alle ausgef�hrten Tasks
	 * 
	 * @param commands
	 *            Liste zus�tzlicher Befehle
	 * @return String der Ausgabe
	 * @throws IOException
	 */
	public ArrayList<ProcessInfo> getAllProcessInfo(ArrayList<String> commands)
			throws IOException {
		// Befehl laden
		ArrayList<String> befehl = new ArrayList<String>();
		befehl.addAll(command);

		// Filter anwenden
		if (commands != null)
			befehl.addAll(commands);

		// Prozess starten
		ProcessBuilder builder = new ProcessBuilder(befehl);

		final Process process = builder.start();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				process.getInputStream()));

		// Ausgabe abfangen
		ArrayList<ProcessInfo> processInfos = new ArrayList<>();
		String line;
		int counter = 0;
		while ((line = br.readLine()) != null) {
			// TODO Debug
			// System.out.println(line);

			// Ueberspringe die erste Zeilen
			if (counter >= 1)
				processInfos.add(new ProcessInfo(line));

			counter++;
		}
		// TODO Debug
		// System.out.println("Program terminated!");

		return processInfos;
	}

	/**
	 * Wendet einen ProzessID Filter an
	 * 
	 * @param pid
	 *            ProzessID
	 * @return String der Ausgabe
	 * @throws IOException
	 */
	public ProcessInfo getFilteredProcessInfo(int pid) throws IOException {
		ArrayList<String> befehle = new ArrayList<>();
		befehle.add("/fi");
		befehle.add("\"PID eq " + pid + "\"");

		ArrayList<ProcessInfo> filtered = getAllProcessInfo(befehle);
		if (filtered.isEmpty())
			return null;

		return (filtered.get(0));
	}
}
