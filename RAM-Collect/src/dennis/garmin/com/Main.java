package dennis.garmin.com;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;

//WMIC and powershell verwenden

public class Main extends JFrame implements ProcessWatchListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3453918269333253886L;
	private ArrayList<ProcessWatcher> pWatcher;
	private long start;

	public static void main(String[] args) {
		Main m = new Main();

		String[] test = { "SN3K" };

		m.doIt(test);
	}

	public Main() {
		pWatcher = new ArrayList<>();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public void doIt(String[] filter) {
		start = System.currentTimeMillis();
		for (String string : filter) {
			ProcessWatcher pw = new ProcessWatcher(string, 500, 5000);
			pw.addWatcher(this);
			pWatcher.add(pw);
			pw.doIt();
		}
	}

	public void stopIt() {
		for (ProcessWatcher pw : pWatcher) {
			pw.setRun(false);
		}
	}

	@Override
	public void sendProcessInfo(ProcessInfo p) {
		File log = new File(p.getName() + " " + p.getPid());
		try {
			log.createNewFile();

			FileWriter fw = new FileWriter(log, true);
			fw.write(p.getRam() + ";" + (System.currentTimeMillis() - start)
					+ System.lineSeparator());
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
