package dennis.garmin.com;

/**
 * Klasse zum speichern von Prozessinformationen.
 * 
 * @author vanderwals
 * 
 */
public class ProcessInfo {
	private static final String DELIMITER = "\",\"";
	// info.replaceAll(PATTERN, REPLACE), PATTERN = "(\\w)(\\s+)", REPLACE =
	// "$1;"

	private String name, session;
	private int pid, sessionNr, ram, cpu;

	/**
	 * Testfunktion
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String test = "SN3K.exe                      4608 Console                    1       499.112 K";
		test = "\"cmd.exe\",\"2100\",\"Console\",\"1\",\"3.876 K\"";

		ProcessInfo p = new ProcessInfo(test);

		System.out.println(p.getName());
		System.out.println(p.getPid());
		System.out.println(p.getRam());
		System.out.println(p.getSession());
		System.out.println(p.getSessionNr());
	}

	/**
	 * Erstellt eine neue Prozessinformation aus den uebergebenen Daten
	 * 
	 * @param name
	 *            Prozessname
	 * @param session
	 *            Prozesssitzung
	 * @param pid
	 *            ProzessID
	 * @param sessionNr
	 *            Sitzungsnummer
	 * @param ram
	 *            Arbeitsspeicher
	 */
	public ProcessInfo(String name, String session, int pid, int sessionNr,
			int ram) {
		this.name = name;
		this.session = session;
		this.pid = pid;
		this.sessionNr = sessionNr;
		this.ram = ram;
	}

	/**
	 * Erstellt eine neue Prozessinfo aus dem tasklist-string
	 * 
	 * @param info
	 *            tasklist Ausgabe
	 */
	public ProcessInfo(String info) {
		String[] infos = info.substring(1).split(DELIMITER);

		// TODO Debug
		// for (String string : infos) {
		// System.out.println(string);
		// }

		this.name = infos[0];
		this.pid = Integer.parseInt(infos[1]);
		this.session = infos[2];
		this.sessionNr = Integer.parseInt(infos[3]);
		this.ram = Integer.parseInt(infos[4].split(" ")[0]
				.replaceAll("\\.", ""));
	}

	public String getName() {
		return name;
	}

	public String getSession() {
		return session;
	}

	public int getPid() {
		return pid;
	}

	public int getSessionNr() {
		return sessionNr;
	}

	public int getRam() {
		return ram;
	}

	public String toString() {
		StringBuilder out = new StringBuilder();
		out.append("Name: " + this.name + System.lineSeparator());
		out.append("PID: " + this.pid + System.lineSeparator());
		out.append("Session: " + this.session + System.lineSeparator());
		out.append("Session-Nr: " + this.sessionNr + System.lineSeparator());
		out.append("RAM: " + this.ram + System.lineSeparator());
		out.append(System.lineSeparator());
		return out.toString();
	}

}
