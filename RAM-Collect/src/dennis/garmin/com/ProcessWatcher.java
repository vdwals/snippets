package dennis.garmin.com;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Ueberwacht einen Prozess eines bestimmten namens und gibt die Infos an die
 * watcher zur�ck.
 * 
 * @author vanderwals
 * 
 */
public class ProcessWatcher {
	private ProcessInfoGrabber pInfoGrabber;
	private String filterByName;
	private ArrayList<Integer> pids;
	private ArrayList<ProcessWatchListener> watcher;
	private boolean run;
	private int sleepReq, sleepPID;

	/**
	 * Testmethode
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		ProcessWatcher pw = new ProcessWatcher("SN3K", 500, 10000);

		pw.doIt();

		try {
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pw.setRun(false);
		System.out.println("STOP");
	}

	/**
	 * Erzeugt einen neuen �berwacher
	 * 
	 * @param filter
	 *            Name der Anwendung
	 */
	public ProcessWatcher(String filter, int sleepReq, int sleepPID) {
		pInfoGrabber = new ProcessInfoGrabber();
		filterByName = filter;
		watcher = new ArrayList<>();
		this.sleepReq = sleepReq;
		this.sleepPID = sleepPID;
		this.run = true;

		getPIDs(filter);
	}

	/**
	 * Fuegt einen neuen Beobachter hinzu, der benachrichtigt wird.
	 * 
	 * @param p
	 *            Beobachter
	 */
	public void addWatcher(ProcessWatchListener p) {
		watcher.add(p);
	}

	/**
	 * Liest die PIDs der Anwendungen aus.
	 * 
	 * @param filter
	 *            Anwendungsname
	 */
	private void getPIDs(String filter) {
		pids = new ArrayList<>();

		try {
			for (ProcessInfo pInfo : pInfoGrabber.getAllProcessInfo(null)) {
				if (pInfo.getName().contains(filter))
					pids.add(pInfo.getPid());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doIt() {
		Thread collectRam = new Thread(new Runnable() {

			@Override
			public void run() {
				while (run) {
					ArrayList<ProcessInfo> pInfos = new ArrayList<>();

					synchronized (pids) {
						for (Integer pid : pids) {
							try {
								ProcessInfo pInfo = pInfoGrabber
										.getFilteredProcessInfo(pid);

								if (pInfo != null)
									pInfos.add(pInfo);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}

					synchronized (watcher) {
						for (ProcessWatchListener beobachter : watcher) {
							for (ProcessInfo pInfo : pInfos) {
								beobachter.sendProcessInfo(pInfo);
							}
						}
					}

					// TODO Debug
					// System.out.println("RAM collected");

					try {
						Thread.sleep(sleepReq);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		collectRam.start();

		Thread collectPIDs = new Thread(new Runnable() {

			@Override
			public void run() {
				long start;
				while (run) {
					start = System.currentTimeMillis();
					getPIDs(filterByName);

					try {
						Thread.sleep(sleepPID
								- (System.currentTimeMillis() - start));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// TODO Debug
					// System.out.println("PIDs collected");
				}
			}
		});
		collectPIDs.start();
	}

	public void setRun(boolean run) {
		this.run = run;
	}

}
