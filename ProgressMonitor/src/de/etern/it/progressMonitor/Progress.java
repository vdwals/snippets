/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.progressMonitor;

import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class Progress {
	public int waitTime = 1000, timePerPercentIndex, percentages[];

	private JProgressBar progress;
	private long timeStarted, running, estimated, remaining, timePerPercent[];
	private JLabel timeRunning, timeEstimated, timeRemaining;
	private String titel;
	private LinkedList<Counter> counterList;

	private boolean started, indeterminate, finished, closed, changed, vertical;
	private boolean hour, minute, second;

	private JPanel panel;

	/**
	 * Creates new progress
	 *
	 * @param titel
	 *            titel
	 * @param indeterminate
	 *            determines, if the progressBar show continous animation
	 * @param runningTime
	 *            determines, if the runningTime should be shown
	 * @param estimatedTime
	 *            determines, if the estimatedTime should be shown
	 * @param remainingTime
	 *            determines, if the remainingTime should be shown
	 */
	protected Progress(String titel, boolean indeterminate, boolean runningTime, boolean estimatedTime,
			boolean remainingTime) {
		this.indeterminate = indeterminate;
		this.titel = titel;
		this.finished = false;
		this.started = false;
		this.closed = false;
		this.changed = false;

		this.counterList = new LinkedList<>();

		this.panel = new JPanel(new GridLayout(0, 1));
		this.panel.add(new JLabel(titel, SwingConstants.CENTER));

		this.progress = new JProgressBar(0, 100);
		this.progress.setIndeterminate(indeterminate);
		if (!indeterminate) {
			this.progress.setStringPainted(true);
		}
		this.panel.add(this.progress);

		if (runningTime) {
			this.timeRunning = new JLabel("", SwingConstants.RIGHT);
			this.panel.add(createPanel("Run: ", this.timeRunning));
		}

		if (estimatedTime && !indeterminate) {
			this.timeEstimated = new JLabel("", SwingConstants.RIGHT);
			this.panel.add(createPanel("Estimated: ", this.timeEstimated));
		}

		if (remainingTime && !indeterminate) {
			this.timeRemaining = new JLabel("", SwingConstants.RIGHT);
			this.panel.add(createPanel("Remaining: ", this.timeRemaining));
		}

		if (!indeterminate && (remainingTime || estimatedTime)) {
			this.timePerPercent = new long[101];
			this.percentages = new int[101];
		}
	}

	public void addValue(int value) {
		this.progress.setValue(this.progress.getValue() + value);
		updateTime(true);
	}

	/**
	 * Erzeugt einen neuen Zaehler
	 *
	 * @param label
	 *            Name des Zaehlers
	 * @param format
	 *            Ausgabeformat der Zahl
	 * @return Zaehler
	 */
	public Counter createCounter(String label, String format) {
		Counter c = new Counter(label, format);
		this.panel.add(c.getPanel());
		this.counterList.add(c);
		this.changed = true;
		return c;
	}

	/**
	 * Creates a new JPanel
	 *
	 * @param info
	 * @param component
	 * @return
	 */
	private JPanel createPanel(String info, JComponent component) {
		JPanel running = new JPanel(new GridLayout(1, 2, 5, 5));
		running.add(new JLabel(info));
		running.add(component);
		return running;
	}

	public void finish() {
		if (this.closed)
			return;

		if (!this.indeterminate) {
			setValue(this.progress.getMaximum());
		}

		System.out.println("-- Beende Prozess " + this.titel + " --");
		System.out.println("Ben�tigte Zeit: " + getTimeToString(System.currentTimeMillis() - this.timeStarted));
		for (Counter counter : this.counterList) {
			System.out.println(counter.toString());
		}
		System.out.println("-------------------------------------");

		this.finished = true;
		this.closed = true;
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return this.panel;
	}

	/**
	 * Returns a formated String for time measurement
	 *
	 * @param time
	 * @return
	 */
	private String getTimeToString(long time) {
		long hour = TimeUnit.MILLISECONDS.toHours(time);
		long minute = TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(hour);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(minute)
				- TimeUnit.HOURS.toSeconds(hour);
		long milliseconds = time - TimeUnit.SECONDS.toMillis(seconds) - TimeUnit.MINUTES.toMillis(minute)
				- TimeUnit.HOURS.toMillis(hour);

		StringBuilder timeString = new StringBuilder();

		if (hour > 0) {
			timeString.append(hour + " h, ");
			if (!this.hour) {
				this.changed = true;
				this.hour = true;
			}
		}

		if ((hour > 0) || (minute > 0)) {
			timeString.append(String.format("%02d min, ", minute));
			if (!this.minute) {
				this.changed = true;
				this.minute = true;
			}
		}

		if ((hour > 0) || (minute > 0) || (seconds > 0)) {
			timeString.append(String.format("%02d sec, ", seconds));
			if (!this.second) {
				this.changed = true;
				this.second = true;
			}
		}

		timeString.append(String.format("%03d", milliseconds));

		return timeString.toString();
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return this.titel;
	}

	public void increment() {
		this.setValue(this.progress.getValue() + 1);
	}

	/**
	 * @return the changed
	 */
	public boolean isChanged() {
		return this.changed;
	}

	public boolean isFinished() {
		return (this.finished && this.started) || (this.finished && this.indeterminate);
	}

	/**
	 * @return the horizontal
	 */
	public boolean isVertical() {
		return this.vertical;
	}

	public void setMax(int max) {
		this.progress.setMaximum(max);
	}

	public void setMin(int min) {
		this.progress.setMinimum(min);
	}

	public void setValue(int value) {
		this.progress.setValue(value);
		updateTime(true);
	}

	/**
	 * @param vertical
	 *            the horizontal to set
	 */
	public void setVertical(boolean vertical) {
		this.vertical = vertical;
		this.progress.setOrientation(JProgressBar.VERTICAL);
	}

	/**
	 * Marks progress as started
	 */
	public void start() {
		System.out.println("-- Starte Prozess " + this.titel + " --");
		this.started = true;
		this.timeStarted = System.currentTimeMillis();
	}

	/**
	 * Updates time measurement
	 */
	protected void updateTime(boolean all) {
		if (this.started) {
			this.running = System.currentTimeMillis() - this.timeStarted;

			if (this.timeRunning != null) {
				this.timeRunning.setText(getTimeToString(this.running));
			}

			if (!this.indeterminate && ((this.timeEstimated != null) || (this.timeRemaining != null))) {
				double progressDone = this.progress.getValue() - this.progress.getMinimum();

				if (progressDone > 0) {
					double progressTotal = this.progress.getMaximum() - this.progress.getMinimum();

					/* Zeit jeden Prozent speichern */
					int percent = (int) ((100 * progressDone) / progressTotal);
					int percentBefore = (int) ((100 * (progressDone - 1)) / progressTotal);

					double percentToDo = 100 - percent;

					if ((percent > percentBefore) && all && (percent < 100)) {
						this.timePerPercent[this.timePerPercentIndex] = this.running;
						this.percentages[this.timePerPercentIndex++] = percent;
					}

					if ((this.timeEstimated != null) && (all || (this.running >= this.estimated))) {
						if (this.timePerPercentIndex > 0) {
							double timePast = this.timePerPercent[this.timePerPercentIndex]
									- this.timePerPercent[this.timePerPercentIndex - 1];
							double percentagesPast = this.percentages[this.timePerPercentIndex]
									- this.percentages[this.timePerPercentIndex - 1];
							this.remaining = (long) ((timePast * percentToDo) / percentagesPast);
							this.estimated = this.running + this.remaining;

						} else {
							this.estimated = (long) (this.running * (progressTotal / progressDone));
						}

						this.timeEstimated.setText(getTimeToString(this.estimated));
					}

					if (this.timeRemaining != null) {
						if ((this.timeEstimated == null) && (this.timePerPercentIndex > 0)) {
							double timePast = this.timePerPercent[this.timePerPercentIndex]
									- this.timePerPercent[this.timePerPercentIndex - 1];
							double percentagesPast = this.percentages[this.timePerPercentIndex]
									- this.percentages[this.timePerPercentIndex - 1];
							this.remaining = (long) ((timePast * percentToDo) / percentagesPast);

						} else if (this.timeEstimated == null) {
							double progressToDo = this.progress.getMaximum() - this.progress.getValue();
							this.remaining = (long) (this.running * (progressToDo / progressDone));

						} else {
							this.remaining = this.estimated - this.running;
						}

						this.timeRemaining.setText(getTimeToString(this.remaining));
					}
				}
			}
		}
	}
}
