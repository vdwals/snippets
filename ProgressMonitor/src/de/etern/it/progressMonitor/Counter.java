/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.progressMonitor;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Counter {
	private int counter;
	private String format, label;
	private JLabel counterLabel;
	private JPanel panel;

	protected Counter(String label, String format) {
		this.counter = 0;
		this.counterLabel = new JLabel("", SwingConstants.RIGHT);

		this.panel = new JPanel(new GridLayout(1, 2, 5, 5));
		this.panel.add(new JLabel(label));
		this.panel.add(this.counterLabel);

		this.label = label;
		this.format = format;
	}

	public void addValue(int value) {
		this.setValue(value + this.counter);
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		return this.panel;
	}

	/**
	 * Gibt einen Wert zurueck, der das prozentuale Verhaeltnis zwischen diesem
	 * und einem anderem Counter anzeigt
	 * 
	 * @param c
	 * @return
	 */
	public int getPercentage(Counter c) {
		return (100 * this.counter) / c.counter;
	}

	/**
	 * Gibt einen String zurueck, der das prozentuale Verhaeltnis zwischen
	 * diesem und einem anderem Counter anzeigt
	 * 
	 * @param c
	 * @return
	 */
	public String getPercentageString(Counter c) {
		return this.label + "/" + c.label + ": " + getPercentage(c) + " %";
	}

	/**
	 * Gibt einen Wert zurueck, der diesen Counter im Verhaeltnis zu einem
	 * anderen hat.
	 * 
	 * @param c
	 * @return
	 */
	public double getRelative(Counter c) {
		return (double) this.counter / (double) c.counter;
	}

	/**
	 * Gibt einen String zurueck, der diesen Counter im Verhaeltnis zu einem
	 * anderen Anzeigt
	 * 
	 * @param c
	 * @return
	 */
	public String getRelativeString(Counter c) {
		return this.label + "/" + c.label + ": " + getRelative(c);
	}

	/**
	 * @return the counter
	 */
	public int getValue() {
		return this.counter;
	}

	public void increment() {
		this.counter++;
		if (this.format == null) {
			this.counterLabel.setText(this.counter + "");
		} else {
			this.counterLabel.setText(String.format(this.format, this.counter));
		}
	}

	public void setValue(int value) {
		this.counter = value;
		if (this.format == null) {
			this.counterLabel.setText(this.counter + "");
		} else {
			this.counterLabel.setText(String.format(this.format, this.counter));
		}
	}

	@Override
	public String toString() {
		return this.label + ": " + this.counterLabel.getText();
	}
}
