/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.progressMonitor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class CommonProgressMonitor {
	public static final CommonProgressMonitor INSTANCE = new CommonProgressMonitor();
	private Vector<Progress> progressList;
	private Thread monitoring;
	private boolean stop;

	private JPanel panel, panelVertical;

	private CommonProgressMonitor() {
		this.panel = new JPanel();
		this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));
		this.panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		this.panelVertical = new JPanel(new FlowLayout());
		this.panelVertical.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		this.progressList = new Vector<>();

		this.stop = false;

		this.monitoring = new Thread(() -> {

			for (int i = 0; i < this.progressList.size(); i++) {
				if (this.progressList.get(i).isVertical()) {
					this.panelVertical.add(this.progressList.get(i).getPanel());
				} else {
					this.panel.add(this.progressList.get(i).getPanel());
				}
			}

			int progresses = this.progressList.size();

			boolean changesMade = false;

			while (!this.stop) {
				/* Entferne beendete Prozesse */
				for (int i = 0; i < this.progressList.size(); i++) {
					/* Aktuallisiere Zeitanzeige */
					this.progressList.get(i).updateTime(false);

					if (this.progressList.get(i).isFinished()) {
						this.progressList.remove(i);
						i--;
						changesMade = true;

					} else if (!changesMade) {
						changesMade = this.progressList.get(i).isChanged();
					}
				}
				/*
				 * Pr�fe, ob progresse entfernt oder neue hinzugef�gt werden
				 * m�ssen
				 */
				if (this.progressList.size() != progresses) {
					this.panel.removeAll();
					this.panelVertical.removeAll();

					for (int i = 0; i < this.progressList.size(); i++) {
						if (this.progressList.get(i).isVertical()) {
							this.panelVertical.add(this.progressList.get(i).getPanel());
						} else {
							this.panel.add(this.progressList.get(i).getPanel());
						}
					}
					changesMade = true;
					progresses = this.progressList.size();
				}

				/* Neu zeichnen wenn n�tig */
				if (changesMade) {
					SwingUtilities.invokeLater(() -> {
						this.panel.repaint();
						this.panel.validate();
					});
					changesMade = false;
				}

				/* Warte */
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			this.progressList.forEach(de.etern.it.progressMonitor.Progress::finish);
			this.progressList.clear();
		});
	}

	/**
	 * Erzeugt einen neuen Prozess
	 *
	 * @param titel
	 *            Name des Prozesses
	 * @param indeterminate
	 *            Durchlaufender Prozess
	 * @param runningTime
	 *            Soll Laufzeit angezeigt werden
	 * @param estimatedTime
	 *            soll gesch�tzte Dauer angezeigt werden
	 * @param remainingTime
	 *            soll gesch�tzte restzeit angezeigt werden
	 * @return Progress.
	 */
	public Progress createProgress(String titel, boolean indeterminate, boolean runningTime, boolean estimatedTime,
			boolean remainingTime) {
		Progress p = new Progress(titel, indeterminate, runningTime, estimatedTime, remainingTime);

		this.progressList.add(p);

		return p;
	}

	/**
	 * @return the panel
	 */
	public JPanel getPanel() {
		JPanel p = new JPanel(new BorderLayout(5, 5));
		p.add(this.panel, BorderLayout.NORTH);

		JPanel a = new JPanel(new BorderLayout(5, 5));
		a.add(p, BorderLayout.CENTER);
		a.add(this.panelVertical, BorderLayout.WEST);

		return a;
	}

	public void start() {
		this.monitoring.start();
	}

	public void stop() {
		this.stop = true;

		try {
			this.monitoring.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
