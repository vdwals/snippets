ProgressMonitor
=========

A very useful snippet I used in several different Projects. You can watch the progress and different counters or statistical values by simply create a Monitor with child processes or counters. It can be used in any Swing related GUI. 
