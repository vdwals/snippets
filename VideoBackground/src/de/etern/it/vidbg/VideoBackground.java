/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.vidbg;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Created by Dennis on 20.05.2015.
 */
public class VideoBackground extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		File dir = new File("Videobackground/blueten_frames");

		// Frames einlesen
		File[] framesFiles = dir.listFiles(pathname -> {
			return pathname.getName().endsWith(".jpg");
		});

		// final JProgressBar progressBar = new JProgressBar();
		// progressBar.setVisible(false);
		// final IIOReadProgressListener progressListener = new
		// IIOReadProgressListener() {
		// public void imageStarted(ImageReader source, int imageIndex) {
		// SwingUtilities.invokeLater(new Runnable() {
		// public void run() {
		// System.out.println(source);
		// progressBar.setValue(0);
		// progressBar.setVisible(true);
		// }
		// });
		// }
		//
		// public void imageProgress(ImageReader source,
		// final float percentageDone) {
		// SwingUtilities.invokeLater(new Runnable() {
		// public void run() {
		// System.out.println("percentageDone" + percentageDone);
		// progressBar.setValue((int) percentageDone);
		// }
		// });
		// }
		//
		// public void imageComplete(ImageReader source) {
		// SwingUtilities.invokeLater(new Runnable() {
		// public void run() {
		// progressBar.setValue(100);
		// progressBar.setVisible(false);
		// }
		// });
		// }
		//
		// public void sequenceStarted(ImageReader source, int minIndex) {
		// }
		//
		// public void sequenceComplete(ImageReader source) {
		// }
		//
		// public void thumbnailStarted(ImageReader source, int imageIndex,
		// int thumbnailIndex) {
		// }
		//
		// public void thumbnailProgress(ImageReader source,
		// float percentageDone) {
		// }
		//
		// public void thumbnailComplete(ImageReader source) {
		// }
		//
		// public void readAborted(ImageReader source) {
		// }
		// };

		// BufferedImage[] frames = new BufferedImage[framesFiles.length];
		// try {
		// URL url = new URL(dir.getAbsolutePath());
		// ImageInputStream iis = ImageIO.createImageInputStream(url
		// .openStream());
		// Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
		// int indexOfFrame = 0;
		//
		// if (iter.hasNext()) {
		// ImageReader reader = (ImageReader) iter.next();
		// reader.setInput(iis);
		// reader.addIIOReadProgressListener(progressListener);
		// frames[indexOfFrame++] = reader.read(reader.getMinIndex());
		//
		// }
		// } catch (MalformedURLException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

		BufferedImage[] frames = new BufferedImage[framesFiles.length];
		for (int indexOfFrame = 0; indexOfFrame < frames.length; indexOfFrame++) {
			try {
				frames[indexOfFrame] = ImageIO.read(framesFiles[indexOfFrame]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		JFrame frame = new JFrame();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		VideoBackground v = new VideoBackground(frames);

		frame.add(v);

		v.animation();
	}

	private BufferedImage[] frames;

	private int actualFrame = 0;

	public VideoBackground(BufferedImage[] frames) {
		this.frames = frames;
	}

	public void animation() {

		new Thread(() -> {
			while (true) {
				this.actualFrame++;

				if (this.actualFrame == this.frames.length) {
					this.actualFrame = 0;
				}

				System.out.println(this.actualFrame);

				try {
					SwingUtilities.invokeAndWait(() -> repaint());
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}

				try {
					Thread.sleep(1000 / 30);
				} catch (InterruptedException e) {

				}
			}
		}).start();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(this.frames[this.actualFrame], 0, 0, null);
	}
}
