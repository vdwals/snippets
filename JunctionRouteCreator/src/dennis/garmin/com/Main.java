package dennis.garmin.com;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Main {
	private static int SIZE = 600;
	private static int LATITUDE = 0, LONGITUDE = 1;

	private JFrame mainFrame;
	private JButton latLong, ok, convertFile;
	private boolean bLatLong;
	private JTextArea coordinates;

	public static void main(String[] args) {
		Main m = new Main();
		m.go();
	}

	public Main() {
		bLatLong = true;
		getMainFrame();
	}

	public void go() {
		getMainFrame().setSize(SIZE, SIZE);
		getMainFrame().setVisible(true);
	}

	private JFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new JFrame();
			mainFrame.setLayout(new BorderLayout(10, 10));
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			mainFrame.add(getLatLong(), BorderLayout.NORTH);

			JScrollPane p = new JScrollPane(getCoordinates());
			mainFrame.add(p, BorderLayout.CENTER);

			JPanel buttons = new JPanel(new GridLayout(1, 2, 5, 5));
			buttons.add(getOk());
			buttons.add(getConvertFile());

			mainFrame.add(buttons, BorderLayout.SOUTH);
		}
		return mainFrame;
	}

	private JButton getLatLong() {
		if (latLong == null) {
			latLong = new JButton("Lat-Long");
			latLong.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (bLatLong) {
						latLong.setText("Long-Lat");
						System.out.println("11.609910 48.144427");
					} else {
						latLong.setText("Lat-Long");
						System.out.println("48.144427 11.609910");
					}

					bLatLong = !bLatLong;
				}
			});
		}
		return latLong;
	}

	private JButton getOk() {
		if (ok == null) {
			ok = new JButton("Ok");
			ok.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						createRoutesFromField(getCoordinates().getText());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		return ok;
	}

	private JButton getConvertFile() {
		if (convertFile == null) {
			convertFile = new JButton("Convert File");
			convertFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						createRoutesFromFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			convertFile.setEnabled(false);
		}
		return convertFile;
	}

	private JTextArea getCoordinates() {
		if (coordinates == null) {
			coordinates = new JTextArea(
					"51.426905,7.526067; 51.437285,7.521189; 51.441901,7.535423; 51.428768,7.541539");
		}
		return coordinates;
	}

	private void createRoutesFromField(String coordinates) throws IOException {
		int counter = 1;
		Scanner scan = new Scanner(coordinates);

		while (scan.hasNextLine()) {
			// Zeilenweise auslesen und Routennamen erstellen
			String line = scan.nextLine();

			if (lineToRoute(line, "", "Route - " + counter))
				counter++;
		}
		scan.close();
	}

	private void createRoutesFromFile() throws IOException {
		// Datei �ffnen
		JFileChooser chooser = new JFileChooser();
		// Dialog zum Oeffnen von Dateien anzeigen
		int r = chooser.showOpenDialog(mainFrame);

		if (r == JFileChooser.APPROVE_OPTION) {
			File input = chooser.getSelectedFile();

			Scanner scan = new Scanner(input);

			String name = "";
			String line = "";
			String output = "";

			while (scan.hasNextLine()) {
				line = scan.nextLine();
				String tmp = "";

				if (line.contains("<td>")) {
					tmp = line.substring(line.indexOf("<td>") + 4,
							line.indexOf("</td>"));

					if (tmp.contains(" - ") && !tmp.contains("href"))
						// Name auslesen
						name = tmp;

					else if (tmp.contains("href")) {
						// Link aendern
						int index = line.indexOf("\"") + 1;
						String replace = line.substring(index,
								line.indexOf("\"", index));
						line = line.replace(replace, name + ".txt");
					}

					else if (tmp.contains("; "))
						// Route auslesen
						lineToRoute(
								tmp,
								input.getAbsolutePath().replace(
										input.getName(), ""), name);
				}
				output += line + System.lineSeparator();
			}
			scan.close();

			BufferedWriter bw = new BufferedWriter(new FileWriter(input));
			bw.write(output);
			bw.close();
		}
	}

	private boolean lineToRoute(String line, String path, String routeName)
			throws IOException {
		String[] waypoints = line.split("; ");

		if (waypoints.length >= 4) {
			// Export Waypoints
			// [waypoint][0: latitude; 1: longitude]
			String[][] coordinatesOfWaypoints = new String[waypoints.length][2];

			for (int i = 0; i < waypoints.length; i++) {
				String[] coordsPerPoint = new String[2];
				if (waypoints[i].contains(","))
					coordsPerPoint = waypoints[i].split(",");
				else
					coordsPerPoint = waypoints[i].split(" ");

				if (!bLatLong) {
					coordinatesOfWaypoints[i][LATITUDE] = coordsPerPoint[LATITUDE];
					coordinatesOfWaypoints[i][LONGITUDE] = coordsPerPoint[LONGITUDE];

				} else {
					coordinatesOfWaypoints[i][LATITUDE] = coordsPerPoint[LONGITUDE];
					coordinatesOfWaypoints[i][LONGITUDE] = coordsPerPoint[LATITUDE];
				}

			}

			// Routen erstellen
			for (int i = 0; i < coordinatesOfWaypoints.length - 1; i++) {
				for (int j = i + 1; j < coordinatesOfWaypoints.length; j++) {
					// Hin und rueckfahrt
					int start = i;
					int end = j;
					for (int k = 0; k < 2; k++) {
						String output = "";

						output += coordinatesOfWaypoints[start][LATITUDE]
								+ System.lineSeparator()
								+ coordinatesOfWaypoints[start][LONGITUDE]
								+ System.lineSeparator()
								+ coordinatesOfWaypoints[end][LATITUDE]
								+ System.lineSeparator()
								+ coordinatesOfWaypoints[end][LONGITUDE]
								+ System.lineSeparator();

						System.out.println(path + routeName + " " + start + "-"
								+ end + ".txt");

						File route = new File(path + routeName + " " + start
								+ "-" + end + ".txt");
						if (!route.exists())
							route.createNewFile();

						BufferedWriter bw = new BufferedWriter(new FileWriter(
								route));
						bw.write(output);
						bw.close();

						start = j;
						end = i;
					}
				}
			}

			return true;
		}
		return false;
	}
}
