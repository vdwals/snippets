package de.etern.it.editImage;

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {

	public static void main(String[] args) throws IOException {
		String path = "";
		String fileName = "baggage";
		String extension = "png";
		String number = "?";

		File out = new File("baggage_icon.png");
		out.createNewFile();

		// Create only new image, if old one does not exist yet.
		BufferedImage inputPicture = ImageIO.read(new File(path + fileName + "." + extension));
		Graphics2D inputEdit = inputPicture.createGraphics();
		Font font = inputEdit.getFont().deriveFont(inputEdit.getFont().getSize() * 0.8F);
		inputEdit.setFont(font);
		Rectangle2D stringBounds = inputEdit.getFontMetrics().getStringBounds(number, inputEdit);

		// Calculate dimensions
		int xCenter = inputPicture.getWidth() / 2;
		int yCenter = inputPicture.getHeight() / 2;
		int radius = (int) Math.max(stringBounds.getHeight(), stringBounds.getWidth()) + 1;

		if ((radius % 2) == (((int) stringBounds.getWidth()) % 2)) {
			radius++;
		}

		// Calculate dimension of new image
		int nWidth = (inputPicture.getWidth() + radius + 1) - xCenter;
		int nHeight = (inputPicture.getHeight() + radius + 1) - yCenter;

		// Create new Image
		BufferedImage dimg = new BufferedImage(nWidth, nHeight, BufferedImage.TYPE_INT_ARGB);

		// Edit new Image
		Graphics2D createGraphics = dimg.createGraphics();

		// Add original icon
		createGraphics.drawImage(inputPicture, 0, 0, null);

		// draw white oval with border
		createGraphics.setColor(Color.WHITE);
		createGraphics.fillOval(xCenter, yCenter, radius, radius);
		// Border color equal font color
		createGraphics.setColor(new Color(inputPicture.getRGB(xCenter, yCenter)));
		createGraphics.drawOval(xCenter, yCenter, radius, radius);

		// Write number in circle
		createGraphics.setFont(font);
		xCenter = (int) (xCenter + 1 + ((radius - stringBounds.getWidth()) / 2));
		yCenter += radius - (radius / 4);
		createGraphics.drawString(number, xCenter, yCenter);

		// finish picture
		createGraphics.dispose();
		ImageIO.write(dimg, extension, out);
	}

	public static BufferedImage resize(BufferedImage img, int newW, int newH) {
		Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return dimg;
	}
}
