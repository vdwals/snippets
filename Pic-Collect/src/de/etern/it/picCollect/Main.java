package de.etern.it.picCollect;

/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Dennis on 27.06.2016.
 */
public class Main {

	public static void main(String[] args) {
		File in = new File("new 1.txt");

		System.out.println(in.getAbsolutePath());

		List<String> images = new LinkedList<>();
		List<String> names = new LinkedList<>();

		try {
			Scanner scan = new Scanner(in);
			scan.useDelimiter("zoom button-photo-display");

			scan.next();

			while (scan.hasNext()) {
				String imageContainer = scan.next();

				String image = imageContainer.substring(imageContainer.indexOf("href=\"") + "href=\"".length());
				image = image.substring(0, image.indexOf("?"));

				String name = imageContainer.substring(imageContainer.indexOf("alt=\"") + "alt=\"".length());
				name = name.substring(0, name.indexOf("\""));

				System.out.println(name + " " + image);

				images.add(image);
				names.add(name);

			}

			scan.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		File out = new File("bilder.crawljob");

		try {
			out.createNewFile();

			BufferedWriter bw = new BufferedWriter(new FileWriter(out));

			for (int i = 0; i < images.size(); i++) {
				bw.write("text=" + images.get(i));
				bw.newLine();
				bw.write("packageName=Bilder");
				bw.newLine();
				bw.write("filename=" + names.get(i) + ".jpg");
				bw.newLine();
				bw.newLine();
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
