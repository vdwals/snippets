import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class WKT2TIKZ {
	private static final String TIKZ = "\\filldraw[fill=black!30, draw=black]",
			TIKZ_WHITE = "\\filldraw[fill=white, draw=black]";

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		File in = new File("polys.txt");

		int minusX = 6128450 - 5000;
		int minusY = 49610010;

		int divide = 10;

		if (!in.createNewFile()) {
			Scanner fileScanner = new Scanner(in);

			while (fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();

				if (line.contains("POLYGON")) {
					line = line.replaceAll("POLYGON \\(\\(", "").replaceAll(
							"\\)\\)", "");

					if (!line.contains(")")) {
						String tikz = TIKZ + " ";
						String coords[] = line.split(", ");

						for (int i = 0; i < coords.length - 1; i++) {
							String subCoords[] = coords[i].split(" ");
							int x = (Integer.parseInt(subCoords[0]) - minusX)
									/ divide;
							int y = (Integer.parseInt(subCoords[1]) - minusY)
									/ divide;

							tikz += "(" + x + "," + y + ")--";
						}

						tikz = tikz.substring(0, tikz.length() - 2) + ";";

						System.out.println(tikz);

					} else {
						String[] polys = line.split("\\),");

						for (int i = 0; i < polys.length; i++) {
							String tikz = TIKZ + " ";
							if (i != 0)
								tikz = TIKZ_WHITE + " ";

							String coords[] = polys[i].replaceAll(" \\(", "")
									.split(", ");

							for (int j = 0; j < coords.length - 1; j++) {
								String subCoords[] = coords[j].split(" ");
								int x = (Integer.parseInt(subCoords[0]) - minusX)
										/ divide;
								int y = (Integer.parseInt(subCoords[1]) - minusY)
										/ divide;

								tikz += "(" + x + "," + y + ")--";
							}

							tikz = tikz.substring(0, tikz.length() - 2) + ";";

							System.out.println(tikz);
						}
					}
				}
			}

			fileScanner.close();
		}
	}
}
