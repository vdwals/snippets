/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.console;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Abstrakte Klasse zur Konsolenausgabe in einer graphischen Oberflaeche.
 *
 * @author Dennis van der Wals
 */
public class Console extends JPanel {
	/**
	 * Logdatei
	 */
	private static File log = new File("log.txt");

	/**
	 * Serial-ID
	 */
	private static final long serialVersionUID = -8209459048334706652L;

	private static Console INSTANCE = new Console();

	/**
	 * Gibt die einzige Instanz der de.etern.it.console.Console zurueck.
	 *
	 * @return de.etern.it.console.Console
	 */
	public static Console getInstance() {
		return INSTANCE;
	}

	/**
	 * Schreibt den String s in die Logdatei.
	 *
	 * @param s
	 *            String
	 */
	public static void writeLog(String s) {
		try {

			log.createNewFile();
			FileWriter fw = new FileWriter(log, true);
			fw.append(s);
			fw.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public boolean printLog;

	public boolean printTimeStamp;

	/* GUI-Komponente */
	private JTextArea output;

	/**
	 * Erzeugt eine neue de.etern.it.console.Console mit leerem Textfeld
	 */
	private Console() {
		this.output = new JTextArea();
		this.output.setLineWrap(true);
		this.output.setEditable(false);
	}

	/**
	 * @return the output
	 */
	public JTextArea getOutput() {
		return this.output;
	}

	/**
	 * Schreibt einen String in die Ausgabe der Oberflaeche.
	 *
	 * @param ausgabe
	 *            Auszugebender Text
	 */
	public void write(String ausgabe) {
		// Erstellt den Ausgabestring.
		String s = ausgabe + System.lineSeparator();

		if (this.printTimeStamp) {
			s = "(" + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()) + "): " + s;
		}

		// Schreibt ihn in die Ausgabe
		this.output.append(s);

		// Schreibt ihn in die Logdatei
		if (this.printLog) {
			writeLog(s);
		}

		// Setzt den Textzeiger auf die neue Zeile
		this.output.setCaretPosition(this.output.getText().length());
	}

}
