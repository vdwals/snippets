package dvdw.assft;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * Class to manipulate SN3K.ini for better usage of SN3K while testing process
 * 
 * @author Dennis van der Wals
 * 
 */
public class Main {
	private static final String LOCATIONS_FOLDER = "//shares/qs/Public/Software/Gemini/",
			LOCATIONS_FILE = "locations.txt",
			SETTINGS_FILE = "assft.ini",
			SETTINGS_FOLDER = "//shares/homedir/",
			SN3K_INI = "SN3000.ini",
			SN3K_EXE = "SN3000.exe",
			LOCATIONS_KEY = "MyLocations=",
			LOCATIONS_SECTION = "[MapView]";
	private LinkedList<Setting> settings;
	private ArrayList<String> content;
	String lang;

	/**
	 * Standardconstructor
	 * 
	 * @throws IOException
	 */
	public Main() throws IOException {
		this.lang = loadSettings();

		// read Settings.ini
		BufferedReader in = new BufferedReader(new FileReader(SN3K_INI));

		content = new ArrayList<String>();
		String str;
		while ((str = in.readLine()) != null)
			content.add(str);
		in.close();

		// start gui
		new GUI(this, lang);
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		/* check or create source files */
		File check = new File(LOCATIONS_FOLDER);
		if (!check.exists()) {
			System.out.println("Unterverzeichnisse erstellt.");
			check.mkdirs();
		}

		check = new File(LOCATIONS_FOLDER + LOCATIONS_FILE);
		if (!check.exists()) {
			System.out.println("Datei f�r Orte erstellt.");
			check.createNewFile();
		}

		check = new File(SETTINGS_FOLDER + System.getProperty("user.name")
				+ "/" + SETTINGS_FILE);
		if (!check.exists()) {
			System.out.println("Einstellungsdatei erstellt.");
			check.createNewFile();
		}

		check = new File(SN3K_INI);
		if (!check.exists()) {
			if (!(new File(SN3K_EXE).exists())) {
				JOptionPane
						.showMessageDialog(
								null,
								"Bitte kopiere diese Datei in das Verzeichnis des SN3K mit der SN3K.exe",
								"Dateien nicht gefunden",
								JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
			(new File(SN3K_INI)).createNewFile();
		}

		// Create GUI
		new Main();
	}

	/**
	 * Reads settings from personal settings file
	 * 
	 * @throws IOException
	 */
	private String loadSettings() throws IOException {
		// read user Settings
		this.settings = new LinkedList<Setting>();
		String lang = "de";
		BufferedReader br = new BufferedReader(new FileReader(SETTINGS_FOLDER
				+ System.getProperty("user.name") + "/" + SETTINGS_FILE));

		String line;
		while ((line = br.readLine()) != null) {
			String[] settings = line.split("|");

			if (settings.length == 4) {
				// reads all settings
				this.settings.add(new Setting(settings[0], settings[1],
						settings[2], settings[3]));

			} else if (line.contains("lang")) {
				// reads language configuration
				lang = line.split("=")[1];
			}
		}
		br.close();

		return lang;
	}

	/**
	 * saves all settings
	 * 
	 * @throws IOException
	 */
	void saveSettings() throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(SETTINGS_FOLDER
				+ System.getProperty("user.name") + "/" + SETTINGS_FILE));

		// saves language
		out.write("lang=" + this.lang + System.lineSeparator());

		// saves all settings
		for (Setting setting : this.settings)
			out.write(setting.getSection() + "|" + setting.getKey() + "|"
					+ setting.getValue() + System.lineSeparator());

		out.close();
	}

	/**
	 * Setzt alle Settings
	 * 
	 * @param content
	 *            Inhalt von Settings.ini
	 * @param defaultSettings
	 *            Switches between new or default values written to ini
	 * @throws IOException
	 */
	void setSettings(ArrayList<String> content, boolean defaultSettings)
			throws IOException {
		boolean sectionReached;
		for (int j = 0; j < settings.size(); j++) {
			String newSetting = settings.get(j).getKey()
					+ (defaultSettings ? settings.get(j).getDefaultValue()
							: settings.get(j).getValue());
			sectionReached = false;

			for (int i = 0; i < content.size(); i++) {
				// Finde sectionsanfang
				if (!sectionReached
						&& content.get(i)
								.contains(settings.get(j).getSection()))
					sectionReached = true;

				else if (sectionReached) {
					if (content.get(i).contains(settings.get(j).getKey())) {
						// Ersetze vorhandene Ortsliste
						content.set(i, newSetting);
						break;
					}

					if (content.get(i).isEmpty()) {
						/*
						 * Wenn die Section verlassen wird und kein Schluessel
						 * gefunden, muss dieser angefuegt werden
						 */
						content.add(i, newSetting);
						break;
					}
				}
			}

			if (!sectionReached) {
				content.add("");
				content.add(settings.get(j).getSection());
				content.add(newSetting);
			}
		}

		// ver�ndern und schreiben
		BufferedWriter out = new BufferedWriter(new FileWriter(SN3K_INI));
		for (String line : content)
			out.write(line + System.lineSeparator());

		out.close();
	}

	/**
	 * Setzt die eingestellten Locations
	 * 
	 * @param content
	 *            Inhalt der Settings.ini
	 * @throws IOException
	 */
	void setLocations(ArrayList<String> content) throws IOException {
		ArrayList<String[]> locations = new ArrayList<>();

		// lade locations aus datei
		File source = new File(LOCATIONS_FOLDER + LOCATIONS_FILE);
		Scanner scan = new Scanner(source);
		while (scan.hasNextLine()) {
			String[] location = new String[3];
			String[] tmp = scan.nextLine().split("=");
			if (tmp.length == 2) {
				location[0] = tmp[0];
				location[1] = tmp[1].split(",")[1];
				location[2] = tmp[1].split(",")[0];
				locations.add(location);
			}
		}
		scan.close();

		String extra = LOCATIONS_KEY;
		for (String[] location : locations) {
			// F�ge alle Orte an
			for (int j = 0; j < location.length; j++) {
				// Umlaute austauschen
				extra += location[j] + "|";
			}
		}
		extra = extra.substring(0, extra.length() - 1);

		boolean sectionReached = false;
		for (int i = 0; i < content.size(); i++) {
			// Finde sectionsanfang
			if (!sectionReached && content.get(i).contains(LOCATIONS_SECTION))
				sectionReached = true;

			else if (sectionReached) {
				if (content.get(i).contains(LOCATIONS_KEY)) {
					// Ersetze vorhandene Ortsliste
					content.set(i, extra);
					break;
				}

				if (content.get(i).isEmpty()) {
					/*
					 * Wenn die Section verlassen wird und kein Schluessel
					 * gefunden, muss dieser angefuegt werden
					 */
					content.add(i, extra);
					break;
				}
			}
		}

		if (!sectionReached) {
			content.add("");
			content.add(LOCATIONS_SECTION);
			content.add(extra);
		}
	}
}
