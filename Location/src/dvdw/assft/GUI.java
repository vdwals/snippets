/**
 * 
 */
package dvdw.assft;

import javax.swing.JFrame;

/**
 * Graphical User Interface
 * 
 * @author Dennis van der Wals
 * 
 */
public class GUI extends JFrame {
	/**
	 * Default Serial-ID
	 */
	private static final long serialVersionUID = 1L;
	private Main mainClass;
	private String langFile;

	public GUI(Main mainClass, String language) {
		this.mainClass = mainClass;
	}

}
