package dvdw.assft;

/**
 * Klasse zum Speichern der Settings
 * 
 * @author vanderwals
 * 
 */
public class Setting {
	private String section, key, value, defaultValue;

	/**
	 * Erstellt ein neues Setting
	 * 
	 * @param section
	 *            Section des Settings
	 * @param name
	 *            name des Settings
	 * @param value
	 *            Wert des Settings
	 */
	public Setting(String section, String name, String value,
			String defaultValue) {
		this.section = section;
		this.key = name;
		this.value = value;
		this.defaultValue = defaultValue;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSection() {
		return section;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

}
