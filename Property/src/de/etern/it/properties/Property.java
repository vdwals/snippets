/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.properties;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.CodeSource;
import java.util.LinkedList;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.SwingConstants;

/**
 * Klasse zum Zugriff auf Sprach- und Einstellungsdateien.
 *
 * @author Dennis van der Wals
 */
public class Property {
	public static String PACKET = "de.etern.it.jserienkollar";//$NON-NLS-1$
	private final static String FILE_PREFIX = "messages_", //$NON-NLS-1$
			DEFAULT_LANG = "Deutsch", //$NON-NLS-1$
			CONFIG = "config.ini"; //$NON-NLS-1$
	public static final String FONT_STYLE_PLAIN = "PLAIN", //$NON-NLS-1$
			FONT_STYLE_BOLD = "BOLD", FONT_STYLE_ITALIC = "ITALIC", //$NON-NLS-1$ //$NON-NLS-2$
			FONT_STYLE_BOLD_ITALIC = "BOLD+ITALIC", //$NON-NLS-1$
			ALIGNMENT_CENTER = "CENTER", ALGINMENT_LEFT = "LEFT", //$NON-NLS-1$ //$NON-NLS-2$
			ALIGNMENT_RIGHT = "RIGHT"; //$NON-NLS-1$
	public static boolean RESOURCE = false;
	private static Properties prop = loadProperties();
	public static String language;

	/**
	 * Intepretiert eine Stringeingabe eines Alignments
	 *
	 * @param alignment
	 *            String mit alignment (left, right, center)
	 * @return SwingConstant des entprechenden Alignments
	 * @throws IllegalArgumentException
	 *             Wenn String nicht interpretiert werden konnte
	 */
	public static int decodeAlignment(String alignment) throws IllegalArgumentException {
		switch (alignment.toUpperCase()) {
		case ALGINMENT_LEFT:
			return SwingConstants.LEFT;

		case ALIGNMENT_CENTER:
			return SwingConstants.CENTER;

		case ALIGNMENT_RIGHT:
			return SwingConstants.RIGHT;

		default:
			throw new IllegalArgumentException("Alignment could not be read.");
		}

	}

	/**
	 * Decodiert einen Fontstil, unabhaengig von der Buchstabengr??e
	 *
	 * @param fontstyle
	 *            (BOLD, PLAIN, ITALIC, BOLD+ITALIC)
	 * @return Font.* Je nach eingabe
	 * @throws IllegalArgumentException
	 *             wird geworfen, wenn kein String zutrifft.
	 */
	public static int decodeFontStyle(String fontstyle) throws IllegalArgumentException {
		switch (fontstyle.toUpperCase()) {
		case FONT_STYLE_PLAIN:
			return Font.PLAIN;

		case FONT_STYLE_BOLD:
			return Font.BOLD;

		case FONT_STYLE_ITALIC:
			return Font.ITALIC;

		case FONT_STYLE_BOLD_ITALIC:
			return Font.BOLD + Font.ITALIC;

		default:
			throw new IllegalArgumentException("Font style could not be read.");
		}
	}

	/**
	 * Liest ein Alginment aus der Konfiguration aus
	 *
	 * @param key
	 * @return Alignment oder left als Swingconstant
	 */
	public static int getAlignment(String key) {
		String alignment = getConfig(key);
		try {
			return decodeAlignment(alignment);
		} catch (Exception e) {
			return SwingConstants.LEFT;
		}
	}

	/**
	 * Laedt einen Wahrheitswert aus der Konfigurationsdatei
	 *
	 * @param key
	 *            Schluessel
	 * @return Wahrheitswert
	 */
	public static boolean getBoolean(String key) {
		String config = getConfig(key);
		return !config.contains("!") && getConfig(key).equals("1");
	}

	/**
	 * Decodes a Color from the config file
	 *
	 * @param key
	 *            Color as String
	 * @return decoded Color
	 */
	public static Color getColor(String key) {
		String colorString = getConfig(key);

		if (colorString == null) {
			System.out.println("Farbe nicht gefunden: " + key);
			return Color.WHITE;
		}

		if (colorString.contains(",")) {
			String[] colors = colorString.split(",");
			if (colors.length == 3)
				return new Color(Integer.parseInt(colors[0]), Integer.parseInt(colors[1]), Integer.parseInt(colors[2]));

		} else if (colorString.contains("#"))
			return new Color(Integer.parseInt(colorString.replace("#", ""), 16));
		return null;
	}

	/**
	 * Laedt den String zum Schluessel aus der Standarddatei.
	 *
	 * @param key
	 *            Schluessel
	 * @return Wert zum Schluessel
	 */
	public static String getConfig(String key) {
		String config = prop.getProperty(key);
		if (config == null) {
			config = "!" + key + "!";
		}
		return config;
	}

	/**
	 * Laedt eine Zahl aus der Konfigurationsdatei.
	 *
	 * @param key
	 *            Schluessel
	 * @return Zahl
	 */
	public static double getDouble(String key) {
		return Double.valueOf(getConfig(key));
	}

	/**
	 * Laedt einen Schriftstil aus der Konfigurationsdatei
	 *
	 * @param key
	 *            Schluessel
	 * @return Wahrheitswert
	 */
	public static int getFontStyle(String key) {
		try {
			return decodeFontStyle(getConfig(key));
		} catch (IllegalArgumentException e) {
			return Font.PLAIN;
		}
	}

	/**
	 * Laedt eine Zahl aus der Konfigurationsdatei.
	 *
	 * @param key
	 *            Schluessel
	 * @return Zahl
	 */
	public static int getInteger(String key) {
		return Integer.valueOf(getConfig(key));
	}

	/**
	 * Collects and returns the available Languages
	 *
	 * @return available Languages
	 */
	public static String[] getLanguages() {
		CodeSource src = Property.class.getProtectionDomain().getCodeSource();
		LinkedList<String> files = new LinkedList<>();

		boolean noZip = false;

		try {
			URL jar = src.getLocation();
			ZipInputStream zip = new ZipInputStream(jar.openStream());
			ZipEntry ze;

			while ((ze = zip.getNextEntry()) != null) {
				String entryName = ze.getName();

				if (entryName.contains(FILE_PREFIX)) {
					files.add(entryName);
				}
			}

			if (files.size() == 0) {
				noZip = true;
			}

		} catch (Exception e) {
			noZip = true;
		}

		if (noZip) {
			// Start Classloader
			ClassLoader loader = Thread.currentThread().getContextClassLoader();

			// List all Files in selected Package
			InputStream in = loader.getResourceAsStream(PACKET.replaceAll("\\.", "/"));
			BufferedReader rdr = new BufferedReader(new InputStreamReader(in));

			String line;
			try {
				while ((line = rdr.readLine()) != null) {
					// List only files with searched beginning
					if (line.contains(FILE_PREFIX)) {

						files.add(line);
					}
				}
				rdr.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

		// Prepare languages for output
		String[] languages = new String[files.size()];
		for (int i = 0; i < languages.length; i++) {
			String language = files.get(i);
			language = language.substring(language.indexOf(FILE_PREFIX) + FILE_PREFIX.length(), language.indexOf("."));
			languages[i] = language;
		}
		return languages;
	}

	/**
	 * Laedt den String in der eingestellten Sprache.
	 *
	 * @param key
	 *            Schluessel
	 * @return String
	 */
	public static String getString(String key) {

		if (language == null) {
			language = DEFAULT_LANG;
		}

		try {
			return ResourceBundle.getBundle(PACKET + "." + FILE_PREFIX + language).getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	/**
	 * Laedt die Properties
	 *
	 * @return
	 */
	private static Properties loadProperties() {

		Properties prop = new Properties();

		try {
			if (RESOURCE) {
				// Load file from resource
				ClassLoader loader = Property.class.getClassLoader();
				prop.load(loader.getResourceAsStream(PACKET.replaceAll("\\.", "/") + "/" + CONFIG));

			} else {
				try {
					FileReader reader = new FileReader(CONFIG);
					prop.load(reader);
					reader.close();

				} catch (Exception e) {
					try {
						new File(CONFIG).createNewFile();
					} catch (IOException ignored) {
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop;
	}

	/**
	 * Laedt einen Wahrheitswert aus der Konfigurationsdatei
	 *
	 * @param key
	 *            Schluessel
	 * @return Wahrheitswert
	 */
	public static void setBoolean(String key, boolean value) {
		setConfig(key, value ? "1" : "0");
	}

	/**
	 * Setzt den String zum Schluessel aus der Standarddatei.
	 *
	 * @param key
	 *            Schluessel
	 * @param value
	 *            zum Schluessel
	 */
	public static void setConfig(String key, String value) {

		prop.setProperty(key, value);
		try {
			new File(CONFIG).createNewFile();

			FileWriter writer = new FileWriter(CONFIG);
			prop.store(writer, null);
			writer.close();
		} catch (IOException e) {
		}
	}
}
