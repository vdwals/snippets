Property
=========

Before I started using a bunch of external libs, I created everything myself. This is a relict of those days, were I created my own class to handle properties for translations and configuration files. Still used in some projects and not completely useless.