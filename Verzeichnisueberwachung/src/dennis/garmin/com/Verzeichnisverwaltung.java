package dennis.garmin.com;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class Verzeichnisverwaltung {
	private final static String VERZEICHNIS_LISTE = "verzeichnisse.lg",
			SEPERATOR = "=";

	private File verzeichnisListe;
	private HashMap<Integer, String> verzeichnisse;

	/**
	 * Testfunktion
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Verzeichnisverwaltung m = new Verzeichnisverwaltung();
		m.addVerzeichnis();
	}

	public Verzeichnisverwaltung() {
		this.verzeichnisListe = new File(VERZEICHNIS_LISTE);
		this.verzeichnisse = new HashMap<>();

		try {
			this.verzeichnisListe.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ladeVerzeichnisse();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void ladeVerzeichnisse() throws FileNotFoundException {
		Scanner scan = new Scanner(verzeichnisListe);

		while (scan.hasNextLine()) {
			String[] line = scan.nextLine().split(SEPERATOR);
			verzeichnisse.put(Integer.parseInt(line[0]), line[1]);
		}

		// TODO Debugausgabe
		for (int key : verzeichnisse.keySet()) {
			System.out.println(verzeichnisse.get(key));
		}

		scan.close();
	}

	public void speicherVerzeichnisse() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(verzeichnisListe));

		for (int key : verzeichnisse.keySet()) {
			bw.write(key + "=" + verzeichnisse.get(key));
			bw.newLine();
		}

		bw.close();
	}

	public void addVerzeichnis() {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int i = fc.showOpenDialog(null);

		if (i == JFileChooser.APPROVE_OPTION) {
			verzeichnisse.put(verzeichnisse.size(), fc.getSelectedFile()
					.getAbsolutePath());
			try {
				speicherVerzeichnisse();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// TODO Debugausgabe
			System.out.println(fc.getSelectedFile().getAbsolutePath());
		}
	}

}
