package dennis.garmin.com;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class CreateRoundButton extends JButton {
	public CreateRoundButton(String label) {
		super("<html><font color = blue>" + label + "</font></html>");

		setContentAreaFilled(false);
	}

	protected void paintComponent(Graphics g) {
		if (getModel().isArmed()) {
			g.setColor(getBackground().brighter());
		} else {
			g.setColor(getBackground());
		}
		g.fillRect(0, 0, getSize().width - 1, getSize().height - 1);

		super.paintComponent(g);
	}

	protected void paintBorder(Graphics g) {
	}

	Shape shape;

	public boolean contains(int x, int y) {
		if (shape == null || !shape.getBounds().equals(getBounds())) {
			shape = new Ellipse2D.Float(0, 0, getWidth(), getHeight());
		}
		return shape.contains(x, y);
	}

	public static void main(String[] args) {
		JButton button = new CreateRoundButton("Click-diesen-Button");
		button.setBackground(Color.green.darker());
//		button.setForeground(Color.blue.darker());
		button.setEnabled(false);

		JFrame frame = new JFrame();
		frame.getContentPane().add(button);
		frame.getContentPane().setLayout(new FlowLayout());
		frame.setSize(150, 150);
		frame.setVisible(true);
	}
}