package dennis.garmin.com;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class Main {
	private static int SIZE = 600;

	private JFrame mainFrame;
	private JButton latLong, ok, convertFile;
	private boolean bLatLong;
	private JTextArea coordinates;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Main m = new Main();
				m.go();
			}
		});
	}

	public Main() {
		bLatLong = true;
		getMainFrame();

		try {

			ClassLoader loader = Main.class.getClassLoader();
			InputStream in = loader.getResourceAsStream("dennis/garmin/com");
			BufferedReader rdr = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = rdr.readLine()) != null) {
				System.out.println("file: " + line);
			}
			rdr.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void go() {
		getMainFrame().setSize(SIZE, SIZE);
		getMainFrame().setVisible(true);
	}

	private JFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new JFrame();
			mainFrame.setLayout(new BorderLayout(10, 10));
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			mainFrame.add(getLatLong(), BorderLayout.NORTH);

			JScrollPane p = new JScrollPane(getCoordinates());
			mainFrame.add(p, BorderLayout.CENTER);
			p.getHorizontalScrollBar().setUI(new YourScrollbarUI());

			JPanel buttons = new JPanel(new GridLayout(1, 2, 5, 5));
			buttons.add(getOk());
			buttons.add(getConvertFile());

			mainFrame.add(buttons, BorderLayout.SOUTH);
		}
		return mainFrame;
	}

	private JButton getLatLong() {
		if (latLong == null) {
			latLong = new JButton("Lat-Long");
			latLong.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (bLatLong) {
						latLong.setText("Long-Lat");
						System.out.println("11.609910 48.144427");
					} else {
						latLong.setText("Lat-Long");
						System.out.println("48.144427 11.609910");
					}

					bLatLong = !bLatLong;
				}
			});
		}
		return latLong;
	}

	private JButton getOk() {
		if (ok == null) {
			ok = new JButton("Ok");
			ok.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						createRoutesFromField(getCoordinates().getText());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			ok.setUI(new MyButtonUI());
		}
		return ok;
	}

	// http://www.java2s.com/Tutorials/Java/Swing/JButton/Change_JButton_with_BasicButtonUI_in_Java.htm

	private JButton getConvertFile() {
		if (convertFile == null) {
			convertFile = new JButton("Convert File");
			convertFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						createRoutesFromFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			convertFile.setUI(new MyButtonUI());
		}
		return convertFile;
	}

	private JTextArea getCoordinates() {
		if (coordinates == null) {
			coordinates = new JTextArea(
					"48.144427 11.609910; 49.792671 9.936590\n"
							+ "50.918860 6.959248; 50.917438 6.956497");
			coordinates.setBackground(Color.CYAN.darker());
			coordinates.setBorder(null);
		}
		return coordinates;
	}

	private void createRoutesFromField(String coordinates) throws IOException {
		int counter = 1;
		Scanner scan = new Scanner(coordinates);

		while (scan.hasNextLine()) {
			// Zeilenweise auslesen und Routennamen erstellen
			String line = scan.nextLine();

			if (lineToRoute(line, "", "Route - " + counter))
				counter++;
		}
		scan.close();
	}

	private void createRoutesFromFile() throws IOException {
		// Datei �ffnen
		JFileChooser chooser = new JFileChooser();
		// Dialog zum Oeffnen von Dateien anzeigen
		int r = chooser.showOpenDialog(mainFrame);

		if (r == JFileChooser.APPROVE_OPTION) {
			File input = chooser.getSelectedFile();

			Scanner scan = new Scanner(input);

			String name = "";
			String line = "";
			String output = "";

			while (scan.hasNextLine()) {
				line = scan.nextLine();
				String tmp = "";

				if (line.contains("<td>")) {
					tmp = line.substring(line.indexOf("<td>") + 4,
							line.indexOf("</td>"));

					if (tmp.contains(" - ") && !tmp.contains("href"))
						// Name auslesen
						name = tmp;

					else if (tmp.contains("href")) {
						// Link aendern
						int index = line.indexOf("\"") + 1;
						String replace = line.substring(index,
								line.indexOf("\"", index));
						line = line.replace(replace, name + ".txt");
					}

					else if (tmp.contains("; "))
						// Route auslesen
						lineToRoute(
								tmp,
								input.getAbsolutePath().replace(
										input.getName(), ""), name);
				}
				output += line + System.lineSeparator();
			}
			scan.close();

			BufferedWriter bw = new BufferedWriter(new FileWriter(input));
			bw.write(output);
			bw.close();
		}
	}

	private boolean lineToRoute(String line, String path, String routeName)
			throws IOException {
		String[] waypoints = line.split("; ");

		if (waypoints.length > 1) {
			String output = "";

			for (int i = 0; i < waypoints.length; i++) {
				System.out.println(waypoints[i]);

				String[] coordsPerPoint = new String[2];
				if (waypoints[i].contains(","))
					coordsPerPoint = waypoints[i].split(",");
				else
					coordsPerPoint = waypoints[i].split(" ");

				if (!bLatLong)
					output += coordsPerPoint[0] + System.lineSeparator()
							+ coordsPerPoint[1] + System.lineSeparator();
				else
					output += coordsPerPoint[1] + System.lineSeparator()
							+ coordsPerPoint[0] + System.lineSeparator();
			}
			System.out.println(path + routeName + ".txt");

			File route = new File(path + routeName + ".txt");
			if (!route.exists())
				route.createNewFile();

			BufferedWriter bw = new BufferedWriter(new FileWriter(route));
			bw.write(output);
			bw.close();

			return true;
		}
		return false;
	}

	protected ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
}
