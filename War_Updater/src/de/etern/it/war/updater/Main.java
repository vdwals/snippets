/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.war.updater;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.etern.it.war.updater.model.Settings;
import de.etern.it.war.updater.view.SelectGUI;

public class Main {
	private static Main INSTANCE;

	public static Main getInstance() {
		return INSTANCE;
	}

	public static void main(String[] args) {
		Main.INSTANCE = new Main();
	}

	private SelectGUI gui;

	private File source, target;

	private Main() {
		this.gui = new SelectGUI();

		File[] lastFolders = Settings.getLastFolder();
		this.source = lastFolders[0];
		this.target = lastFolders[1];
		this.gui.getPathToSource().setText(lastFolders[0].getAbsolutePath());
		this.gui.getPathToTarget().setText(lastFolders[1].getAbsolutePath());

		Console.getInstance().printLog = true;
		Console.getInstance().printTimeStamp = true;

		this.gui.show();
	}

	private String getCopyCommand(String sourcePath, String targetPath, boolean delete) {
		String command = "robocopy \"" + sourcePath + "\" \"" + targetPath + "\"";
		if (delete) {
			command += " /mir";
		} else {
			command += " /e";
		}

		return command;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(File source) {
		Settings.setLastFolder(source, this.target);
		this.source = source;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(File target) {
		Settings.setLastFolder(this.source, target);
		this.target = target;
	}

	public void startCopyFromSourceToTarget(boolean update, boolean backup) {
		if (this.source.getAbsolutePath().equals(this.target.getAbsolutePath()))
			return;

		new Thread(() -> {
			Main.this.gui.activateButtons(false);

			/* Update SVN */
			Runtime runTime = Runtime.getRuntime();
			try {
				String command1 = "svn info https://absvn.pass-consulting.com/svn/VTO-NG_development";
				Console.getInstance().write("Run Info: " + command1);
				Main.this.gui.getRunning().setText("Run Info: " + command1);
				final Process process = runTime
						.exec("svn info https://absvn.pass-consulting.com/svn/VTO-NG_development");

				new Thread(() -> {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line;

					while (process.isAlive()) {
						try {

							while ((line = reader.readLine()) != null) {
								Console.getInstance().write(line);
							}
						} catch (IOException e) {
							break;
						}
					}

				}).start();

				while (process.isAlive()) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e2) {
					}
				}

				if (update) {
					command1 = "svn update \"" + Main.this.source.getAbsolutePath() + "\"";
					Console.getInstance().write("Run Update: " + command1);
					Main.this.gui.getRunning().setText("Run Update: " + command1);
					final Process updateProcess = runTime.exec(command1);

					new Thread(() -> {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(updateProcess.getInputStream()));
						String line;

						while (updateProcess.isAlive()) {
							try {

								while ((line = reader.readLine()) != null) {
									Console.getInstance().write(line);
								}
							} catch (IOException e) {
								break;
							}
						}

					}).start();

					while (updateProcess.isAlive()) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e3) {
						}
					}

				}
			} catch (IOException e4) {
			}

			// TODO hint: suche nach aenderungen
			// forfiles /P [path(.)] /S /D +14.07.2015

			if (backup) {
				/* Starte Backup */
				// try {
				String backupDir = Main.this.target.getAbsolutePath() + "_backup_"
						+ new SimpleDateFormat("YY-MM-dd-HH-mm").format(new Date());
				File dir1 = new File(backupDir);
				dir1.mkdirs();

				File origin = new File(Main.this.target.getAbsolutePath());
				origin.renameTo(dir1);

				// String command = getCopyCommand(
				// target.getAbsolutePath(), backupDir, true);
				// Console.getInstance().write("Run Backup: " + command);
				// Main.this.gui.getRunning().setText(
				// "Run Backup: " + command);
				//
				// Process copy = runTime.exec(command);
				//
				// new Thread(new Runnable() {
				//
				// @Override
				// public void run() {
				// BufferedReader reader = new BufferedReader(
				// new InputStreamReader(copy
				// .getInputStream()));
				// String line;
				//
				// while (copy.isAlive()) {
				// try {
				//
				// while ((line = reader.readLine()) != null) {
				// if (!line.contains("%"))
				// Console.getInstance().write(
				// line);
				// }
				// } catch (IOException e) {
				// break;
				// }
				// }
				//
				// }
				// }).start();
				//
				// while (copy.isAlive())
				// try {
				// Thread.sleep(500);
				// } catch (InterruptedException e) {
				// }
				//
				// } catch (IOException e1) {
				// e1.printStackTrace();
				// }
			}

			/* Starte kopierprozess */
			File[] dirs = new File[4];
			dirs[0] = new File(Main.this.source.getAbsolutePath() + "/06_Implementation/Product_Definitions");
			dirs[1] = new File(Main.this.source.getAbsolutePath()
					+ "/12_Customer_Projects/02_Giller/06_Implementation/VTO2G_Giller");
			dirs[2] = new File(Main.this.source.getAbsolutePath()
					+ "/12_Customer_Projects/03_Morgan_Stanley/06_Implementation/VTO2G_MorganStanley");
			dirs[3] = new File(Main.this.source.getAbsolutePath()
					+ "/Customer_Specific_Components/Customer_Specific_Components/FareFamilies");
			for (File dir2 : dirs) {
				Process copy;
				try {
					String command2 = getCopyCommand(dir2.getAbsolutePath(), Main.this.target.getAbsolutePath(), false);
					Console.getInstance().write("Run Copy: " + command2);
					Main.this.gui.getRunning().setText("Run Copy: " + command2);
					copy = runTime.exec(command2);

					new Thread(() -> {
						BufferedReader reader = new BufferedReader(new InputStreamReader(copy.getInputStream()));
						String line;

						while (copy.isAlive()) {
							try {

								while ((line = reader.readLine()) != null) {
									if (!line.contains("%")) {
										Console.getInstance().write(line);
									}
								}
							} catch (IOException e) {
								break;
							}
						}

					}).start();

					while (copy.isAlive()) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e5) {
						}
					}

				} catch (IOException e1) {
				}
			}

			Main.this.gui.activateButtons(true);
		}).start();
	}
}
