/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.war.updater.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import de.etern.it.war.updater.Console;
import de.etern.it.war.updater.Main;

public class SelectGUI {
	private JFrame mainFrame;
	private JTextField pathToSource, pathToTarget;
	private JButton selectSource, selectTarget, syncTargetWithSource, syncSourceWithTarget;
	private JCheckBox update, backup;
	private JLabel running;

	private ActionListener buttonActions;

	public SelectGUI() {
		this.mainFrame = new JFrame("Giller-Project_Snychronizer");
		this.mainFrame.setLayout(new BorderLayout(5, 5));

		JPanel main = new JPanel(new GridLayout(5, 1, 20, 20));

		JPanel feld1 = new JPanel(new BorderLayout(5, 5));
		feld1.add(getPathToSource(), BorderLayout.CENTER);
		feld1.add(getSelectSource(), BorderLayout.EAST);

		JPanel feld2 = new JPanel(new BorderLayout(5, 5));
		feld2.add(getPathToTarget(), BorderLayout.CENTER);
		feld2.add(getSelectTarget(), BorderLayout.EAST);

		JPanel buttons = new JPanel(new GridLayout(1, 5, 5, 5));
		buttons.add(new JLabel());
		buttons.add(getSyncSourceWithTarget());
		buttons.add(getSyncTargetWithSource());

		JPanel checkbox = new JPanel(new GridLayout(1, 4, 5, 5));
		checkbox.add(new JLabel("Update"));
		checkbox.add(getUpdate());
		checkbox.add(new JLabel("Backup"));
		checkbox.add(getBackup());

		main.add(feld1);
		main.add(feld2);
		main.add(buttons);
		main.add(checkbox);
		main.add(getRunning());

		this.mainFrame.add(main, BorderLayout.NORTH);
		this.mainFrame.add(new JScrollPane(Console.getInstance().getOutput(), JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);

		this.mainFrame.pack();

		this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void activateButtons(boolean enable) {
		getBackup().setEnabled(enable);
		getUpdate().setEnabled(enable);
		getSelectSource().setEnabled(enable);
		getSelectTarget().setEnabled(enable);
		getSyncSourceWithTarget().setEnabled(enable);
		getSyncTargetWithSource().setEnabled(enable);
	}

	private JButton generateDefaultButton(String label) {
		JButton button = new JButton(label);
		button.addActionListener(getButtonActions());
		return button;
	}

	/**
	 * @return the backup
	 */
	private JCheckBox getBackup() {
		if (this.backup == null) {
			this.backup = new JCheckBox();
			this.backup.setSelected(true);
		}
		return this.backup;
	}

	/**
	 * @return the buttonActions
	 */
	private ActionListener getButtonActions() {
		if (this.buttonActions == null) {
			this.buttonActions = arg0 -> {
				if ((arg0.getSource() == getSelectSource()) || (arg0.getSource() == getSelectTarget())) {
					JFileChooser fc = new JFileChooser();
					fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

					int state = fc.showOpenDialog(null);

					if (state == JFileChooser.APPROVE_OPTION)
						if (arg0.getSource() == getSelectSource()) {
							getPathToSource().setText(fc.getSelectedFile().getAbsolutePath());
							Main.getInstance().setSource(fc.getSelectedFile());
						} else {
							getPathToTarget().setText(fc.getSelectedFile().getAbsolutePath());
							Main.getInstance().setTarget(fc.getSelectedFile());
						}

				} else if (arg0.getSource() == getSyncSourceWithTarget()) {
					Main.getInstance().startCopyFromSourceToTarget(getUpdate().isSelected(), getBackup().isSelected());

				} else if (arg0.getSource() == getSyncTargetWithSource()) {

				}
			};
		}
		return this.buttonActions;
	}

	/**
	 * @return the pathToSource
	 */
	public JTextField getPathToSource() {
		if (this.pathToSource == null) {
			this.pathToSource = new JTextField(50);
		}
		return this.pathToSource;
	}

	/**
	 * @return the pathToTarget
	 */
	public JTextField getPathToTarget() {
		if (this.pathToTarget == null) {
			this.pathToTarget = new JTextField(50);
		}
		return this.pathToTarget;
	}

	public JLabel getRunning() {
		if (this.running == null) {
			this.running = new JLabel();
		}
		return this.running;
	}

	/**
	 * @return the selectSource
	 */
	private JButton getSelectSource() {
		if (this.selectSource == null) {
			this.selectSource = generateDefaultButton("...");
		}
		return this.selectSource;
	}

	/**
	 * @return the selectTarget
	 */
	private JButton getSelectTarget() {
		if (this.selectTarget == null) {
			this.selectTarget = generateDefaultButton("...");
		}
		return this.selectTarget;
	}

	/**
	 * @return the syncSourceWithTarget
	 */
	private JButton getSyncSourceWithTarget() {
		if (this.syncSourceWithTarget == null) {
			this.syncSourceWithTarget = generateDefaultButton("<-");
			this.syncSourceWithTarget.setToolTipText("Kopiert Daten aus dem Zielverzeichnis in die Quelle.");
		}
		return this.syncSourceWithTarget;
	}

	/**
	 * @return the syncTargetWithSource
	 */
	private JButton getSyncTargetWithSource() {
		if (this.syncTargetWithSource == null) {
			this.syncTargetWithSource = generateDefaultButton("->");
			this.syncTargetWithSource.setToolTipText("Kopiert Daten aus der Quelle in das Zielverzeichnis.");
		}
		return this.syncTargetWithSource;
	}

	/**
	 * @return the update
	 */
	private JCheckBox getUpdate() {
		if (this.update == null) {
			this.update = new JCheckBox();
			this.update.setSelected(true);
		}
		return this.update;
	}

	public void show() {
		this.mainFrame.setVisible(true);
	}
}
