/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.war.updater.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Settings {
	private static final String SAVE_FILE = "lastFolder.txt";

	public static File[] getLastFolder() {
		File f = new File(SAVE_FILE);
		try {
			f.createNewFile();
		} catch (IOException e1) {
		}

		File[] paths = new File[2];
		paths[0] = new File(f.getAbsolutePath());
		paths[1] = new File(f.getAbsolutePath());

		if (f.exists()) {
			try {
				Scanner scan = new Scanner(f);
				for (int i = 0; (i < paths.length) && scan.hasNextLine(); i++) {
					paths[i] = new File(scan.nextLine());
				}

				scan.close();
			} catch (FileNotFoundException e) {
			}

		}

		return paths;
	}

	public static void setLastFolder(File source, File target) {
		File f = new File(SAVE_FILE);

		try {
			f.createNewFile();
			FileWriter fw = new FileWriter(f);
			fw.write(source.getAbsolutePath() + "\n" + target.getAbsolutePath());
			fw.close();

		} catch (IOException e) {
		}

	}

}
