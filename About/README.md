About
=========

This package provides a standard display of the common "About" information for a software. I use it in many other Projects controlled via a GUI, therefore I outsourced it. Since it's only a single class I thought it's useless to create a whole Repository for it, so it becomes a snippet.