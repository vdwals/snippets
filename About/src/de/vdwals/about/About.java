/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.about;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import de.etern.it.update.Version;

/**
 * Klasse zum Anzeigen der Softwareinformationen.
 *
 * @author Dennis van der Wals
 *
 */
public class About extends JFrame {
	/**
	 * Serial-ID
	 */
	private static final long serialVersionUID = 1L;
	private static final int FONT_SIZE = 100;

	/**
	 * Testmethode
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final About a = new About("Testapp", "Dennis van der Wals", "dennis@vdwals.de",
				"https://github.com/vdwals/snipplets.git", "Dennis van der Wals",
				new Version(1, 0, 0, Version.RELEASE));

		a.setVisible(true);
	}

	private final Font standardFont, titelFont;

	private JButton open;
	private String appName, author, mail, web, copyright;

	private Version version;

	/**
	 * Erzeugt das Fenster fuer die Darstellung von Programminformationen
	 */
	public About(String appName, String author, String email, String webAddress, String copyright, Version version) {
		this.appName = appName;
		this.author = author;
		this.mail = email;
		this.web = webAddress;
		this.copyright = copyright;
		this.version = version;

		this.setLayout(new BorderLayout(5, 5));

		final int fontSize = Toolkit.getDefaultToolkit().getScreenSize().width / FONT_SIZE;

		this.standardFont = new Font(UIManager.getDefaults().getFont("Label.font").getFontName(), Font.PLAIN, fontSize);
		this.titelFont = new Font(this.standardFont.getFontName(), Font.BOLD, fontSize);

		final JPanel center = new JPanel(new BorderLayout(10, 10));

		final JLabel titel = new JLabel(this.appName, SwingConstants.CENTER);
		titel.setFont(this.titelFont);

		center.add(getTitelLabel(this.appName), BorderLayout.NORTH);

		final JPanel infos = new JPanel(new GridLayout(0, 2, 10, 10));

		infos.add(getLabel("Version", SwingConstants.LEFT));
		infos.add(getLabel(this.version.toString(), SwingConstants.RIGHT));

		infos.add(getLabel("Author", SwingConstants.LEFT));
		infos.add(getLabel(this.author, SwingConstants.RIGHT));

		final JLabel mail = getLabel("<html><a href=\"\">" + this.mail + "</a></html>", SwingConstants.RIGHT);
		sendMail(mail, this.mail);
		infos.add(getLabel("Contact", SwingConstants.LEFT));
		infos.add(mail);

		final JLabel web = getLabel("<html><a href=\"\">" + this.web + "</a></html>", SwingConstants.RIGHT);
		goWebsite(web, this.web);
		infos.add(getLabel("Web", SwingConstants.LEFT));
		infos.add(web);

		center.add(infos, BorderLayout.CENTER);
		center.add(getLabel("�" + this.copyright, SwingConstants.CENTER), BorderLayout.SOUTH);

		center.add(new JLabel(" "), BorderLayout.WEST);
		center.add(new JLabel(" "), BorderLayout.EAST);

		this.add(center, BorderLayout.CENTER);

		final JButton close = new JButton("Close");
		close.addActionListener(arg0 -> About.this.setVisible(false));

		final JPanel button = new JPanel(new GridLayout(0, 3, 10, 10));
		button.add(new JLabel(" "));
		button.add(close);
		button.add(new JLabel(" "));

		this.add(button, BorderLayout.SOUTH);

		this.pack();
		this.setResizable(false);

		this.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - this.getHeight()) / 2);
	}

	/**
	 * Gibt ein JLabel mit passenden Einstellungen zurueck.
	 *
	 * @param text
	 *            Inhalt des Labels
	 * @param alignment
	 *            Ausrichtung des Textes
	 * @return JLabel mit Text und Ausrichtung, sowie der gew�hlten
	 *         Standardschriftart.
	 */
	private JLabel getLabel(final String text, final int alignment) {
		final JLabel neu = new JLabel(text, alignment);
		neu.setFont(this.standardFont);
		return neu;
	}

	public JButton getOpenButton(final String openText) {
		if (this.open == null) {
			this.open = new JButton(openText);
			this.open.addActionListener(e -> About.this.setVisible(true));
		}
		return this.open;
	}

	/**
	 * Erzeugt das Label f�r den Titel des Programms
	 *
	 * @param text
	 *            Titel des Programms
	 * @return Label mit Titel, zentrierter Ausrichtung und titelschriftart
	 */
	private JLabel getTitelLabel(final String text) {
		final JLabel titel = new JLabel(text, SwingConstants.CENTER);
		titel.setFont(this.titelFont);
		return titel;
	}

	/**
	 * Methode, die einem JLabel erm�glicht einen weblink aufzurufen.
	 *
	 * @param website
	 *            Jlabel mit Link
	 * @param site
	 *            Zielseite
	 */
	private void goWebsite(final JLabel website, final String site) {
		website.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				try {
					Desktop.getDesktop().browse(new URI(site));
				} catch (URISyntaxException | IOException ex) {
					// It looks like there's a problem
				}
			}
		});
	}

	/**
	 * Methode, die einem JLabel erm�glicht, einen mailto-link zu �ffnen, wenn
	 * er geklickt wird.
	 *
	 * @param contact
	 *            Jlabel mit Email adresse
	 * @param mail
	 *            Emailadresse
	 */
	private void sendMail(final JLabel contact, final String mail) {
		contact.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				try {
					Desktop.getDesktop().mail(new URI("mailto:" + mail));
				} catch (URISyntaxException | IOException ex) {
					// It looks like there's a problem
				}
			}
		});
	}
}
