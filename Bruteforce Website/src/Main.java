/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
public class Main {

	private static char getNextChar(int i) {
		i++;
		switch (i) {
		case 1:
			i = 48;
			break;
		case 58:
			i = 65;
			break;
		case 91:
			i = 97;
			break;
		case 123:
			i = 47;
			break;
		}

		return (char) i;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 600; i++) {
			System.out.println(i + ": " + (char) i);

			/*
			 * Interessante Bereiche: 48 - 57 Zahlen, 65 - 90 Buchstaben, 97 -
			 * 122 kleinbuchstaben. Ausgangsadresse:
			 * http://m155a.freeiz.com/1111.html
			 */
		}

		char[] bruteforce = new char[20];

		while (bruteforce[0] != 47) {
			for (int i = 47; i < 123; i++) {
				updateNextField(bruteforce, 19);
				i = bruteforce[19];

				String url = "http://m155a.freeiz.com/" + new String(bruteforce).trim() + ".html";
				System.out.println(url);
				System.out.println(WebsiteGrabber.ladeSeite(url));
			}
		}
	}

	private static void updateNextField(char[] array, int field) {
		if (field == -1)
			return;

		array[field] = getNextChar(array[field]);
		if (array[field] == 47) {
			array[field] = 48;
			updateNextField(array, field - 1);
		}
	}
}
