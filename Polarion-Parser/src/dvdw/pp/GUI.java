package dvdw.pp;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame {
	/**
	 * Default Serial-ID
	 */
	private static final long serialVersionUID = 1L;
	private Main main;
	private JButton next, prev, save, cancel;
	private JLabel workitemNumber;
	private Testresult[] options = { Testresult.PASSED, Testresult.FAILED,
			Testresult.UNAPPLICABLE };

	private JPanel item;

	public LinkedList<JTextField> actualResults;
	public LinkedList<JComboBox<Testresult>> results;

	public GUI(Main main, Workitem item) {
		this.main = main;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel top = new JPanel(new GridLayout(0, 3, 5, 5));
		top.add(getPrev());
		top.add(getWorkitemNumber());
		top.add(getNext());

		getitem().add(getItemPanel(item));

		JPanel sub = new JPanel(new GridLayout(0, 2, 5, 5));
		sub.add(getSave());
		sub.add(getCancel());

		this.setLayout(new BorderLayout(5, 5));
		this.add(top, BorderLayout.NORTH);
		this.add(getitem(), BorderLayout.CENTER);
		this.add(sub, BorderLayout.SOUTH);

		this.pack();
		this.setVisible(true);
	}

	public JPanel getItemPanel(Workitem item) {
		JPanel p = new JPanel(new GridLayout(5, 5));

		JPanel info = new JPanel(new GridLayout(3, 2, 5, 5));
		info.add(new JLabel("Title: "));
		info.add(new JLabel(item.getTitle()));
		info.add(new JLabel("ID: "));
		info.add(new JLabel(item.getId()));
		info.add(new JLabel("Description: "));
		info.add(new JLabel());
		// info.add(new JTextField(item.getDescription()), BorderLayout.SOUTH);

		JPanel steps = new JPanel(new GridLayout(0, 4, 5, 5));
		actualResults = new LinkedList<>();
		results = new LinkedList<>();
		for (int i = 0; i < item.getTestSteps().size(); i++) {
			actualResults.add(new JTextField());
			results.add(new JComboBox<Testresult>(options));
			steps.add(getField(item.getTestSteps().get(i)));
			steps.add(getField(item.getExpectedResults().get(i)));
			steps.add(actualResults.get(i));
			steps.add(results.get(i));
		}

		p.add(info);
		p.add(getField(item.getDescription()));
		p.add(steps);

		return p;
	}

	private JTextArea getField(String s) {
		JTextArea textArea = new JTextArea(s);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		return textArea;
	}

	public JPanel getitem() {
		if (item == null)
			item = new JPanel(new GridLayout(1, 0));
		return item;
	}

	public JLabel getWorkitemNumber() {
		if (workitemNumber == null)
			workitemNumber = new JLabel("1/" + main.countWorkitems());

		return workitemNumber;
	}

	public JButton getNext() {
		if (next == null) {
			next = new JButton(">>");
			next.addActionListener(main);
		}
		return next;
	}

	public JButton getPrev() {
		if (prev == null) {
			prev = new JButton("<<");
			prev.addActionListener(main);
		}
		return prev;
	}

	public JButton getSave() {
		if (save == null) {
			save = new JButton("Save");
			save.addActionListener(main);
		}
		return save;
	}

	public JButton getCancel() {
		if (cancel == null) {
			cancel = new JButton("Cancel");
			cancel.addActionListener(main);
		}
		return cancel;
	}
}
