/**
 * 
 */
package dvdw.pp;

import java.util.LinkedList;

/**
 * @author vanderwals
 * 
 */
public class Workitem {
	private String title, description, id;
	private LinkedList<String> testSteps, expectedResults, actualResult;
	private LinkedList<Testresult> testResults;
	private Testresult finalResult;

	public Workitem(String title, String description, String id,
			LinkedList<String> testSteps, LinkedList<String> expectedResults) {
		this.title = title;
		this.description = description;
		this.id = id;
		this.testSteps = testSteps;
		this.expectedResults = expectedResults;
		this.actualResult = new LinkedList<>();
		this.testResults = new LinkedList<>();
	}

	public void setResult(String actualResult, Testresult testResult, int index) {
		this.actualResult.add(index, actualResult);
		this.testResults.add(index, testResult);
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getId() {
		return id;
	}

	public LinkedList<String> getTestSteps() {
		return testSteps;
	}

	public LinkedList<String> getExpectedResults() {
		return expectedResults;
	}

	public LinkedList<String> getActualResult() {
		return actualResult;
	}

	public LinkedList<Testresult> getTestResults() {
		return testResults;
	}

	public Testresult getFinalResult() {
		return finalResult;
	}
}
