/**
 * 
 */
package dvdw.pp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * @author vanderwals
 * 
 */
public class Main implements ActionListener {
	private LinkedList<Workitem> workitems;
	private int item;
	private GUI gui;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		JFileChooser fc = new JFileChooser();
		int state = fc.showOpenDialog(null);

		String path = "";

		if (state == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			path = file.getAbsolutePath();
		} else {
			System.out.println("Auswahl abgebrochen");
			System.exit(0);
		}

		try {
			XMLParser p = new XMLParser(path);
			LinkedList<Workitem> w = p.getWorkitems();
			new Main(w);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Main(LinkedList<Workitem> workitems) {
		this.workitems = workitems;
		item = 0;
		gui = new GUI(this, workitems.get(0));
	}

	public int countWorkitems() {
		return workitems.size();
	}

	public boolean hasNextItem() {
		return item < workitems.size() - 1;
	}

	public boolean hasPrevItem() {
		return item > 0;
	}

	private Workitem getNextItem() {
		if (hasNextItem()) {
			return workitems.get(++item);
		}
		return null;
	}

	private Workitem getPrevItem() {
		if (hasPrevItem()) {
			return workitems.get(--item);
		}
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = ((JButton) e.getSource()).getActionCommand();

	}

}
