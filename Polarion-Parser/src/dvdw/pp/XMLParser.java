package dvdw.pp;

import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Klasse zum einlesen einer Polarion xml
 * 
 * @author vanderwals
 * 
 */
public class XMLParser {
	private String filepath;
	private Document polarionDocument;

	/**
	 * Erzeugt einen neuen Parser und bereitet ihn vor
	 * 
	 * @param path
	 *            Pfad zur Datei
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public XMLParser(String path) throws ParserConfigurationException,
			SAXException, IOException {
		this.filepath = path;

		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		polarionDocument = docBuilder.parse(this.filepath);
	}

	/**
	 * Liest alle Workitems aus dem XML in eine Liste ein
	 * 
	 * @return Liste mit Workitems
	 */
	public LinkedList<Workitem> getWorkitems() {
		LinkedList<Workitem> workitems = new LinkedList<>();

		// Get root element
		NodeList polarionWorkItems = polarionDocument.getFirstChild()
				.getChildNodes();
		for (int i = 0; i < polarionWorkItems.getLength(); i++) {
			Node workItem = polarionWorkItems.item(i);

			// Get all workitems
			if (workItem.getNodeType() == Node.ELEMENT_NODE) {
				Element workElement = (Element) workItem;

				String title = "";
				String description = "";
				String id = "";
				LinkedList<String> testSteps = new LinkedList<>();
				LinkedList<String> expectedResults = new LinkedList<>();

				// open costum fields
				NodeList child = workElement
						.getElementsByTagName("customFields");
				if (child.getLength() == 1) {
					NodeList customFields = child.item(0).getChildNodes();

					// Get necessary information from customField
					for (int j = 0; j < customFields.getLength(); j++) {
						if (customFields.item(j).getNodeType() == Node.ELEMENT_NODE) {
							Element field = (Element) customFields.item(j);

							switch (field.getAttribute("id")) {
							case "testObject":
								title = field.getElementsByTagName("string")
										.item(0).getTextContent();
								System.out.println("Titel: " + title);
								break;

							case "testDetails":
								description = field
										.getElementsByTagName("text").item(0)
										.getTextContent();
								System.out.println("Desc: " + description);
								break;

							case "testCaseID":
								id = field.getElementsByTagName("string")
										.item(0).getTextContent();
								System.out.println("ID: " + id);
								break;

							case "testSteps":
								Node n = field
										.getElementsByTagName("testSteps")
										.item(0);
								getTestSteps(n, testSteps, expectedResults);
								break;

							default:
								break;
							}
						}
					}

				}
				workitems.add(new Workitem(title, description, id, testSteps,
						expectedResults));
			}
		}
		return workitems;
	}

	/**
	 * Liest die Steps aus einem Workitem heraus
	 * 
	 * @param node
	 *            Knoten mit Steps
	 * @param testSteps
	 *            Liste mit Steps
	 * @param expectedResult
	 *            Liste mit Results
	 */
	private void getTestSteps(Node node, LinkedList<String> testSteps,
			LinkedList<String> expectedResult) {
		Element e = (Element) node;

		Node steps = e.getElementsByTagName("steps").item(0);
		if (steps.getNodeType() == Node.ELEMENT_NODE) {
			NodeList subSteps = ((Element) steps)
					.getElementsByTagName("testStep");

			for (int i = 0; i < subSteps.getLength(); i++) {
				if (subSteps.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element value = (Element) ((Element) subSteps.item(i))
							.getElementsByTagName("values").item(0);

					NodeList items = value.getElementsByTagName("item");

					String tmp = getText((Element) items.item(0));
					if (tmp != null)
						testSteps.add(tmp);

					tmp = getText((Element) items.item(1));
					if (tmp != null)
						expectedResult.add(tmp);

					System.out.println("Step: " + testSteps.getLast()
							+ "\n Result: " + expectedResult.getLast());
				}
			}
		}
	}

	/**
	 * Hohlt den Text eines Results/Steps aus der html-formatierung
	 * 
	 * @param e
	 *            html-element
	 * @return Reinen text
	 */
	private String getText(Element e) {
		Node n = ((Element) e.getElementsByTagName("html").item(0))
				.getElementsByTagName("p").item(0);

		if (n == null)
			n = ((Element) e.getElementsByTagName("html").item(0))
					.getElementsByTagName("span").item(0);

		if (n != null)
			return n.getTextContent();

		return null;
	}
}
