/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstruktur.rsbaum;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class BinarySearchTree {
	private BinarySearchTreeNode root;
	private BinarySearchTreeNode nil;
	private final Color schwarz;
	private final Color rot;

	/**
	 * Konstruktor fuer einen neuen binaeren Suchbaum
	 */
	public BinarySearchTree(final int key) {
		this.schwarz = new Color("s");
		this.rot = new Color("r");
		this.nil = null;
		this.nil = new BinarySearchTreeNode(0, this.root, this.schwarz, this.nil);
		this.root = new BinarySearchTreeNode(key, this.nil, this.schwarz, this.nil);
	}

	/**
	 * Loescht einen Knoten aus dem Baum.
	 *
	 * @param keyNode
	 *            zu loeschender Knoten.
	 */
	public void Delete(final BinarySearchTreeNode keyNode) {
		BinarySearchTreeNode y = keyNode;
		BinarySearchTreeNode x = null;
		Color origColor = y.getColor();
		if (keyNode.getLeftChild() == this.nil) {
			x = keyNode.getRightChild();
			this.Transplant(keyNode, keyNode.getRightChild());
		} else if (keyNode.getRightChild() == this.nil) {
			this.Transplant(keyNode, keyNode.getLeftChild());
		} else {
			y = this.getMinimum(keyNode.getRightChild());
			origColor = y.getColor();
			x = y.getRightChild();
			if (y.getParent() != keyNode) {
				this.Transplant(y, y.getRightChild());
				y.setRechts(keyNode.getRightChild());
				y.getRightChild().setEltern(y);
			} else {
				x.setEltern(y);
			}
			this.Transplant(keyNode, y);
			y.setLinks(keyNode.getLeftChild());
			y.getLeftChild().setEltern(y);
			y.setColor(keyNode.getColor());
		}
		if (origColor == this.schwarz) {
			this.RBDeleteFixup(x);
		}
	}

	/**
	 * Loescht einen Schluessel aus dem Baum.
	 *
	 * @param key
	 *            zu loeschender Schluessel.
	 */
	public void Delete(final int key) {
		final BinarySearchTreeNode keyNode = this.search(key);
		if (keyNode == null) {
			System.out.println("Zahl nicht vorhanden");
		} else {
			this.Delete(keyNode);
		}
	}

	/**
	 * Diese Methode gibt das Minimum des Baumes zurueck.
	 *
	 * @return Knoten mit kleinstem Punkt
	 */
	public BinarySearchTreeNode getMinimum(final BinarySearchTreeNode n) {
		BinarySearchTreeNode temp = n;
		while (temp.getLeftChild() != this.nil) {
			temp = temp.getLeftChild();
		}
		return temp;
	}

	/**
	 * Gibt die Wurzel des Baums zurueck
	 *
	 * @return root
	 */
	public BinarySearchTreeNode getRoot() {
		return this.root;
	}

	/**
	 * Methode zum Einfuegen eines ComparablePoint.
	 *
	 * @param p
	 *            einzufuegender Punkt
	 * @return eingefuegter Knoten
	 */
	public BinarySearchTreeNode insert(final int p) {
		BinarySearchTreeNode eltern = this.nil;
		BinarySearchTreeNode kind = this.root;
		while (kind != this.nil) {
			eltern = kind;
			if (kind.getInfo() > p) {
				kind = kind.getLeftChild();
			} else {
				kind = kind.getRightChild();
			}
		}
		final BinarySearchTreeNode neu = new BinarySearchTreeNode(p, eltern, this.rot, this.nil);
		if (eltern.getInfo() > p) {
			eltern.setLinks(neu);
		} else {
			eltern.setRechts(neu);
		}
		this.RBInsertFixup(neu);
		return neu;
	}

	public void LeftRotate(final BinarySearchTreeNode x) {
		final BinarySearchTreeNode y = x.getRightChild();
		y.setEltern(x.getParent());
		if (this.root == x) {
			this.root = y;
		}
		x.setEltern(y);
		x.setRechts(y.getRightChild());
		y.setLinks(x);
		if (x == y.getParent().getLeftChild()) {
			y.getParent().setLinks(y);
		} else {
			y.getParent().setRechts(y);
		}
	}

	public String[][] ouput() {
		final int tiefe = this.tiefe();
		final int breite = tiefe * 2 * 2;
		final String[][] s = new String[tiefe][breite];
		for (int i = 0; i < tiefe; i++) {
			for (int j = 0; j < breite; j++) {
				s[i][j] = "    ";
			}
		}
		this.ouput(this.root, s, 0, breite - 1, 0);
		return s;
	}

	private void ouput(final BinarySearchTreeNode n, final String[][] s, final int links, final int rechts, int level) {
		final int mitte = (links + rechts) / 2;
		s[level][mitte] = "" + n.getInfo() + "." + n.getColor().getFarbe();
		level++;
		if (n.getLeftChild() != this.nil) {
			this.ouput(n.getLeftChild(), s, links, mitte, level);
		}
		if (n.getRightChild() != this.nil) {
			this.ouput(n.getRightChild(), s, mitte, rechts, level);
		}
	}

	private void RBDeleteFixup(BinarySearchTreeNode x) {
		BinarySearchTreeNode w;
		while ((x != this.root) && (x.getColor() == this.schwarz))
			if (x == x.getParent().getLeftChild()) {
				w = x.getParent().getRightChild();
				if (w.getColor() == this.rot) {
					w.setColor(this.schwarz);
					x.getParent().setColor(this.rot);
					this.LeftRotate(x.getParent());
					w = x.getParent().getRightChild();
				}
				if ((w.getLeftChild().getColor() == this.schwarz) && (w.getRightChild().getColor() == this.schwarz)) {
					w.setColor(this.rot);
					x = x.getParent();
				} else {
					if (w.getRightChild().getColor() == this.schwarz) {
						w.getLeftChild().setColor(this.schwarz);
						w.setColor(this.rot);
						this.RightRotate(w);
						w = x.getParent().getRightChild();
					}
					w.setColor(x.getParent().getColor());
					x.getParent().setColor(this.schwarz);
					this.LeftRotate(x.getParent());
					x = this.root;
				}
			} else {
				w = x.getParent().getLeftChild();
				if (w.getColor() == this.rot) {
					w.setColor(this.schwarz);
					x.getParent().setColor(this.rot);
					this.RightRotate(x.getParent());
					w = x.getParent().getLeftChild();
				}
				if ((w.getRightChild().getColor() == this.schwarz) && (w.getLeftChild().getColor() == this.schwarz)) {
					w.setColor(this.rot);
					x = x.getParent();
				} else {
					if (w.getLeftChild().getColor() == this.schwarz) {
						w.getRightChild().setColor(this.schwarz);
						w.setColor(this.rot);
						this.LeftRotate(w);
						w = x.getParent().getLeftChild();
					}
					w.setColor(x.getParent().getColor());
					x.getParent().setColor(this.schwarz);
					this.RightRotate(x.getParent());
					x = this.root;
				}
			}
		x.setColor(this.schwarz);
	}

	public void RBInsertFixup(BinarySearchTreeNode neu) {
		while (neu.getParent().getColor() == this.rot) {
			if (neu.getParent() == neu.getParent().getParent().getLeftChild()) {
				final BinarySearchTreeNode y = neu.getParent().getParent().getRightChild();
				if (y.getColor() == this.rot) {
					neu.getParent().setColor(this.schwarz);
					neu.getParent().getParent().setColor(this.rot);
					y.setColor(this.schwarz);
					neu = neu.getParent().getParent();
				} else {
					if (neu == neu.getParent().getRightChild()) {
						neu = neu.getParent();
						this.LeftRotate(neu);
					}
					neu.getParent().setColor(this.schwarz);
					neu.getParent().getParent().setColor(this.rot);
					this.RightRotate(neu.getParent().getParent());
				}
			} else {
				final BinarySearchTreeNode y = neu.getParent().getParent().getLeftChild();
				if (y.getColor() == this.rot) {
					neu.getParent().setColor(this.schwarz);
					neu.getParent().getParent().setColor(this.rot);
					y.setColor(this.rot);
					neu = neu.getParent().getParent();
				} else {
					if (neu == neu.getParent().getLeftChild()) {
						neu = neu.getParent();
						this.RightRotate(neu);
					}
					neu.getParent().setColor(this.schwarz);
					neu.getParent().getParent().setColor(this.rot);
					this.LeftRotate(neu.getParent().getParent());
				}
			}
			this.root.setColor(this.schwarz);
		}
	}

	public void RightRotate(final BinarySearchTreeNode y) {
		final BinarySearchTreeNode x = y.getLeftChild();
		x.setEltern(y.getParent());
		if (this.root == y) {
			this.root = x;
		}
		y.setEltern(x);
		y.setLinks(x.getRightChild());
		x.setRechts(y);
		if (y == x.getParent().getLeftChild()) {
			x.getParent().setLinks(x);
		} else {
			x.getParent().setRechts(x);
		}

	}

	/**
	 * Sucht den Punkt p im Baum und gibt dessen Knoten oder null zurueck. Diese
	 * Methode uebergibt Parameter an eine rekursive Suchmethode.
	 *
	 * @param p
	 *            zu suchender Punkt
	 * @return Knoten mit Punkt, oder null
	 */
	public BinarySearchTreeNode search(final int p) {
		return this.search(p, this.root);
	}

	/**
	 * Sucht den Punkt p im Baum und gibt dessen Knoten oder null zurueck.
	 *
	 * @param p
	 *            zu suchender Punkt
	 * @param temp
	 *            Startknoten
	 * @return Knoten mit Punkt, oder null
	 */
	public BinarySearchTreeNode search(final int p, final BinarySearchTreeNode temp) {
		if ((temp == null) || (temp.getInfo() == p))
			return temp;
		if (temp == this.nil)
			return null;
		if (temp.getInfo() > p)
			return this.search(p, temp.getLeftChild());
		else
			return this.search(p, temp.getRightChild());
	}

	/**
	 * Nimmt den groessten Schluessel aus dem Baum und gibt ihn zurueck.
	 *
	 * @return Knoten mit groe�tem Schluessel
	 */
	public BinarySearchTreeNode takeMax() {
		BinarySearchTreeNode temp = this.getRoot();
		while (temp.getLeftChild() != this.nil) {
			temp = temp.getLeftChild();
		}
		this.Delete(temp.getInfo());
		return temp;
	}

	public int tiefe() {
		return this.tiefe(this.root, 0);
	}

	public int tiefe(final BinarySearchTreeNode n, final int zaehler) {
		if ((n.getLeftChild() != this.nil) && (n.getRightChild() != this.nil)) {
			final int a = this.tiefe(n.getLeftChild(), zaehler + 1);
			final int b = this.tiefe(n.getRightChild(), zaehler + 1);
			if (a < b)
				return b;
			else
				return a;
		}
		if (n.getLeftChild() != this.nil)
			return this.tiefe(n.getLeftChild(), zaehler + 1);
		if (n.getRightChild() != this.nil)
			return this.tiefe(n.getRightChild(), zaehler + 1);
		return zaehler + 1;
	}

	public void Transplant(final BinarySearchTreeNode u, final BinarySearchTreeNode v) {
		if (u.getParent() == this.nil) {
			this.root = v;
		} else if (u == u.getParent().getLeftChild()) {
			u.getParent().setLinks(v);
		} else {
			u.getParent().setRechts(v);
		}
		v.setEltern(u.getParent());
	}
}
