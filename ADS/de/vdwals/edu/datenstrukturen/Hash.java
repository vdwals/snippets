/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class Hash {

	public static int hash(final int x) {
		return (((3 * x) + 7) % 16);
	}

	public static int hash2(final int x) {
		return (7 - (x % 7));
	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] A = { 12, 44, 71, 88, 23, 94, 11, 39, 20, 16, 5 };
		for (final int element : A) {
			System.out.println("h(" + element + ") = " + hash(element) + "||h2(" + element + ") = " + hash2(element));
		}
	}

}
