/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

import java.awt.Point;

/**
 * Diese Klasse repraesentiert Punkte, die lexikographisch verglichen werden
 * k�nnen.
 *
 * @author Jan Haunert
 * @version 1
 */
public class ComparablePoint extends Point {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Der Konstruktor.
	 *
	 * @param x
	 *            die x-Koordinate
	 * @param y
	 *            die y-Koordinate
	 */
	public ComparablePoint(final int x, final int y) {
		super(x, y);
	}

	/**
	 * Vergleicht this und p lexikographisch.
	 *
	 * @param p
	 *            der Punkt, mit dem this verglichen wird
	 * @return -1, 0, oder 1, je nachdem, ob this < p, this = p oder this > p
	 */
	public int compareTo(final ComparablePoint p) {
		if (this.x < p.x)
			return -1;
		else if (this.x == p.x)
			if (this.y < p.y)
				return -1;
			else if (this.y == p.y)
				return 0;
		return 1;
	}
}
