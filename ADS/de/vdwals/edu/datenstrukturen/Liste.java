/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class Liste {
	private Item head;

	/**
	 * Erzeugt eine neue Liste.
	 */
	public Liste() {
		this.head = null;
	}

	/**
	 * Entfernt ein Item aus der Liste.
	 *
	 * @param k
	 */
	public void delete(final Item k) {
		if (k == null) {
			System.out.println("Fehler");
		} else {
			if (k.getPrev() != null) {
				k.getPrev().setNext(k.getNext());
			} else {
				this.head = k.getNext();
			}
			if (k.getNext() != null) {
				k.getNext().setPrev(k.getPrev());
			}
		}
	}

	/**
	 * @return head
	 */
	public Item getHead() {
		return this.head;
	}

	/**
	 * Fuegt ein Object an erster Stelle ein.
	 *
	 * @param x
	 *            einzufuegendes Objekt
	 * @return Item mit dem Object
	 */
	public Item insert(final Object x) {
		final Item k = new Item(x, this.head);
		if (this.head != null) {
			this.head.setPrev(k);
		}
		this.head = k;
		return k;
	}

	/**
	 * Dursucht eine Liste nach einem bestimmten Object und gibt dessen Item
	 * zurueck.
	 *
	 * @param x
	 *            zu suchendes Item
	 * @return Object von k
	 */
	public Item search(final Object x) {
		Item k = this.head;
		while ((k != null) && !x.equals(k.getKey())) {
			k = k.getNext();
		}
		return k;
	}
}
