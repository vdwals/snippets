/* ********************************************************************
 * ADS *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
/* ********************************************************************
 * ADS *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
package de.vdwals.edu.datenstrukturen;

/**
 * @author Dennis van der Wals
 *
 */
public class Stapel {
	private final Object[] stapel;
	private int top;

	/**
	 * Initialisiert einen neuen Stapel einer bestimmten laenge.
	 *
	 * @param laenge
	 *            groesse des Stapels
	 */
	public Stapel(final int laenge) {
		this.stapel = new Object[laenge];
		this.top = 0;
	}

	/**
	 * Ermittelt, ob der Stapel leer ist.
	 *
	 * @return true falls leer, sonst false
	 */
	public boolean empty() {
		return (this.top == 0);
	}

	/**
	 * Entfernt das oberste Objekt und gibt es zurueck.
	 *
	 * @return oberstes Objekt
	 */
	public Object pop() {
		if (this.empty()) {
			System.out.println("Fehler: Underflow");
		}
		return this.stapel[this.top--];
	}

	/**
	 * Legt ein Object auf den Stapel.
	 *
	 * @param k
	 *            hinzuzufuegendes Objekt
	 */
	public void push(final Object k) {
		this.top++;
		if (this.top >= this.stapel.length) {
			System.out.println("Fehler: Overflow");
		} else {
			this.stapel[this.top] = k;
		}
	}

	/**
	 * Gibt das oberste Object zurueck.
	 *
	 * @return oberstes Object
	 */
	public Object top() {
		if (this.empty()) {
			System.out.println("Fehler: Underflow");
		}
		return this.stapel[this.top];
	}
}
