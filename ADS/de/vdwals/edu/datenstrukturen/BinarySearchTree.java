/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class BinarySearchTree {
	private BinarySearchTreeNode root;

	/**
	 * Konstruktor fuer einen neuen binaeren Suchbaum
	 */
	public BinarySearchTree() {
		this.root = null;
	}

	/**
	 * Diese Methode gibt das Minimum des Baumes zurueck.
	 *
	 * @return Knoten mit kleinstem Punkt
	 */
	public BinarySearchTreeNode getMinimum() {
		BinarySearchTreeNode temp = this.root;
		while (temp.getLeftChild() != null) {
			temp = temp.getLeftChild();
		}
		return temp;
	}

	/**
	 * Gibt die Wurzel des Baums zurueck
	 *
	 * @return root
	 */
	public BinarySearchTreeNode getRoot() {
		return this.root;
	}

	/**
	 * Methode zum Einfuegen eines ComparablePoint.
	 *
	 * @param p
	 *            einzufuegender Punkt
	 * @return eingefuegter Knoten
	 */
	public BinarySearchTreeNode insert(final ComparablePoint p) {
		BinarySearchTreeNode eltern = null;
		BinarySearchTreeNode kind = this.root;
		while (kind != null) {
			eltern = kind;
			if (kind.getInfo().compareTo(p) == 1) {
				kind = kind.getLeftChild();
			} else {
				kind = kind.getRightChild();
			}
		}
		final BinarySearchTreeNode neu = new BinarySearchTreeNode(p, eltern);
		if (eltern == null) {
			this.root = neu;
		} else if (eltern.getInfo().compareTo(p) == 1) {
			eltern.setLinks(neu);
		} else {
			eltern.setRechts(neu);
		}
		return neu;
	}

	/**
	 * Sucht den Punkt p im Baum und gibt dessen Knoten oder null zurueck. Diese
	 * Methode uebergibt Parameter an eine rekursive Suchmethode.
	 *
	 * @param p
	 *            zu suchender Punkt
	 * @return Knoten mit Punkt, oder null
	 */
	public BinarySearchTreeNode search(final ComparablePoint p) {
		return this.search(p, this.root);
	}

	/**
	 * Sucht den Punkt p im Baum und gibt dessen Knoten oder null zurueck.
	 *
	 * @param p
	 *            zu suchender Punkt
	 * @param temp
	 *            Startknoten
	 * @return Knoten mit Punkt, oder null
	 */
	public BinarySearchTreeNode search(final ComparablePoint p, final BinarySearchTreeNode temp) {
		if ((temp == null) || (temp.getInfo().compareTo(p) == 0))
			return temp;
		if (temp.getInfo().compareTo(p) == 1)
			return this.search(p, temp.getLeftChild());
		else
			return this.search(p, temp.getRightChild());
	}
}
