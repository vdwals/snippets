/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class Item {
	private final Object key;
	private Item prev;
	private Item next;

	/**
	 * Konstruiert ein neues Item.
	 *
	 * @param key
	 *            Object des Items
	 * @param next
	 *            naechstes Item in der Liste
	 */
	public Item(final Object key, final Item next) {
		this.key = key;
		this.prev = null;
		this.next = next;
	}

	/**
	 * @return the key
	 */
	public Object getKey() {
		return this.key;
	}

	/**
	 * @return the next
	 */
	public Item getNext() {
		return this.next;
	}

	/**
	 * @return the prev
	 */
	public Item getPrev() {
		return this.prev;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(final Item next) {
		this.next = next;
	}

	/**
	 * @param prev
	 *            the prev to set
	 */
	public void setPrev(final Item prev) {
		this.prev = prev;
	}

}
