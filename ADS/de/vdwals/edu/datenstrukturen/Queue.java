/* ********************************************************************
 * ADS *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
/* ********************************************************************
 * ADS *
 * Copyright (C) 2010-2011 Dionysoft - http://www.dionysoft.kilu.de *
 * *
 * This library is free software; you can redistribute it and/or *
 * modify it under the terms of the GNU Lesser General Public *
 * License as published by the Free Software Foundation; either *
 * version 2.1 of the License, or (at your option) any later version. *
 * *
 * This library is distributed in the hope that it will be useful, *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU *
 * Lesser General Public License for more details. *
 * *
 * You should have received a copy of the GNU Lesser General Public *
 * License along with this library; if not, write to the *
 * Free Software Foundation, Inc., *
 * 51 Franklin St, Fifth Floor, *
 * Boston, MA 02110-1301 USA *
 * *
 * Or get it online: *
 * http://www.gnu.org/copyleft/lesser.html *
 * ********************************************************************
 */
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class Queue {
	private int tail;
	private int head;
	private final Object[] queue;

	/**
	 * Konstruktor fuer eine neue Queue.
	 *
	 * @param laenge
	 *            groe�e der Queue
	 */
	public Queue(final int laenge) {
		this.queue = new Object[laenge];
		this.tail = 0;
		this.head = 0;
	}

	/**
	 * Nimmt das Objekt vom Kopf der Schlange und gibt es zurueck.
	 *
	 * @return Objekt am Kopf
	 */
	public Object dequeue() {
		final Object k = this.queue[this.head];
		if (this.head == (this.queue.length - 1)) {
			this.head = 0;
		} else {
			this.head++;
		}
		return k;
	}

	/**
	 * Ermittelt, ob die Queue leer ist.
	 *
	 * @return true falls leer, sonst false
	 */
	public boolean empty() {
		return (this.head == this.tail);
	}

	/**
	 * Fuegt ein neues Objekt an den Schwanz der Schlange
	 *
	 * @param k
	 *            einzufuegendes Objekt
	 */
	public void enqueue(final Object k) {
		this.queue[this.tail] = k;
		if (this.tail == (this.queue.length - 1)) {
			this.tail = 0;
		} else {
			this.tail++;
		}
	}
}
