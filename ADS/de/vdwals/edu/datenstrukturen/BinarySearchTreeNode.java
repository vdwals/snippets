/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.datenstrukturen;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class BinarySearchTreeNode {
	private ComparablePoint key;
	private BinarySearchTreeNode links;
	private BinarySearchTreeNode rechts;
	private BinarySearchTreeNode eltern;

	/**
	 * Konstruktor fuer ein BinarySearchTreeNode.
	 *
	 * @param key
	 *            Einzufuegender Wert
	 * @param parent
	 *            Elternknoten
	 */
	public BinarySearchTreeNode(final ComparablePoint key, final BinarySearchTreeNode parent) {
		this.key = key;
		this.eltern = parent;
		this.rechts = null;
		this.links = null;
	}

	/**
	 * Gibt die Information des Knotens zurueck, hier ComparablePoint
	 *
	 * @return key
	 */
	public ComparablePoint getInfo() {
		return this.key;
	}

	/**
	 * Gibt das linke Kind zurueck
	 *
	 * @return links
	 */
	public BinarySearchTreeNode getLeftChild() {
		return this.links;
	}

	/**
	 * Gibt Elternknoten zurueck
	 *
	 * @return eltern
	 */
	public BinarySearchTreeNode getParent() {
		return this.eltern;
	}

	/**
	 * Gibt das rechte Kind zurueck
	 *
	 * @return rechts
	 */
	public BinarySearchTreeNode getRightChild() {
		return this.rechts;
	}

	/**
	 * Methode findet den Knoten mit dem naechsten kleinsten Punkt.
	 *
	 * @return Knoten mit naechstkleinstem Punkt
	 */
	public BinarySearchTreeNode getSuccessor() {
		if (this.rechts != null) {
			BinarySearchTreeNode temp = this.rechts;
			while (temp.getLeftChild() != null) {
				temp = temp.getLeftChild();
			}
			return temp;
		} else
			return null;
	}

	/**
	 * @param eltern
	 *            the eltern to set
	 */
	public void setEltern(final BinarySearchTreeNode eltern) {
		this.eltern = eltern;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(final ComparablePoint key) {
		this.key = key;
	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(final BinarySearchTreeNode links) {
		this.links = links;
	}

	/**
	 * @param rechts
	 *            the rechts to set
	 */
	public void setRechts(final BinarySearchTreeNode rechts) {
		this.rechts = rechts;
	}
}
