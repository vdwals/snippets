/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class MergeSort {

	/**
	 * Testmethode f�r einen MergeSort Algorithmus.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[100];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 100);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		mergeSort(unsortZahlen);
		System.out.println("-------------------");
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
	}

	/**
	 * Vermischt zwei vorsortierte Arrays
	 *
	 * @param zahlenreihe
	 *            Quellarray, welches sortiert wird
	 * @param links
	 *            Anfangsposition der ersten vorsortierten Zahlenreihe
	 * @param mitte
	 *            Endposition der ersten und Anfangsposition der zweiten
	 *            vorsortierten Zahlenreihe
	 * @param rechts
	 *            Endposition der zweiten vorsortierten Reihe
	 */
	private static void merge(final int[] zahlenreihe, final int links, final int mitte, final int rechts) {
		// Erzeugt neue Arrays, damit kein Wert verloren geht und weisst ihnen
		// die Quellarrayzahlen zu
		final int[] Links = new int[(mitte - links) + 1];
		final int[] Rechts = new int[(rechts - mitte) + 1];
		for (int i = 0; i < Links.length; i++) {
			Links[i] = zahlenreihe[links + i];
		}
		for (int i = 0; i < (Rechts.length - 1); i++) {
			Rechts[i] = zahlenreihe[mitte + i + 1];
		}
		Rechts[Rechts.length - 1] = -1;
		int i = 0;
		int j = 0;
		// Zahlen werden verglichen und korrekt sortiert.
		for (int k = links; k <= rechts; k++)
			if (((i < Links.length) && (Links[i] <= Rechts[j])) || (Rechts[j] == -1)) {
				zahlenreihe[k] = Links[i];
				i++;
			} else {
				zahlenreihe[k] = Rechts[j];
				j++;
			}
	}

	/**
	 * Sortiert ein Array aus Zahlen, indem es gespalten und Stueck fuer Stueck
	 * sortiert zusammengesetzt wird, ohne Angabe von linkem oder rechtem Rand.
	 *
	 * @param zahlenreihe
	 *            Zu sortierendes Array
	 */
	public static void mergeSort(final int[] zahlenreihe) {
		mergeSort(zahlenreihe, 0, zahlenreihe.length - 1);
	}

	/**
	 * Sortiert ein Array aus Zahlen, indem es gespalten und Stueck fuer Stueck
	 * sortiert zusammengesetzt wird.
	 *
	 * @param zahlenreihe
	 *            Zu sortierendes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 */
	public static void mergeSort(final int[] zahlenreihe, final int links, final int rechts) {
		if (links < rechts) {
			final int mitte = Math.round((links + rechts) / 2);
			mergeSort(zahlenreihe, links, mitte);
			mergeSort(zahlenreihe, (mitte + 1), rechts);
			merge(zahlenreihe, links, mitte, rechts);
		}
	}
}
