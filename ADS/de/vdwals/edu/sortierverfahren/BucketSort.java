/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

import java.util.Random;

/**
 *
 * @author Dennis
 *
 */
public class BucketSort {

	public static void bucketSort(final int[] zahlenreihe) {
		final int laenge = zahlenreihe.length;
		final int[][] bucket = new int[10][1];
		for (int i = 0; i < laenge; i++) {
			final int b = zahlenreihe[i] / laenge;
			if (bucket[b][bucket[b].length - 1] != 0) {
				bucket[b] = ArrayVerlangern.verlaengern(bucket[b]).clone();
			}
			bucket[b][bucket[b].length - 1] = zahlenreihe[i];
		}
		for (int i = 0; i < 10; i++) {
			InsertionSort.insertionSort(bucket[i]);
		}
		int j = 0;
		for (int i = 0; i < 10; i++) {
			for (int k = 0; k < bucket[i].length; k++) {
				zahlenreihe[j] = bucket[i][k];
				j++;
			}
		}
	}

	/**
	 * Testmethode f�r einen BucketSort Algorithmus mit Zahlen bis 1000000.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = randomSequence(100000, 1000000);
		System.out.println(CheckSorted.checkSorted(unsortZahlen));
		bucketSort(unsortZahlen);
		System.out.println("-------------------");
		System.out.println(CheckSorted.checkSorted(unsortZahlen));
	}

	/**
	 * method randomSequence erzeugt ein Array mit Zufallszahlen.
	 *
	 * @param length
	 *            die Laenge des Arrays
	 * @param max
	 *            die groesste moegliche Zahl
	 * @return das erzeugte Array
	 */
	public static int[] randomSequence(final int length, final int max) {
		final Random random = new Random(0);
		final int[] s = new int[length];
		long maxl = max;
		maxl++;
		for (int i = 0; i < length; i++) {
			final double r = random.nextDouble();
			s[i] = (int) (r * maxl);
		}
		return s;
	}

	/**
	 * Gibt eine kopie des Quellarrays zur�ck mit um 1 erhoehter kapazitaet.
	 *
	 * @param quellArray
	 * @return vergroesserte Kopie des Quellarrays
	 */
	public static int[] verlaengern(final int[] quellArray) {
		final int[] zielArray = new int[quellArray.length + 1];
		for (int i = 0; i < quellArray.length; i++) {
			zielArray[i] = quellArray[i];
		}
		return zielArray;
	}
}
