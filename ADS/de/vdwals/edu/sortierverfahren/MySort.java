/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class MySort {
	// Experimentel ermittelter Wert
	static int stop = 40;
	static int maxVal = 136000;

	/**
	 * Ruft den QuickSort Algorithmus auf und uebergibt die Startparameter zum
	 * sortieren eines Arrays
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 */
	public static void inQuickSort(final int[] zahlenreihe) {
		inQuickSort(zahlenreihe, 0, zahlenreihe.length - 1);
	}

	/**
	 * Sortiert ein Array mit einem QuickSort Algorithmus, sollte das Teilarray
	 * weniger als 30 Elemente enthalten, wird InsertionSort angewendet
	 *
	 * @param zahlenreihe
	 *            zu sortierenes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 */
	public static void inQuickSort(final int[] zahlenreihe, final int links, final int rechts) {
		if ((rechts - links) > stop) {
			final int mitte = partition(zahlenreihe, links, rechts);
			inQuickSort(zahlenreihe, links, mitte - 1);
			inQuickSort(zahlenreihe, mitte + 1, rechts);
		} else {
			insertionSort(zahlenreihe, links, rechts);
		}
	}

	/**
	 * Sortiert ein Teilarray mittels InsertionSort
	 *
	 * @param Zahlenreihe
	 *            zu sortierendes Array
	 * @param links
	 *            linker Rand
	 * @param rechts
	 *            rechter Rand
	 */
	public static void insertionSort(final int[] Zahlenreihe, final int links, final int rechts) {
		for (int i = links + 1; i <= rechts; i++) {
			final int temp = Zahlenreihe[i];
			int j = i - 1;
			while ((j >= links) && (Zahlenreihe[j] > temp)) {
				Zahlenreihe[j + 1] = Zahlenreihe[j];
				j--;
			}
			Zahlenreihe[j + 1] = temp;
		}
	}

	/**
	 * Sortiert das Array in eine <= pivot haelfte und > pivot haelfte
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 * @return Aufteilpunkt
	 */
	public static int partition(final int[] zahlenreihe, final int links, final int rechts) {
		final int pivot = zahlenreihe[rechts];
		int i = links - 1;
		for (int j = links; j < rechts; j++)
			if (zahlenreihe[j] <= pivot) {
				i++;
				Swap.swap(zahlenreihe, i, j);
			}
		Swap.swap(zahlenreihe, i + 1, rechts);
		return i + 1;
	}
}
