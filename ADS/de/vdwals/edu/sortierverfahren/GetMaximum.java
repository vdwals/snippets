/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 *         Diverse Methoden die Maxima aufspueren
 *
 */
public class GetMaximum {

	/**
	 * Methode zum ermitteln eines Maximums in einem Array
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @return Position des Maximums
	 */
	public static int getMax(final int[] zahlenreihe) {
		int position = 0;
		for (int i = 1; i < zahlenreihe.length; i++)
			if (zahlenreihe[i] > zahlenreihe[position]) {
				position = i;
			}
		return position;
	}

	/**
	 * Ermittelt die groesste Zahl zwischen zwei Position
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @param start
	 *            kleinste Position (beginnt bei 0, wird nicht ueberprueft)
	 * @param ende
	 *            groesste Position (beginnt bei 0, wird nicht ueberprueft)
	 * @return position der Zahl
	 */
	public static int getMaxBetween(final int[] zahlenreihe, final int start, final int ende) {
		int position = start + 1;
		for (int i = start + 2; i < ende; i++)
			if (zahlenreihe[i] > zahlenreihe[position]) {
				position = i;
			}
		return position;
	}

	/**
	 * Ueberprueft ob ein Array sortiert ist und sucht die Position des
	 * Maximums.
	 *
	 * @param zahlenreihe
	 *            Zu ueberpruefendes Array
	 * @return Position des Maximums, falls sortiert -1
	 */
	public static int getMaxCheckSort(final int[] zahlenreihe) {
		int maxInfo = 0;
		int zaehler = 1;
		for (int i = 0; i < zahlenreihe.length; i++)
			if (zahlenreihe[i] > zahlenreihe[maxInfo]) {
				maxInfo = i;
				zaehler++;
			}
		if (zaehler == zahlenreihe.length) {
			maxInfo = -1;
		}
		return maxInfo;
	}

	/**
	 * Ermittelt die groesste Zahl ab einer bestimmten Position
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @param pos
	 *            kleinste Position (beginnt bei 0, wird nicht ueberprueft)
	 * @return position der Zahl
	 */
	public static int getMaxFromPos(final int[] zahlenreihe, final int pos) {
		int position = pos + 1;
		for (int i = pos + 2; i < zahlenreihe.length; i++)
			if (zahlenreihe[i] > zahlenreihe[position]) {
				position = i;
			}
		return position;
	}

	/**
	 * Ermittelt die groesste Zahl x <= key
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @param key
	 *            groesste Zahl
	 * @return position der Zahl, -1 falls keine Zahl gefunden
	 */
	public static int getMaxTillKey(final int[] zahlenreihe, final int key) {
		int position = 0;
		for (position = 0; position < (zahlenreihe.length - 1); position++)
			if (zahlenreihe[position] <= key) {
				break;
			}
		for (int i = position; i < zahlenreihe.length; i++)
			if ((zahlenreihe[i] > zahlenreihe[position]) && (zahlenreihe[i] <= key)) {
				position = i;
			}
		if (zahlenreihe[position] <= key)
			return position;
		else
			return -1;
	}

	/**
	 * Ermittelt die groesste Zahl bis zu einer bestimmten Position
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @param pos
	 *            groesste Position (beginnt bei 0, wird nicht ueberprueft)
	 * @return position der Zahl
	 */
	public static int getMaxTillPos(final int[] zahlenreihe, final int pos) {
		int position = 0;
		for (int i = 1; i < pos; i++)
			if (zahlenreihe[i] > zahlenreihe[position]) {
				position = i;
			}
		return position;
	}

	/**
	 * Testmethode f�r einen getMax Algorithmus
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[10];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 10);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		System.out.println("-------------------");
		final int maxtillkey = getMaxTillKey(unsortZahlen, 1);
		if (maxtillkey == -1) {
			System.out.println("Zahl nicht vorhanden");
		} else {
			System.out.println(unsortZahlen[getMaxTillKey(unsortZahlen, 1)]);
		}
	}

	/**
	 * Methode zum ermitteln des Maximums in einem Array aus einer Auswahl an
	 * Positionen
	 *
	 * @param zahlenreihe
	 *            Array mit den zu ueberpruefenden Zahlen
	 * @param positionen
	 *            zu vergleichende Postitionen
	 * @return die Position des Maximums
	 */
	public static int max(final int[] zahlenreihe, final int[] positionen) {
		int maxPosition = positionen[0];
		for (int i = 1; i < positionen.length; i++)
			if (zahlenreihe[positionen[i]] > zahlenreihe[maxPosition]) {
				maxPosition = positionen[i];
			}
		return maxPosition;
	}

	public static int min(final int[] zahlenreihe) {
		final int min = 0;
		for (int i = 1; i < zahlenreihe.length; i++)
			if (zahlenreihe[i] < zahlenreihe[min]) {
				i = min;
			}
		return min;
	}
}
