/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class CountingSort {

	/**
	 * Ermittelt Maximum eines Arrays und startet CountingSort mit k
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 */
	public static void countingSort(final int[] zahlenreihe) {
		countingSort(zahlenreihe, zahlenreihe[GetMaximum.getMax(zahlenreihe)]);
	}

	/**
	 * Sortiert ein Array mittels CountingSort
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 * @param intervalllaenge
	 *            Zahlenintervall des Arrays [0,k]
	 */
	public static void countingSort(final int[] zahlenreihe, final int intervalllaenge) {
		final int[] counting = new int[intervalllaenge + 1];
		for (int i = 0; i < zahlenreihe.length; i++) {
			counting[zahlenreihe[i]]++;
		}
		for (int i = 1; i < counting.length; i++) {
			counting[i] += counting[i - 1];
		}
		final int[] sortArray = new int[zahlenreihe.length];
		for (int i = sortArray.length - 1; i >= 0; --i) {
			sortArray[counting[zahlenreihe[i]] - 1] = zahlenreihe[i];
			counting[zahlenreihe[i]]--;
		}
		for (int i = 0; i < zahlenreihe.length; i++) {
			zahlenreihe[i] = sortArray[i];
		}
	}

	/**
	 * Testmethode f�r einen CountingSort Algorithmus.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[1000000];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 1000000);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		countingSort(unsortZahlen);
		System.out.println("-------------------");
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
	}
}
