/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class CheckSorted {

	/**
	 * Ueberprueft ein Array, ob es aufsteigend sortiert ist
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefnendes Array
	 * @return true, falls sortiert, sonst false
	 */
	public static boolean checkSorted(final int[] zahlenreihe) {
		boolean temp = true;
		for (int i = 1; i < zahlenreihe.length; i++)
			if (zahlenreihe[i] < zahlenreihe[i - 1]) {
				temp = false;
				break;
			}
		return temp;
	}

	/**
	 * Ueberprueft eine Reihe von Zahlen ob sie ab der Startposition sortiert
	 * sind.
	 *
	 * @param zahlenreihe
	 *            zu ueberpruefendes Array
	 * @param start
	 *            Erste Zahl
	 * @param end
	 *            Letzte Zahl
	 * @return sortiert
	 */
	public static boolean checkSorted(final int[] zahlenreihe, final int start, final int end) {
		boolean temp = true;
		for (int i = start + 1; i < (end + 1); i++)
			if (zahlenreihe[i] < zahlenreihe[i - 1]) {
				temp = false;
				break;
			}
		return temp;
	}

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] test1 = { 1, 2, 3 };
		final int[] test2 = { 1, 3, 2 };
		System.out.println(checkSorted(test1));
		System.out.println(checkSorted(test2));
	}

}
