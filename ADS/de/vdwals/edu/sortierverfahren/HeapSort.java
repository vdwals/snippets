/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class HeapSort {

	/**
	 * Erzeugt aus einem Array ein MaxHeap
	 *
	 * @param a
	 *            zu sortierendes Array
	 */
	public static void buildMaxHeap(final int[] a) {
		final int heapSize = a.length;
		for (int i = Math.round(heapSize / 2); i >= 0; i--) {
			maxHeapify(a, i, heapSize);
		}
	}

	/**
	 * Extrahiert das Maximum aus einem Heap
	 *
	 * @param heap
	 *            Heap
	 * @return Maximum
	 */
	public static int extractMax(final int[] heap) {
		final int max = findMax(heap);
		heap[0] = heap[heap.length - 1];
		heap[heap.length - 1] = -1;
		maxHeapify(heap, 0, heap.length - 1);
		return max;
	}

	/**
	 * Findet das Maximum in einem Heap
	 *
	 * @param heap
	 *            Heap
	 * @return Maximum
	 */
	public static int findMax(final int[] heap) {
		return heap[0];
	}

	/**
	 * Sortiert ein uebergebenes Array per HeapSort
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 */
	public static void heapSort(final int[] zahlenreihe) {
		final int[] temp = new int[zahlenreihe.length];
		for (int i = 0; i < zahlenreihe.length; i++) {
			temp[i] = zahlenreihe[i];
		}
		buildMaxHeap(temp);
		for (int i = zahlenreihe.length - 1; i >= 0; i--) {
			zahlenreihe[i] = extractMax(temp);
		}
	}

	/**
	 * Ermittelt den Index des linken Childs der Zahl an der Stelle i.
	 *
	 * @param i
	 *            Position des Parents
	 * @return Position des linken Childs
	 */
	public static int left(final int i) {
		return 2 * i;
	}

	/**
	 * Testmethode f�r einen HeapSort Algorithmus.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[50];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 100);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		System.out.println(CheckSorted.checkSorted(unsortZahlen));
		heapSort(unsortZahlen);
		System.out.println("-------------------");
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		System.out.println(CheckSorted.checkSorted(unsortZahlen));
	}

	/**
	 * Methode zum ermitteln der Position des Maximums in einem Array
	 *
	 * @param zahlenreihe
	 *            Array mit den zu ueberpruefenden Zahlen
	 * @param positionen
	 *            zu vergleichende Postitionen
	 * @return die Position des Maximums
	 */
	public static int max(final int[] zahlenreihe, final int[] positionen) {
		int maxPosition = positionen[0];
		for (int i = 1; i < positionen.length; i++)
			if (zahlenreihe[positionen[i]] > zahlenreihe[maxPosition]) {
				maxPosition = positionen[i];
			}
		return maxPosition;
	}

	/**
	 * Ueberprueft ein Heap , ob es die MaxHeap-Bedingung erfuellt, sonst tausch
	 * es die entsprechenden Zahlen
	 *
	 * @param a
	 *            Array, dass auf ueberprueft wird
	 * @param i
	 *            Position des Parents
	 * @param heapSize
	 *            groe�e des Heaps
	 */
	public static void maxHeapify(final int[] a, final int i, final int heapSize) {
		int links = left(i + 1) - 1;
		int rechts = right(i + 1) - 1;
		if (links > (heapSize - 1)) {
			links = i;
		}
		if (rechts > (heapSize - 1)) {
			rechts = i;
		}
		final int[] positionen = { i, links, rechts };
		final int max = max(a, positionen);
		if (max != i) {
			Swap.swap(a, i, max);
			maxHeapify(a, max, heapSize);
		}
	}

	/**
	 * Ermittelt den Index des Parents der Zahl an der Stelle i.
	 *
	 * @param i
	 *            Position des Childs
	 * @return Position des Parents
	 */
	public static int parent(final int i) {
		return Math.round(i / 2);
	}

	/**
	 * Ermittelt den Index des rechten Childs der Zahl an der Stelle i.
	 *
	 * @param i
	 *            Position des Parents
	 * @return Position des rechten Childs
	 */
	public static int right(final int i) {
		return (2 * i) + 1;
	}
}
