/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

public class ArrayVerlangern {

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[][] test = new int[5][4];
		System.out.println(test[1].length);
		// int[] tmp = new int[test[1].length + 1];
		// for (int i = 0; i < tmp.length; i++) {
		// tmp[i] = verlaengern(test[1])[i];
		// }
		// test[1] = tmp.clone();
		test[1] = verlaengern(test[1], 9 - test[1].length).clone();
		System.out.println(test[1].length);
	}

	public static int[] verlaengern(final int[] quellArray) {
		final int[] zielArray = new int[quellArray.length + 1];
		for (int i = 0; i < quellArray.length; i++) {
			zielArray[i] = quellArray[i];
		}
		return zielArray;
	}

	public static int[] verlaengern(final int[] quellArray, final int laenge) {
		final int[] zielArray = new int[quellArray.length + laenge];
		for (int i = 0; i < quellArray.length; i++) {
			zielArray[i] = quellArray[i];
		}
		return zielArray;
	}

}
