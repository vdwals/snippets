/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class SelectionSort {

	/**
	 * Sortiert ein uebergebenes Array
	 *
	 * @param A
	 *            zu sortierendes Array
	 */
	public static void easySort(final int[] A) {
		for (int i = 0; i < (A.length - 1); i++) {
			final int merkVar = A[i];
			final int position = findMin(A, i);
			A[i] = A[position];
			A[position] = merkVar;
		}
	}

	/**
	 * Ermittelt den Minimalwert eines Arrays ab einer bestimmten Stelle.
	 *
	 * @param array
	 *            zu Untersuchendes Array
	 * @param start
	 *            Position im Array, ab der gesucht wird.
	 * @return position des niedrigsten Wertes
	 */
	public static int findMin(final int[] array, final int start) {
		int aktMin = array[start];
		int position = start;
		for (int i = start + 1; i < array.length; i++)
			if (aktMin > array[i]) {
				aktMin = array[i];
				position = i;
			}
		return position;
	}

	/**
	 * Testmethode f�r einen EasySort Algorithmus.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[50];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 100);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		easySort(unsortZahlen);
		System.out.println("-------------------");
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
	}
}
