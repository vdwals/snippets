/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.sortierverfahren;

/**
 *
 * @author Dennis
 *
 */
public class QuickSort {

	/**
	 * Testmethode f�r einen InsertionSort Algorithmus.
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final int[] unsortZahlen = new int[50];
		for (int i = 0; i < unsortZahlen.length; i++) {
			unsortZahlen[i] = (int) (Math.random() * 100);
		}
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		quickSort(unsortZahlen);
		System.out.println("-------------------");
		for (final int element : unsortZahlen) {
			System.out.println(element);
		}
		System.out.println(CheckSorted.checkSorted(unsortZahlen));
	}

	public static void median(final int[] zahlenreihe, final int[] positionen) {
		if (zahlenreihe[positionen[0]] > zahlenreihe[positionen[1]]) {
			Swap.swap(positionen, 0, 1);
		}
		if (zahlenreihe[positionen[2]] > zahlenreihe[positionen[0]]) {
			Swap.swap(positionen, 0, 2);
		}
		if (zahlenreihe[positionen[2]] > zahlenreihe[positionen[1]]) {
			Swap.swap(positionen, 1, 2);
		}
		Swap.swap(zahlenreihe, positionen[1], positionen[2]);
	}

	/**
	 * Sortiert das Array in eine <= pivot haelfte und > pivot haelfte
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 * @return Aufteilpunkt
	 */
	public static int partition(final int[] zahlenreihe, final int links, final int rechts) {
		/*
		 * Auskommentierte Medianfunktion verlangsamt den Algorithmus, obwohl
		 * statistisch schneller.
		 */
		// int[] positionen = {links, (int) (links + rechts)/2, rechts};
		// median(zahlenreihe, positionen);
		// Swap.swap(zahlenreihe, positionen[1], rechts);
		final int pivot = zahlenreihe[rechts];
		int i = links - 1;
		for (int j = links; j < rechts; j++)
			if (zahlenreihe[j] <= pivot) {
				i++;
				Swap.swap(zahlenreihe, i, j);
			}
		Swap.swap(zahlenreihe, i + 1, rechts);
		return i + 1;
	}

	/**
	 * Ruft den QuickSort Algorithmus auf und uebergibt die Startparameter zum
	 * sortieren eines Arrays
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 */
	public static void quickSort(final int[] zahlenreihe) {
		quickSort(zahlenreihe, 0, zahlenreihe.length - 1);
	}

	/**
	 * Sortiert ein Array mit einem QuickSort Algorithmus
	 *
	 * @param zahlenreihe
	 *            zu sortierenes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 */
	public static void quickSort(final int[] zahlenreihe, final int links, final int rechts) {
		if (links <= rechts) {
			final int mitte = partition(zahlenreihe, links, rechts);
			quickSort(zahlenreihe, links, mitte - 1);
			quickSort(zahlenreihe, mitte + 1, rechts);
		}
	}

	/**
	 * Ermoeglicht ein zufaelliges Pivotelement
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 * @return Spaltindex
	 */
	public static int randomizedPartition(final int[] zahlenreihe, final int links, final int rechts) {
		final int i = links + ((links - rechts) * (int) Math.random());
		Swap.swap(zahlenreihe, rechts, i);
		return partition(zahlenreihe, links, rechts);
	}

	/**
	 * Ruft den QuickSort Algorithmus auf und uebergibt die Startparameter zum
	 * sortieren eines Arrays
	 *
	 * @param zahlenreihe
	 *            zu sortierendes Array
	 */
	public static void randomizedQuickSort(final int[] zahlenreihe) {
		randomizedQuickSort(zahlenreihe, 0, zahlenreihe.length - 1);
	}

	/**
	 * Sortiert ein Array mit einem QuickSort Algorithmus
	 *
	 * @param zahlenreihe
	 *            zu sortierenes Array
	 * @param links
	 *            linker Rand des Arrays
	 * @param rechts
	 *            rechter Rand des Arrays
	 */
	public static void randomizedQuickSort(final int[] zahlenreihe, final int links, final int rechts) {
		if (links < rechts) {
			final int mitte = randomizedPartition(zahlenreihe, links, rechts);
			randomizedQuickSort(zahlenreihe, links, mitte - 1);
			randomizedQuickSort(zahlenreihe, mitte + 1, rechts);
		}
	}
}
