import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Class to manipulate SN3K.ini for better usage of SN3K while testing process
 * 
 * @author Dennis van der Wals
 * 
 */
public class Main {

	private static LinkedList<String> lines1, lines2;

	public static void main(String[] args) {
		lines1 = new LinkedList<>();
		lines2 = new LinkedList<>();

		try {
			Scanner scan = new Scanner(new File("t1.csv"));

			while (scan.hasNextLine())
				lines1.add(scan.nextLine());

			scan = new Scanner(new File("t2.csv"));
			while (scan.hasNextLine())
				lines2.add(scan.nextLine());

			scan.close();

			for (int i = 1; i < lines1.size(); i++) {
				for (String line2 : lines2) {
					int index1, index2;
					index1 = line2.indexOf(";");
					index2 = line2.indexOf(";", index1 + 1);
					String test = line2.substring(index1 + 1, index2);
					String id = line2.substring(0, index1);

					if (lines1.get(i).contains(test))
						lines1.set(i, lines1.get(i) + id);
				}
			}
			
			File f = new File("t3.csv");
			f.createNewFile();
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			for (String string : lines1) {
				bw.write(string);
				bw.newLine();
			}
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
