/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.astern;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
	private static LinkedList<Knoten> staedte;

	private static Knoten addStadt(final String stadt) {
		for (final Knoten s : staedte) {
			if (s.getName().equals(stadt))
				return s;
		}

		final Knoten neu = new Knoten(stadt);
		staedte.add(neu);
		return neu;
	}

	private static void addVerbindung(final String start, final String ziel, final int kosten,
			final boolean entfernung) {
		final Knoten[] ziele = new Knoten[2];
		ziele[0] = addStadt(start);
		ziele[1] = addStadt(ziel);

		final Kante verbindung = new Kante(ziele, kosten);

		if (entfernung) {
			ziele[0].addEntfernung(verbindung);
			ziele[1].addEntfernung(verbindung);
		} else {
			ziele[0].addVerbindung(verbindung);
			ziele[1].addVerbindung(verbindung);
		}
	}

	private static Knoten getPfad(final String start, final String ziel) {
		Knoten s = new Knoten(start);
		Knoten z = new Knoten(ziel);

		for (final Knoten stadt : staedte) {
			if (stadt.equals(s)) {
				s = stadt;
			} else if (stadt.equals(z)) {
				z = stadt;
			}
		}

		if (staedte.contains(s) && staedte.contains(z))
			return AStern.getPfad(s, z, staedte);

		return null;
	}

	public static void main(final String[] args) throws FileNotFoundException {
		staedte = new LinkedList<>();

		Scanner scan = new Scanner(new File("c_edge"));

		while (scan.hasNextLine()) {
			final String[] s = scan.nextLine().split(" ");
			addVerbindung(s[0], s[1], Integer.parseInt(s[2]), false);
		}
		scan.close();

		scan = new Scanner(new File("h_edge"));

		while (scan.hasNextLine()) {
			final String[] s = scan.nextLine().split(" ");
			addVerbindung(s[0], s[1], Integer.parseInt(s[2]), true);
		}

		scan.close();

		// Beispielausgabe
		// for (Knoten stadt : staedte) {
		// System.out.println(stadt.getName() + " Verbindung nach:");
		// for (Kante ziel : stadt.getVerbindungen()) {
		// System.out.println(ziel.getTarget(stadt).getName() + "\t"
		// + ziel.getKosten());
		// }
		//
		// System.out.println(stadt.getName() + " Entfernung nach:");
		// for (Kante ziel : stadt.getEntfernungen()) {
		// System.out.println(ziel.getTarget(stadt).getName() + "\t"
		// + ziel.getKosten());
		// }
		// }

		// test von Wuerzburg nach Erfurt: Direkte Fahrt, 195 km
		Knoten pfad = getPfad("Koeln", "Muenchen");

		while (pfad.getParent() != null) {
			System.out.print(pfad.getName() + " - ");
			pfad = pfad.getParent();
		}
		System.out.println(pfad.getName());
	}
}
