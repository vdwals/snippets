/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.astern;

public class Kante {
	private final Knoten[] knoten;
	private final int kosten;

	public Kante(final Knoten[] knoten, final int kosten) {
		this.kosten = kosten;
		this.knoten = knoten;
	}

	public boolean equals(final Kante k) {
		if (k == null)
			return false;

		if ((k.getTarget(this.knoten[0]) == null) || (k.getTarget(this.knoten[1]) == null))
			return false;

		return (k.getTarget(this.knoten[0]).equals(this.knoten[1])
				&& k.getTarget(this.knoten[1]).equals(this.knoten[0]));
	}

	public int getKosten() {
		return this.kosten;
	}

	public Knoten getTarget(final Knoten start) {
		if (this.knoten[0].equals(start))
			return this.knoten[1];
		else if (this.knoten[1].equals(start))
			return this.knoten[0];
		else
			return null;
	}
}
