/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.astern;

import java.util.LinkedList;

public class Knoten {
	private final String name;
	private final LinkedList<Kante> verbindungen, entfernungen;
	private Knoten parent;
	private int g, h;

	public Knoten(final Knoten parent, final Knoten k, final int g, final int h) {
		this.name = k.name;
		this.verbindungen = k.verbindungen;
		this.entfernungen = k.entfernungen;
		this.parent = parent;
		this.g = g;
		this.h = h;
	}

	public Knoten(final String name) {
		this.name = name;
		this.verbindungen = new LinkedList<>();
		this.entfernungen = new LinkedList<>();
	}

	public void addEntfernung(final Kante entfernung) {
		for (final Kante v : this.entfernungen) {
			if (v.equals(entfernung))
				return;
		}
		this.entfernungen.add(entfernung);
	}

	public void addVerbindung(final Kante verbindung) {
		for (final Kante v : this.verbindungen) {
			if (v.equals(verbindung))
				return;
		}
		this.verbindungen.add(verbindung);
	}

	public int calcH(final Knoten ziel) {
		for (final Kante entfernung : getEntfernungen()) {
			if (entfernung.getTarget(this).equals(ziel))
				return entfernung.getKosten();
		}
		return 0;
	}

	public boolean equals(final Knoten k) {
		if (k == null)
			return false;

		return k.name.equals(this.name);
	}

	public LinkedList<Kante> getEntfernungen() {
		return this.entfernungen;
	}

	public int getF() {
		return this.g + this.h;
	}

	public int getG() {
		return this.g;
	}

	public int getH() {
		return this.h;
	}

	public String getName() {
		return this.name;
	}

	public Knoten getParent() {
		return this.parent;
	}

	public LinkedList<Kante> getVerbindungen() {
		return this.verbindungen;
	}
}
