/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.astern;

import java.util.LinkedList;

public class AStern {

	private static boolean besserIn(final Knoten k, final LinkedList<Knoten> list) {
		for (final Knoten knoten : list) {
			if (knoten.equals(k) && (knoten.getF() <= k.getF()))
				return true;
		}

		return false;
	}

	private static Knoten getBesteF(final LinkedList<Knoten> liste) {
		Knoten beste = null;
		for (final Knoten k : liste) {
			if ((beste == null) || (k.getF() < beste.getF())) {
				beste = k;
			}
		}

		return beste;
	}

	public static Knoten getPfad(final Knoten start, final Knoten ziel, final LinkedList<Knoten> staedte) {

		final LinkedList<Knoten> zuPruefen = new LinkedList<>();
		final LinkedList<Knoten> geprueft = new LinkedList<>();

		zuPruefen.add(new Knoten(null, start, 0, 0));

		while (!zuPruefen.isEmpty()) {
			final Knoten q = getBesteF(zuPruefen);
			zuPruefen.remove(q);

			// Alle Verbindungen heraussuchen
			final LinkedList<Knoten> folgende = new LinkedList<>();
			for (final Kante k : q.getVerbindungen()) {
				// Entfernung zum Verbindungsziel heraussuchen
				// int h = q.calcH(ziel);
				// for (Kante entfernung : q.getEntfernungen()) {
				// if (entfernung.getTarget(q).equals(ziel)) {
				// h = entfernung.getKosten();
				// break;
				// }
				// }
				if (!geprueft.contains(k.getTarget(q))) {
					folgende.add(new Knoten(q, k.getTarget(q), q.getG() + k.getKosten(), q.calcH(ziel)));
				}
			}

			for (final Knoten knoten : folgende) {
				if (knoten.equals(ziel))
					return new Knoten(q, ziel, 0, 0);
				else if (!besserIn(knoten, zuPruefen) && !besserIn(knoten, geprueft)) {
					zuPruefen.add(knoten);
				}
			}

			geprueft.add(q);
		}

		return null;
	}
}
