package com.kata09.rules;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.kata09.IPricingRule;

/**
 * Implementierung, die ein JavaScript ausführt um den Preis zu berechnen.
 * Dadurch bleibt die PricingRule sehr flexibel und während des Betriebs
 * austauschbar.
 *
 * @author Dennis van der Wals
 *
 */
public class JSPricer implements IPricingRule {
	public class Console {
		public void log(String text) {
			System.out.println("console: " + text);
		}
	}

	private ScriptEngineManager factory;

	private File script;

	public JSPricer() {
		this.factory = new ScriptEngineManager();

		this.script = new File("pricing.js");
	}

	@Override
	public double getItemPrice(String item, int count) throws IllegalArgumentException {
		if (this.script != null) {

			ScriptEngine e = this.factory.getEngineByName("JavaScript");

			e.put("item", item);
			e.put("count", count);

			Console console = new Console();
			e.put("console", console);

			try {
				e.eval(new FileReader(this.script));
			} catch (ScriptException | FileNotFoundException e1) {
				e1.printStackTrace();
			}

			String errorMessage = (String) e.get("error");
			if (errorMessage != null) {
				throw new IllegalArgumentException(errorMessage);
			}

			return (double) e.get("price");
		}

		return 0;
	}

}
