package kata.rules.js;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.kata09.rules.JSPricer;

class JSTest {

	@Test
	void testGetItemPrice() {
		JSPricer p = new JSPricer();

		assertEquals(2.3, p.getItemPrice("A", 1));
	}

	@Test
	void testPrices() {
		JSPricer p = new JSPricer();

		assertEquals(2.3, p.getItemPrice("A", 1));
		assertEquals(4.6, p.getItemPrice("A", 2));
		assertEquals(7, p.getItemPrice("A", 3));
		assertEquals(1.5, p.getItemPrice("B", 1));
		assertEquals(2.5, p.getItemPrice("B", 2));
	}
}
