var priceMap = {
	A: {
		"1": 2.3,
		"3": 7
	},
	B: {
		"1": 1.5,
		"2": 2.5
	},
	C: {
		"1": 0.3,
		"5": 1,
		"10": 1.8
	},
	D: {
		"1": 23
	},
	"E": {
		"1": 9.75
	}
}

var price = 0;
var error = null;

while (count > 0) {
	var itemPrice = 0;
	var itemsPriced = 0;
	
	if (!priceMap[item]) {
		error = "The item " + item + " can not be priced by this rule.";
		break;
	}
	
	for (var number in priceMap[item]) {
		if (number <= count && number > itemsPriced) {
			itemPrice = priceMap[item][number];
			itemsPriced = number;
		}
	}
	
	count -= itemsPriced;
	price += itemPrice;
	
	if (itemsPriced === 0) {
		error = "Item " + item + " can not be priced for " + count + " pieces.";
	}
}