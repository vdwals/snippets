package com.kata09;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kata09.rules.SimpleRule;

/**
 * Vorschlag zur Lösung von
 * http://codekata.com/kata/kata09-back-to-the-checkout/. Implementiert das
 * Beispiel und mittels Plugins erweitert werden. Dazu muss ein Plugin
 * IPricingRule implementieren und als .jar in das Verzeichnis plugins kopiert
 * werden. Um das richtige Plugin auszuwählen, ist der Name der Klasse (inkl.
 * Paket) bei der Instanziierung von Checkout anzugeben. Die
 * Checkout-Implementierung weiß so nichts über die Preislogik und selbige kann
 * zukünftig erweitert werden.
 *
 * @author Dennis van der Wals
 *
 */
public class Checkout {
	/**
	 * Standardverzeichnis für Plugins
	 */
	private static final String PLUGINS_DIR = "plugins";

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(Checkout.class);

	/**
	 * Durchsucht den Pluginspfad nach Implementierungen von IPricingRule und gibt
	 * die Instanz zurück, deren Klassenname übergeben wurde. Wurde kein oder ein
	 * leerer Name übergeben, wird die Beispiel-Implementierung zurückgegeben.
	 *
	 * @param name Plugin-Klassenname inkl. Paket
	 * @return Instanz von IPricingRule oder <code>null</code>
	 */
	private static IPricingRule getPricingRuleOrNull(String name) {
		IPricingRule r = null;

		if ((name == null) || "".equals(name)) {
			LOG.info(MessageFormat.format("PricingRule plugin {0} loaded.", SimpleRule.class.getName()));
			r = new SimpleRule();
		}

		File pluginDir = new File(PLUGINS_DIR);

		if (pluginDir.exists() && pluginDir.isDirectory()) {
			// Load all files from plugin folder.
			File[] plugins = pluginDir.listFiles((FilenameFilter) (dir, name1) -> name1.endsWith(".jar"));

			URL[] urls = new URL[plugins.length + 1];
			urls[0] = Checkout.class.getProtectionDomain().getCodeSource().getLocation();

			for (int pluginIndex = 0; pluginIndex < plugins.length; pluginIndex++) {
				try {
					urls[pluginIndex + 1] = plugins[pluginIndex].toURI().toURL();
				} catch (MalformedURLException e) {
					LOG.warn("URL invalid: ", e);
				}

				LOG.debug(MessageFormat.format("Jar in plugin directory found: {0}", urls[pluginIndex + 1]));
			}

			URLClassLoader u = new URLClassLoader(urls);
			ServiceLoader<IPricingRule> s = ServiceLoader.load(IPricingRule.class, u);

			// Return only matching plugin-instance
			for (IPricingRule pricingRule : s) {
				if (name.equals(pricingRule.getClass().getName())) {
					LOG.info(MessageFormat.format("PricingRule plugin {0} loaded.", name));
					r = pricingRule;
					break;
				}
			}
		} else {
			LOG.info("Plugin directory is missing.");
		}

		return r;
	}

	private Map<String, Integer> cart;
	private IPricingRule pricingRule;

	/**
	 * Erzeugt eine neue Instanz mit der übergebenen IPricingRule.
	 *
	 * @param pricingRule Instanz einer IPricingRule-Implementierung
	 * @throws Exception, wenn keine Instanz übergeben wurde.
	 */
	public Checkout(IPricingRule pricingRule) throws IllegalArgumentException {
		if (pricingRule == null) {
			throw new IllegalArgumentException("PricingRule instance is missing.");
		}
		this.pricingRule = pricingRule;
		this.cart = new HashMap<>(26);
	}

	/**
	 * Erzeugt eine neue Instanz mit dem der Beispiel-Implementierung oder dem
	 * entsprechenden Plugin.
	 *
	 * @param pricingRule Leer oder <code>null</code> für die
	 *                    Beispiel-Implementierung, sonst name des Plugins.
	 */
	public Checkout(String pricingRule) {
		this.pricingRule = getPricingRuleOrNull(pricingRule);
		this.cart = new HashMap<>(26);
	}

	/**
	 * Liest das Objekt des Warenkorbs für die Bepreisung ein.
	 *
	 * @param item Objekt des Warenkorbs.
	 */
	public void scan(String item) {
		if (!this.cart.containsKey(item)) {
			this.cart.put(item, 0);
		}

		this.cart.put(item, this.cart.get(item) + 1);
	}

	/**
	 * Gibt den Preis der gescanten Objekte zurück.
	 *
	 * @return Preis nach IPricingRule-Implementierung.
	 */
	public double total() {
		double total = 0;

		for (String item : this.cart.keySet()) {
			try {
				total += this.pricingRule.getItemPrice(item, this.cart.get(item));
			} catch (Exception e) {
				LOG.error(MessageFormat.format("{0} could not be priced.", item));
			}
		}

		return total;
	}
}
