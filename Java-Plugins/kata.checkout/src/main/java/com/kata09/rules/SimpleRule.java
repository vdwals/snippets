package com.kata09.rules;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.kata09.IPricingRule;

public class SimpleRule implements IPricingRule {
	private Map<String, Map<Integer, Double>> skuPriceMap;

	public SimpleRule() {
		this.skuPriceMap = new HashMap<>(26);

		addToPriceMap("A", 1, 2.3);
		addToPriceMap("A", 3, 7);
		addToPriceMap("B", 1, 1.5);
		addToPriceMap("B", 2, 2.5);
		addToPriceMap("C", 1, 0.3);
		addToPriceMap("C", 5, 1);
		addToPriceMap("C", 10, 1.8);
		addToPriceMap("D", 1, 23);
		addToPriceMap("E", 1, 9.75);
		addToPriceMap("X", 2, 9.75);
	}

	public void addToPriceMap(String sku, int number, double amount) {
		if (!this.skuPriceMap.containsKey(sku)) {
			this.skuPriceMap.put(sku, new TreeMap<Integer, Double>());
		}

		this.skuPriceMap.get(sku).put(number, amount);
	}

	@Override
	public double getItemPrice(String item, int count) throws IllegalArgumentException {
		BigDecimal price = BigDecimal.valueOf(0);

		Map<Integer, Double> stockPriceMap = this.skuPriceMap.get(item);

		if (stockPriceMap == null) {
			throw new IllegalArgumentException("The item " + item + " can not be priced by this rule.");
		}

		List<Integer> stockSet = new LinkedList<>();
		stockSet.addAll(stockPriceMap.keySet());
		Collections.sort(stockSet);
		for (int i = stockSet.size() - 1; i >= 0; i--) {
			Integer stock = stockSet.get(i);

			while (count >= stock) {
				count -= stock;
				price = price.add(BigDecimal.valueOf(stockPriceMap.get(stock)));
			}
		}

		if (count != 0) {
			throw new IllegalArgumentException("Item " + item + " can not be priced for " + count + " pieces.");
		}

		return price.doubleValue();
	}

	public void setPriceMap(Map<String, Map<Integer, Double>> priceMap) {
		this.skuPriceMap = priceMap;
	}

}
