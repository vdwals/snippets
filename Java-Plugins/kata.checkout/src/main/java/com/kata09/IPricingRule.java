package com.kata09;

/**
 * Interface für die PricingRule, welche den Preis pro Gegenstand nach Stückzahl
 * berechnet.
 *
 * @author Dennis van der Wals
 *
 */
public interface IPricingRule {

	/**
	 * Gibt den Preis des Gegenstandes für die entsprechende Anzahl zurück.
	 *
	 * @param item  Einkaufsgegenstand.
	 * @param count Anzahl der gekauften Gegenstände.
	 * @return Preis der Gegenstände für die entsprechende Anzahl.
	 * @throws IllegalArgumentException wird erzeugt, wenn ein Gegenstand in der
	 *                                  Anzahl nicht von der Regel bepreist werden
	 *                                  kann.
	 */
	public double getItemPrice(String item, int count) throws IllegalArgumentException;
}
