package kata.checkout;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.kata09.Checkout;
import com.kata09.IPricingRule;
import com.kata09.rules.JSPricer;

class CheckoutTest {

	private void scan(Checkout c, String items) {
		for (int index = 0; index < items.length(); index++) {
			c.scan(items.charAt(index) + "");
		}
	}

	@Test
	void testException() {
		Checkout c = new Checkout("");

		c.scan("X");

		assertEquals(0, c.total());
	}

	@Test
	void testGeneral() {
		Checkout c = new Checkout("");

		c.scan("A");
		assertTrue(c.total() == 2.3);
	}

	@Test
	void testPlugin() {
		IPricingRule r = new JSPricer();
		Checkout c = new Checkout(r);

		c.scan("A");
		assertEquals(2.3, c.total());
	}

	@Test
	void testPluginByServiceLoader() {
		Checkout c = new Checkout("com.kata09.rules.JSPricer");

		c.scan("A");
		assertEquals(2.3, c.total());
	}

	@Test
	void testRandomOrder() {
		List<String> items = Arrays.asList(new String[] { "A", "A", "A", "A", "A", "A", "D" });

		for (int i = 0; i < 5; i++) {
			Collections.shuffle(items);
			Checkout c = new Checkout("");

			for (String item : items) {
				c.scan(item);
			}

			assertEquals(((2 * 7) + 23), c.total());
		}
	}

	@Test
	void testReal() {

		Checkout c = new Checkout("");

		c.scan("A");
		assertEquals(2.3, c.total());

		scan(c, "BA");
		assertEquals((2.3 + 2.3 + 1.5), c.total());

		c.scan("A");
		assertEquals((7 + 1.5), c.total());

		for (int i = 0; i < 4; i++) {
			c.scan("C");
		}
		assertEquals((7 + 1.5 + (4 * 0.3)), c.total());

		c.scan("C");
		assertEquals((7 + 1.5 + 1), c.total());
	}

	@Test
	void testRebates() {
		Checkout c = new Checkout("");

		scan(c, "AAA");
		assertEquals(7, c.total());

		scan(c, "AA");
		assertEquals((7 + (2 * 2.3)), c.total());

		scan(c, "AA");
		assertEquals(((2 * 7) + 2.3), c.total());

		c = new Checkout("");

		scan(c, "CCCCC");
		assertEquals(1, c.total());
		scan(c, "CCCC");
		assertEquals(2.2, c.total());
		c.scan("C");
		assertEquals(1.8, c.total());
	}

	@Test
	void testSummation() {
		Checkout c = new Checkout("");

		scan(c, "ABCDE");

		assertEquals((2.3 + 1.5 + 0.3 + 23 + 9.75), c.total());
	}

}
