/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.bugReport;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import de.etern.it.properties.Property;

/**
 * Klasse zum Aufnehmen und Abspeichern von Fehlermeldungen.
 *
 * @author Dennis van der Wals
 *
 */
public class BugReport {
	private static File report = new File("stack.rep"); //$NON-NLS-1$

	public static String bugAdd, appName;

	/**
	 * Speichert alle relevanten Daten zu einer Exception und versucht diese an
	 * den Server zu uebermitteln.
	 *
	 * @param e
	 *            Exception
	 */
	public static void recordBug(Exception e, boolean simple) {
		// e.printStackTrace();

		if (!Property.getBoolean("bug_tracking"))
			return;

		// Fehlermeldung erzeugen
		StringBuilder error = new StringBuilder();
		error.append("**************Neuer Fehler**************\n"); //$NON-NLS-1$
		error.append(new SimpleDateFormat().format(Calendar.getInstance().getTime()) + "\n"); //$NON-NLS-1$
		error.append("Fehler: \t" + e.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		error.append("Nachricht: \t" + e.getMessage() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		error.append("Ausloeser: \t" + e.getCause() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$

		if (!simple) {
			error.append("-------Details-------\n"); //$NON-NLS-1$

			// Stack hinzufuegen
			int counter = 1;
			for (StackTraceElement element : e.getStackTrace()) {
				error.append("Element: \t" + (counter++) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				error.append("Datei: \t\t" + element.getFileName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				error.append("Klasse: \t" + element.getClassName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				error.append("Methode: \t" + element.getMethodName() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				error.append("Zeile: \t\t" + element.getLineNumber() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
				error.append("-------\n"); //$NON-NLS-1$
			}
		}

		try {

			// Fehlermeldung hochladen
			upload(new Scanner(error.toString()));

			/*
			 * Wenn eine Fehlermeldungsdatei existiert und Fehler beinhaltet,
			 * lade diese auch hoch
			 */
			if (!report.createNewFile() && (report.length() > 0)) {
				upload();
			}

		} catch (Exception e2) {

			// Wenn Server nicht erreichbar, Fehler in Datei speichern
			try {

				report.createNewFile();
				FileWriter fw = new FileWriter(report, true);
				fw.append(error.toString());
				fw.close();

			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	/**
	 * Laedt die Fehlerdatei hoch
	 *
	 * @return erfolgreicher Upload, oder nicht
	 */
	private static boolean upload() {
		try {
			// Lade Datei hoch
			upload(new Scanner(report));

			// Loesche den Inhalt der Datei
			FileWriter fw = new FileWriter(report, false);
			fw.write(""); //$NON-NLS-1$
			fw.close();

			// Loesche die Datei
			report.delete();

			// Gib Erfolgsmeldung zurueck.
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Uebermittelt den Dateiinhalt an den Server
	 *
	 * @throws java.io.IOException
	 * @throws java.io.UnsupportedEncodingException
	 * @throws java.net.MalformedURLException
	 */
	private static void upload(Scanner scan) throws MalformedURLException, UnsupportedEncodingException, IOException {

		if ((bugAdd != null) && (appName != null)) {
			// Zeile fuer Zeile versenden
			while (scan.hasNextLine()) {
				new URL(bugAdd + "?app=" + appName + "&text=" //$NON-NLS-1$ //$NON-NLS-2$
						+ URLEncoder.encode(scan.nextLine(), "UTF-8")).openStream(); //$NON-NLS-1$
			}
		}
		scan.close();
	}
}
