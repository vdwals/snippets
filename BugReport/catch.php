<?php

//Ansprechen mit: 100.100.100.100/apps/bugreport/catch.php?app=Reaktor&text=test

//Variablen auslesen
$ip = $_SERVER['REMOTE_ADDR'];
$app = $_REQUEST['app'];
$inhalt = $_REQUEST['text'];

//nur gueltige Apps durchlassen
if (strcmp($app, "Reaktor") == 0 || strcmp($app, "F3Hack") == 0 || strcmp($app, "TextCrypt") == 0) {

	//Ueberpruefe, ob IP ermittelt
	if (strcmp($ip, "") == 0) {
		$ip = "anonym";
	}

	//Verzeichnis anlegen, falls notwendig
	mkdir("./$app/");

	//Datei �ffnen
	$datei = fopen("./$app/$ip.rep","a+");

	//Start des Reports
	if (strcmp($start, "1") == 0) {
		//IP anh�ngen
		fputs($datei, "$ip\n");
	}

	//uebertragene Zeile einfuegen
	fputs($datei, urldecode($inhalt)."\n");

	//Datei schlie�en
	fclose($datei);
}
?>
