/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.leastsquares;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Jama.Matrix;

/**
 * Loest die Aufgaben
 *
 * @author Dennis van der Wals, Max van gen Hassend
 */
public class LeastSquares {

	/**
	 * Berechnet die SigmaMatrix der Unbekannten
	 * 
	 * @param A
	 *            Matrix
	 * @param Q
	 *            Matrix
	 * @param varianz
	 *            Varianz
	 * @return SigmaMatrix
	 */
	public static Matrix abweichung(Matrix A, Matrix Q, double varianz) {

		return A.transpose().times(Q.inverse()).times(A).inverse().times(varianz);
	}

	/**
	 * Loest die Aufgaben
	 * 
	 * @param eingabe
	 *            Eingabestring
	 */
	public static void berechneAufgaben(String eingabe) {
		Scanner eingabeScanner = new Scanner(eingabe);

		// Eingabe
		int N, M;
		Matrix A = null;
		Matrix Q = null;
		double[] messungen = null;

		try {
			N = eingabeScanner.nextInt();
			M = eingabeScanner.nextInt();

			A = new Matrix(M - 1, N - 1);
			Q = new Matrix(M - 1, M - 1);
			messungen = new double[M - 1];

			eingabeScanner.nextLine();
			for (int eingabeZeile = 0; eingabeZeile < (M - 1); eingabeZeile++) {
				Scanner zeilenScanner = new Scanner(eingabeScanner.nextLine());
				// Erkenne Dezimalpunkte
				zeilenScanner.useLocale(Locale.US);

				int start = zeilenScanner.nextInt() - 2;
				int ziel = zeilenScanner.nextInt() - 2;
				messungen[eingabeZeile] = zeilenScanner.nextDouble();
				Q.set(eingabeZeile, eingabeZeile, zeilenScanner.nextDouble());

				if (start >= 0) {
					A.set(eingabeZeile, start, -1);
				}

				if (ziel >= 0) {
					A.set(eingabeZeile, ziel, 1);
				}

				zeilenScanner.close();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ungueltige Eingabe");
			eingabeScanner.close();
			return;
		}

		eingabeScanner.close();

		// DEBUG
		// for (int i = 0; i < a.length; i++) {
		// for (int j = 0; j < a[i].length; j++) {
		// System.out.print(a[i][j] + "\t");
		// }
		// System.out.println();
		// }

		// Matrizen erzeugen
		Matrix L = new Matrix(messungen, 1);
		L = L.transpose();

		// DEBUG
		// L.print(5, 3);
		// A.print(5, 3);

		// Aufgabe a)
		Matrix X_hat = loeseGausscheElimination(A.transpose().times(A), A.transpose().times(L));

		System.out.println("L�sung a)");
		System.out.println("X: ");
		X_hat.print(5, 3);

		System.out.println("L: ");
		Matrix L_hat = A.times(X_hat);
		L_hat.print(5, 3);

		// Aufgabe b)
		double varianzNull = genauigkeit(L, L_hat, Q, M, N);

		System.out.println("L�sung b)");
		System.out.println("Standardabweichung der Gewichtseinheit: " + Math.sqrt(varianzNull));

		Matrix abweichungen = abweichung(A, Q, varianzNull);

		System.out.println("Standardabweichungen der Hoehen:");
		getDiagonaleWurzel(abweichungen).print(5, 3);

		System.out.println("Standardabweichungen der Messungen:");
		getDiagonaleWurzel(A.times(abweichungen).times(A.transpose())).print(5, 3);

		// Aufgabe c)

		// double[] beispielHoehen = { 0, 12, 7, 8.8, 9, 5, -2, -1.1 };

		// Erstelle Matrix mit korrekten Messungen

	}

	/**
	 * Berechnet die Varianz der Gewichtseinheit
	 * 
	 * @param messungen
	 *            Vektor gemessener Werte
	 * @param ausgegelicheneMessungen
	 *            Vektor korrigierter Messungen
	 * @param Q
	 *            Kofaktormatrix
	 * @param anzahlMessungen
	 *            Anzahl der Messungen
	 * @param anzahlUnbekannte
	 *            Anzahl der Unbekannten
	 * @return Varianz der Gewichtseinheit (sigma�)
	 */
	public static double genauigkeit(Matrix messungen, Matrix ausgegelicheneMessungen, Matrix Q, int anzahlMessungen,
			int anzahlUnbekannte) {

		Matrix v = ausgegelicheneMessungen.minus(messungen);

		return v.transpose().times(Q.inverse()).times(v).get(0, 0) / (anzahlMessungen - anzahlUnbekannte);
	}

	/**
	 * Gibt einen Vektor zurueck, der die Wurzeln der Diagonalen der Matrix A
	 * enthaelt.
	 * 
	 * @param A
	 *            Matrix
	 * @return Diagonale
	 */
	public static Matrix getDiagonaleWurzel(Matrix A) {
		Matrix diagonaleWurzel = new Matrix(A.getColumnDimension(), 1);
		for (int hoehe = 0; hoehe < A.getColumnDimension(); hoehe++) {
			diagonaleWurzel.set(hoehe, 0, Math.sqrt(A.get(hoehe, hoehe)));
		}

		return diagonaleWurzel;
	}

	/**
	 * Loest durch Gausssche Elimination das Gleichungssystem in der Normalform.
	 * 
	 * @param L
	 *            Linke Seite der Gleichung
	 * @param R
	 *            Rechte Seite der Gleichung
	 * @return Loesungsvektor
	 */
	public static Matrix loeseGausscheElimination(Matrix L, Matrix R) {
		if (R.getColumnDimension() > 1)
			return null;

		if (R.getRowDimension() != L.getRowDimension())
			return null;

		// Kopiere Matrix
		Matrix loesung = new Matrix(L.getRowDimension(), L.getColumnDimension() + 1);
		loesung.setMatrix(0, L.getRowDimension() - 1, 0, L.getColumnDimension() - 1, L);
		loesung.setMatrix(0, L.getRowDimension() - 1, L.getColumnDimension(), L.getColumnDimension(), R);

		// DEBUG
		// System.out.println("Zu loesende Matrix: ");
		// loesung.print(4, 2);

		Matrix aktuelleZeile = null;
		Matrix folgendeZeile = null;

		// Gausscher Algorithmus
		for (int zeile = 0; zeile < loesung.getRowDimension(); zeile++) {

			// Lese aktuelle Zeile ein
			aktuelleZeile = loesung.getMatrix(zeile, zeile, 0, loesung.getColumnDimension() - 1);

			// DEBUG
			// System.out.println("Aktuelle Zeile: ");
			// aktuelleZeile.print(5, 3);

			/*
			 * Pruefe ob Wert fuer aktuelle Zeile gleich 0 und suche Ersatzzeile
			 */
			int tauschZeilenIndex = zeile;
			while ((loesung.get(tauschZeilenIndex, zeile) == 0) && (tauschZeilenIndex < loesung.getRowDimension())) {
				tauschZeilenIndex++;
			}

			// Gib nichts zurueck, wenn nicht eindeutig loesbar.
			if (tauschZeilenIndex == loesung.getRowDimension())
				return null;

			// Vertausche Zeilen wenn notwendig.
			if (tauschZeilenIndex != zeile) {
				// DEBUG
				// System.out.println("Vor tausch: ");
				// loesung.print(5, 3);

				folgendeZeile = loesung.getMatrix(tauschZeilenIndex, tauschZeilenIndex, 0,
						loesung.getColumnDimension() - 1);
				loesung.setMatrix(tauschZeilenIndex, tauschZeilenIndex, 0, loesung.getColumnDimension() - 1,
						aktuelleZeile);
				loesung.setMatrix(zeile, zeile, 0, loesung.getColumnDimension() - 1, folgendeZeile);

				aktuelleZeile = folgendeZeile;

				// DEBUG
				// System.out.println("Nach tausch: ");
				// loesung.print(5, 3);
				// System.out.println("Aktuelle Zeile: ");
				// aktuelleZeile.print(5, 3);
			}

			aktuelleZeile = aktuelleZeile.times(1 / aktuelleZeile.get(0, zeile));

			// DEBUG
			// System.out.println("Nach Division: ");
			// aktuelleZeile.print(5, 3);

			// Schreibe aktuelle Zeile zurueck
			loesung.setMatrix(zeile, zeile, 0, loesung.getColumnDimension() - 1, aktuelleZeile);

			for (int folgenderZeilenIndex = 0; folgenderZeilenIndex < loesung
					.getRowDimension(); folgenderZeilenIndex++) {

				/*
				 * Pruefe ob Zahl in gleicher Spalte schon 0 ist und nicht die
				 * aktuelle Zeile geprueft wird
				 */
				if ((loesung.get(folgenderZeilenIndex, zeile) != 0) && (folgenderZeilenIndex != zeile)) {

					/*
					 * Hohle zu aendernde Zeile aus Matrix. Alternativ haette
					 * auch die zu addierende Zeile zu einer in den uebrigen
					 * Zeilen mit 0 aufgefuellten Matrix aufgeblaeht werden
					 * koennen, aber das schien mir gleich umstaendlich.
					 */
					folgendeZeile = loesung.getMatrix(folgenderZeilenIndex, folgenderZeilenIndex, 0,
							loesung.getColumnDimension() - 1);

					// DEBUG
					// System.out.println("Zu aendernde Zeile: ");
					// folgendeZeile.print(5, 3);

					// Bestimme Faktor fuer aktuelleZeile
					double faktor = (-1 * folgendeZeile.get(0, zeile)) / aktuelleZeile.get(0, zeile);

					// DEBUG
					// System.out.println("Faktor: " + faktor);

					folgendeZeile = folgendeZeile.plus(aktuelleZeile.times(faktor));

					// DEBUG
					// System.out.println("Nach Aenderung: ");
					// folgendeZeile.print(5, 3);

					// Zeile wieder in Matrix einfuegen
					loesung.setMatrix(folgenderZeilenIndex, folgenderZeilenIndex, 0, loesung.getColumnDimension() - 1,
							folgendeZeile);
				}

			}
			// DEBUG
			// System.out.println("Matrix: ");
			// loesung.print(5, 3);
		}

		return loesung.getMatrix(0, loesung.getRowDimension() - 1, loesung.getColumnDimension() - 1,
				loesung.getColumnDimension() - 1);
	}

	public static void main(String[] args) {
		JPanel panel = new JPanel(new GridLayout(1, 1));
		panel.setPreferredSize(new Dimension(200, 200));

		JEditorPane eingabe = new JEditorPane();
		eingabe.setText("4\n6\n1 2 4.1 1\n2 3 -7 1\n3 4 1.1 1\n4 1 1.2 1\n4 2 5.4 1");
		panel.add(eingabe);

		int wahl = JOptionPane.showConfirmDialog(null, panel, "Messungen", JOptionPane.OK_CANCEL_OPTION);

		if (wahl == JOptionPane.OK_OPTION) {
			berechneAufgaben(eingabe.getText());
		}
	}
}
