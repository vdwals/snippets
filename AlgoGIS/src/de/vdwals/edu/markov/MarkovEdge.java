/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.markov;

/**
 * Klasse zum Speichern eine Kante in der Markovkette.
 *
 * @author Dennis van der Wals
 *
 */
public class MarkovEdge {
	private MarkovNode vorgaenger, nachfolger;
	private double kosten;

	/**
	 * Standardkonstruktor
	 * 
	 * @param vorgaenger
	 *            Quellknoten
	 * @param nachfolger
	 *            Zielknoten
	 * @param kosten
	 *            Pfadkosten
	 */
	public MarkovEdge(MarkovNode vorgaenger, MarkovNode nachfolger, double kosten) {
		this.vorgaenger = vorgaenger;
		this.nachfolger = nachfolger;
		this.kosten = kosten;
	}

	/**
	 * @return the kosten
	 */
	public double getKosten() {
		return this.kosten;
	}

	/**
	 * @return the nachfolger
	 */
	public MarkovNode getNachfolger() {
		return this.nachfolger;
	}

	/**
	 * @return the vorgaenger
	 */
	public MarkovNode getVorgaenger() {
		return this.vorgaenger;
	}

}
