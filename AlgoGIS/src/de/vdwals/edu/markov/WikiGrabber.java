/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.markov;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Klasse zum sammeln von Beispieltexten (Wikipedia oder Welt.de)
 *
 * @author Dennis van der Wals
 *
 */
public class WikiGrabber {

	public static void collectTextFromWelt(int length) {
		int size = 0;

		LinkedList<String> links = new LinkedList<>();
		links.add(
				"http://www.welt.de/debatte/kommentare/article127965665/Auch-im-Internet-gibt-es-ein-Recht-auf-Vergessen.html");

		LinkedList<String> besuchteLinks = new LinkedList<>();
		while ((size < length) && (links.size() > 0)) {
			try {
				String aktuellerLink = links.removeFirst();
				Document doc = Jsoup.connect(aktuellerLink).get();
				besuchteLinks.add(aktuellerLink);

				String inhalt = doc.select("div[class*=widget storyBody default]").get(0).text();
				size += inhalt.length();
				System.out.println(size + " - " + aktuellerLink + ": " + inhalt);

				BufferedWriter bw = new BufferedWriter(new FileWriter(new File("text.txt"), true));
				bw.write(inhalt);
				bw.newLine();
				bw.close();

				Elements artikellinks = doc.select("a[class=news]");
				for (Element element : artikellinks) {
					String neuerLink = element.toString();
					neuerLink = neuerLink.substring(neuerLink.indexOf("\"") + 1);
					neuerLink = neuerLink.substring(0, neuerLink.indexOf("\""));

					if (!besuchteLinks.contains(neuerLink) && !links.contains(neuerLink)) {
						// System.out.println("Neuer Link: " + neuerLink);
						links.add(neuerLink);
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void collectTextFromWiki(int length) {
		int size = 0;

		LinkedList<String> links = new LinkedList<>();
		links.add("http://de.wikipedia.org/wiki/Recht");

		LinkedList<String> besuchteLinks = new LinkedList<>();
		while ((size < length) && (links.size() > 0)) {
			try {
				String aktuellerLink = links.removeFirst();
				Document doc = Jsoup.connect(aktuellerLink).get();
				besuchteLinks.add(aktuellerLink);
				String inhalt = doc.body().text().replaceAll("\\s+", " ");
				System.out.println(inhalt);
				size += inhalt.length();

				BufferedWriter bw = new BufferedWriter(new FileWriter(new File("text.txt"), true));
				bw.write(inhalt);
				bw.newLine();
				bw.close();

				Elements wikilinks = doc.select("a[href]");
				for (Element element : wikilinks) {
					if (element.toString().contains("\"/wiki/") && !element.toString().contains("accesskey")
							&& !element.toString().contains(":")) {
						String neuerLink = element.toString();
						neuerLink = neuerLink.substring(neuerLink.indexOf("\"") + 1);
						neuerLink = neuerLink.substring(0, neuerLink.indexOf("\""));
						neuerLink = "http://de.wikipedia.org" + neuerLink;

						if (!besuchteLinks.contains(neuerLink) && !links.contains(neuerLink)) {
							links.add(neuerLink);
						}
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		try {
			new File("text.txt").createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		collectTextFromWelt(500000);
	}
}
