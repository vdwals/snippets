/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.markov;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Einfaches T9 unterstuezt nur Standard-lateinisches Alphabet.
 *
 * @author Dennis van der Wals, Max van gen Hassend
 *
 */
public class T9simple {
	private static char[][] keyLetters = { { 32 }, { 65, 66, 67 }, { 68, 69, 70 }, { 71, 72, 73 }, { 74, 75, 76 },
			{ 77, 78, 79 }, { 80, 81, 82, 83 }, { 84, 85, 86 }, { 87, 88, 89, 90 } };

	public static void main(String[] args) throws IOException {

		if (new File("text.txt").createNewFile()) {
			WikiGrabber.collectTextFromWelt(500000);
		}

		T9simple t9 = new T9simple("text.txt");

		String beispieltext = "Dies ist der zu entschluesselnde Beispieltext";
		Scanner scanner = new Scanner(System.in);

		while (!beispieltext.equals("STOP")) {
			beispieltext = scanner.nextLine();

			int[] keys = t9.getKeys(beispieltext);

			for (int i = 0; i < beispieltext.length(); i++) {
				System.out.print(beispieltext.charAt(i) + " ");
			}
			System.out.println();
			for (int key : keys) {
				System.out.print(key + " ");
			}
			System.out.println();

			System.out.println(t9.getT9text(keys));
		}

		scanner.close();

	}

	private double[][] propsLetter2Letter, propsKeys2Letter;

	/**
	 * Erzeugt eine neue Instanz fuer eine einfache T9-Texterkennung
	 *
	 * @param beispieltext
	 *            Pfad zum Quelltext zur wahrscheinlichkeitsbestimmung
	 */
	public T9simple(String beispieltext) {
		this.propsKeys2Letter = new double[9][27];

		// Tastatur und Buchstaben mappen
		for (int key = 0; key < this.propsKeys2Letter.length; key++) {
			for (int letter = 0; letter < keyLetters[key].length; letter++) {
				this.propsKeys2Letter[key][charToIndex(keyLetters[key][letter])] = 1.0 / keyLetters[key].length;

				// System.out
				// .println("P("
				// + keyLetters[key][letter]
				// + "|"
				// + indexToKey(key)
				// + "= "
				// +
				// propsKeys2Letter[key][charToIndex(keyLetters[key][letter])]);
			}
		}

		// Beispieltext mappen
		try {
			Scanner scan = new Scanner(new File(beispieltext));

			StringBuilder sb = new StringBuilder();
			while (scan.hasNextLine()) {
				sb.append(scan.nextLine());
			}

			scan.close();

			erstelleBuchstabenMatrix(sb.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// System.out.print("\t |");
		// for (int i = 0; i < propsLetter2Letter.length; i++) {
		// System.out.print(indexToChar(i) + "\t|");
		// }
		// System.out.println();
		//
		// for (int i = 0; i < propsLetter2Letter.length; i++) {
		// System.out.print(indexToChar(i) + "\t|");
		// for (int j = 0; j < propsLetter2Letter[i].length; j++) {
		// System.out.print(String
		// .format("%03.2f", propsLetter2Letter[i][j] * 100) + "\t|");
		// }
		// System.out.println();
		// }
	}

	/**
	 * Gibt zu einem Character den Index fuer die verwendeten Arrays zurueck.
	 * Dazu wird der ASCII wert einfach umgerechnet (Beispiel: A = (ASCII) 65,
	 * Index = 1 = 65 - 64 (Erste Spalte bzw. Zeile im Array)). Klein- und
	 * Gro�buchstaben werden gleich behandelt. Aussnahme: Alle Sonderzeichen
	 * sind Index = 0 (Leerzeichen)
	 *
	 * @param c
	 *            Character dessen Index berechnet wird.
	 * @return Index des Characters; Spalte/Zeile, die fuer den Character steht.
	 */
	private int charToIndex(char c) {
		int tmp = c;
		if ((tmp < 91) && (tmp > 64))
			return tmp - 64;
		else if ((tmp < 123) && (tmp > 96))
			return tmp - 96;

		return 0;
	}

	/**
	 * Erstellt eine Wahrscheinlichkeitsmatrix fuer Buchstabenkombinationen aus
	 * einem uebergebenem Text.
	 *
	 * @param text
	 *            Text, aus dem die Haeufigkeiten von Buchstabenpaaren gelesen
	 *            wird.
	 */
	public void erstelleBuchstabenMatrix(String text) {
		if ((text == null) || (text.length() == 0))
			return;

		this.propsLetter2Letter = new double[27][27];

		// Zaehlmatrix initialisieren
		int[][] haeufigkeitsmatrix = new int[27][27];

		int zeile = charToIndex(text.charAt(0));
		@SuppressWarnings("unused")
		int gesamt = 0;

		for (int i = 1; i < (text.length() - 1); i++) {

			// Naechsten Buchstaben bestimmen
			int spalte = charToIndex(text.charAt(i));

			/*
			 * Wenn Vorgaenger und Aktueller Character ein Buchstabe sind,
			 * zaehlen.
			 */
			if ((spalte != -1) && (zeile != -1)) {
				haeufigkeitsmatrix[zeile][spalte]++;
				gesamt++;
			}

			zeile = spalte;
		}

		// Relative Haeufigkeiten eintragen.
		for (int i = 0; i < haeufigkeitsmatrix.length; i++) {
			// Anzahl der Buchstaben bestimmen, die auf i folgten.
			int buchstaben = 0;
			for (int j = 0; j < haeufigkeitsmatrix[i].length; j++) {
				buchstaben += haeufigkeitsmatrix[i][j];
			}

			// DEBUG
			// System.out.println("Buchstaben fuer " + indexToChar(i) + ": "
			// + buchstaben);
			// gesamt += buchstaben;

			// Anzahl der gefunden Folgenden durch Anzahl aller folgenden Teilen
			for (int j = 0; j < haeufigkeitsmatrix[i].length; j++) {
				this.propsLetter2Letter[i][j] = ((double) haeufigkeitsmatrix[i][j]) / ((double) buchstaben);
			}
		}

		// DEBUG
		// System.out.println("Buchstaben gesamt= " + gesamt);
	}

	/**
	 * Gibt zu einem Character die Taste zurueck, die diesen enthaelt.
	 *
	 * @param c
	 *            Character
	 * @return Taste von Zifferntastatur, die den Buchstaben abbildet (1-9)
	 */
	private int getKey(char c) {
		/*
		 * Pruefe alle Wahrscheinlichkeiten und gibt die Taste zurueck, fuer die
		 * gilt P(c|Taste) > 0; da jeder Buchstabe nur einer Taste zugeordnet
		 * ist, ist das Ergebnis eindeutig.
		 */
		for (int i = 0; i < this.propsKeys2Letter.length; i++) {
			if (this.propsKeys2Letter[i][charToIndex(c)] > 0)
				return indexToKey(i);
		}
		return 0;
	}

	/**
	 * Ubersetzt einen uebergebenen String in Handytasten von 1-9.
	 *
	 * @param text
	 *            Zu dechiffrierender Text
	 * @return int Array der gedrueckten Tasten von 1-9 (nicht bei 0 beginnend!)
	 */
	private int[] getKeys(String text) {
		int[] keys = new int[text.length()];

		for (int i = 0; i < keys.length; i++) {
			keys[i] = getKey(text.charAt(i));
		}

		return keys;
	}

	/**
	 * Gibt einen Text zurueck, der die hoechste Wahrscheinlichkeit hat von den
	 * gedrueckten Tasten abgebildet zu werden.
	 *
	 * @param keys
	 *            gedrueckte Tasten
	 * @return Text, der dadurch entstehen kann
	 */
	public String getT9text(int[] keys) {
		String text = "";

		// Startknoten mit Leerzeichen erstellen
		MarkovNode start = new MarkovNode((char) 32);

		// Liste mit Vorgaengerknoten erstellen
		ArrayList<MarkovNode> vorgaenger = new ArrayList<>();
		vorgaenger.add(start);

		ArrayList<MarkovNode> nachfolger = new ArrayList<>();

		// Erstelle Wahrscheinlichkeitsbaum
		for (int key : keys) {

			// Alle Knoten zu einer Taste erstellen
			for (int j = 0; j < this.propsKeys2Letter[0].length; j++) {

				/*
				 * Wenn der gepruefte Buchstabe eine Wahrscheinlichkeit groesser
				 * 0 hat zu der Taste zu gehoeren, Knoten und Kanten dazu
				 * erstellen.
				 */
				if (this.propsKeys2Letter[keyToIndex(key)][j] > 0) {
					// Knoten ertellen
					MarkovNode letter2Key = new MarkovNode(indexToChar(j));

					// Kanten zu allen vorgaengerknoten erstellen
					for (MarkovNode markovNode : vorgaenger) {
						/*
						 * Die Kante erhaelt den VorgaengerKnoten und wird in
						 * diesem gespeichert, sowie die Kosten. Die Kosten
						 * berechnen sich aus dem Logarithmus des Produkts der
						 * Wahrscheinlichkeit, dass der Nachfolgende Buchstaben
						 * aus dem Vorgehendem folgt (Matrix:
						 * propsLetter2Letter[vorgaenger][nachfolger]), und der
						 * Wahrscheinlichkeit, dass der Buchstabe zu der Taste
						 * gehoert (Matrix: letter2Key[taste][nachfolger]).
						 */
						double kosten = this.propsLetter2Letter[charToIndex(markovNode.getLetter())][charToIndex(
								letter2Key.getLetter())];
						kosten *= this.propsKeys2Letter[keyToIndex(key)][j];
						kosten = Math.log(kosten);

						// DEBUG
						// System.out.println("Kosten: " +
						// markovNode.getLetter()
						// + "-" + keys[i] + ":" + letter2Key.getLetter()
						// + " = " + String.format("%.2f", kosten));

						MarkovEdge kante = new MarkovEdge(markovNode, letter2Key, kosten);

						markovNode.getKantenNachfolger().add(kante);
						letter2Key.getKantenVorgaenger().add(kante);
					}

					/*
					 * Knoten fuer naechsten Buchstaben als Vorgaenger
					 * abspeichern.
					 */
					nachfolger.add(letter2Key);
				}
			}

			/*
			 * Bevor naechste Taste verarbeitet wird, Listen der Vorgaenger und
			 * Nachfolger vorbereiten
			 */
			vorgaenger.clear();
			vorgaenger.addAll(nachfolger);
			nachfolger.clear();
		}

		// Baum mit 'Wortende' (Leerzeichen) Abschlie�en.
		if (keys[keys.length - 1] != 1) {
			MarkovNode end = new MarkovNode((char) 32);

			// Kanten zu allen vorgaengerknoten erstellen
			for (MarkovNode markovNode : vorgaenger) {
				MarkovEdge letzteKante = new MarkovEdge(markovNode, end,
						this.propsLetter2Letter[charToIndex(markovNode.getLetter())][0]);
				markovNode.getKantenNachfolger().add(letzteKante);
				end.getKantenVorgaenger().add(letzteKante);
				// DEBUG
				// System.out.println("Kosten: "
				// + markovNode.getLetter()
				// + "-"
				// + end.getLetter()
				// + " = "
				// + String.format("%.2f",
				// propsLetter2Letter[charToIndex(markovNode
				// .getLetter())][0]));
			}
		}

		// Endknoten des laengsten Pfades durch den Baum bestimmen.
		MarkovNode ende = longestPath(start);

		// Text auslesen
		while (ende != null) {
			text = ende.getLetter() + text;
			ende = ende.getVorgaenger();
		}

		return text;
	}

	/**
	 * Gibt zu einem Index den Character fuer die verwendeten Arrays zurueck.
	 * Dazu wird der ASCII Wert einfach umgerechnet (Beispiel:Index = 1 (Erste
	 * Spalte bzw. Zeile im Array); A = (ASCII) 65 = 1 + 64 (Erste Spalte bzw.
	 * Zeile im Array)) Aussnahme = Index 0 = Leerzeichen
	 *
	 * @param i
	 * @return
	 */
	private char indexToChar(int i) {
		if (i == 0)
			return (char) 32;

		return (char) (i + 64);
	}

	/**
	 * Gibt die Taste zurueck, die zum Index gehoert (Index = 8, Taste = 9 = 8 +
	 * 1 (8. Zeile/Spalte steht fuer Taste 9)
	 *
	 * @param index
	 *            Index
	 * @return Taste, die durch Index dargestellt wird.
	 */
	private int indexToKey(int index) {
		return index + 1;
	}

	/**
	 * Gibt den Index zu einer Taste zurueck (Taste = 1, Index = 0 = 1 - 1 (1.
	 * Zeile/Spalte im Array).
	 *
	 * @param key
	 *            Taste
	 * @return Index eines Arrays, das die Taste repraesentiert.
	 */
	private int keyToIndex(int key) {
		return key - 1;
	}

	/**
	 * Geht einen Baum durch und bestimmt den laengsten Pfad in diesem Graphen.
	 * Gibt den Letzten Knoten des Baumes zurueck (Das Ende des laengsten
	 * Pfades)
	 *
	 * @param start
	 *            Erster Markovknoten im Baum
	 * @return Letzten Markov Knoten im Baum mit aufgefuellten Informationen zum
	 *         laengsten Pfad.
	 */
	private MarkovNode longestPath(MarkovNode start) {
		// Liste aller Knoten erstellen
		ArrayList<MarkovNode> knoten = new ArrayList<>();
		ArrayList<MarkovNode> toCheck = new ArrayList<>();
		toCheck.add(start);

		while (!toCheck.isEmpty()) {
			MarkovNode check = toCheck.remove(0);

			// DEBUG
			// System.out.println("Pruefe Knoten: " + check.getLetter());

			/*
			 * Nur die Nachfolgenden Knoten des ersten Knoten pruefen, da alle
			 * Knoten einer Zeile mit allen Knoten der naechsten Zeile verbunden
			 * sind, muss nur ein Knoten je Zeile auf seine Nachfolger geprueft
			 * werden. Dies vermeidet doppelte Eintraege
			 */
			if (check.getKantenNachfolger().size() > 0) {
				toCheck.add(check.getKantenNachfolger().get(0).getNachfolger());
			}

			for (MarkovEdge kante : check.getKantenNachfolger()) {
				knoten.add(kante.getNachfolger());
			}
		}

		// DEBUG
		// System.out.println(knoten.size());

		// Gehe durch alle Knoten und berechne laengsten Weg und Vorgaenger
		knoten.get(0).setDistance(0);
		knoten.get(0).setVorgaenger(null);
		for (int j = 1; j < knoten.size(); j++) {
			knoten.get(j).setDistance(Double.NEGATIVE_INFINITY);
			knoten.get(j).setVorgaenger(null);

			for (MarkovEdge kanteZuVorgaenger : knoten.get(j).getKantenVorgaenger()) {

				// DEBUG
				// System.out.println("Knoten-I: "
				// + kanteZuVorgaenger.getVorgaenger().getLetter() + " - "
				// + kanteZuVorgaenger.getVorgaenger().getDistance());
				// System.out.println("Kosten: " +
				// kanteZuVorgaenger.getKosten());
				// System.out.println("Knoten-" + j + ": "
				// + knoten.get(j).getDistance());

				if ((kanteZuVorgaenger.getVorgaenger().getDistance() + kanteZuVorgaenger.getKosten()) > knoten.get(j)
						.getDistance()) {
					knoten.get(j).setDistance(
							kanteZuVorgaenger.getVorgaenger().getDistance() + kanteZuVorgaenger.getKosten());
					knoten.get(j).setVorgaenger(kanteZuVorgaenger.getVorgaenger());
				}
			}
		}

		return knoten.get(knoten.size() - 1);
	}
}
