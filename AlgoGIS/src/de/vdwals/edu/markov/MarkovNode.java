/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.markov;

import java.util.ArrayList;

/**
 * Klasse zum Speichern eines Knotens der Markovkette.
 *
 * @author Dennis van der Wals
 *
 */
public class MarkovNode {
	private char letter;
	private double distance;
	private MarkovNode vorgaenger;
	private ArrayList<MarkovEdge> kantenNachfolger, kantenVorgaenger;

	/**
	 * Standardkonstruktor
	 * 
	 * @param c
	 *            der repraesentierte Buchstabe.
	 */
	public MarkovNode(char c) {
		this.letter = c;
		this.kantenNachfolger = new ArrayList<>();
		this.kantenVorgaenger = new ArrayList<>();
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return this.distance;
	}

	/**
	 * @return the kanten
	 */
	public ArrayList<MarkovEdge> getKantenNachfolger() {
		return this.kantenNachfolger;
	}

	/**
	 * @return the kantenVorgaenger
	 */
	public ArrayList<MarkovEdge> getKantenVorgaenger() {
		return this.kantenVorgaenger;
	}

	/**
	 * @return the letter
	 */
	public char getLetter() {
		return this.letter;
	}

	/**
	 * @return the vorgaenger
	 */
	public MarkovNode getVorgaenger() {
		return this.vorgaenger;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}

	/**
	 * @param kanten
	 *            the kanten to set
	 */
	public void setKanten(ArrayList<MarkovEdge> kanten) {
		this.kantenNachfolger = kanten;
	}

	/**
	 * @param vorgaenger
	 *            the vorgaenger to set
	 */
	public void setVorgaenger(MarkovNode vorgaenger) {
		this.vorgaenger = vorgaenger;
	}

}
