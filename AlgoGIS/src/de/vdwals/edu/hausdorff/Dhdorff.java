/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.edu.hausdorff;

import java.awt.Point;
import java.util.LinkedList;

/**
 *
 * @author Dennis van der Wals
 *
 */
public class Dhdorff {

	/**
	 * Gibt den Kleinsten Abstand eines Punktes zu einer Punktemenge zur�ck
	 * 
	 * @param p
	 *            Punkt
	 * @param Q
	 *            Punktemenge
	 * @return kleinster Abstand
	 */
	public static double d(Point p, LinkedList<Point> Q) {
		double min = Double.MAX_VALUE;
		for (Point q : Q) {
			double d = d(p, q);
			if (d < min) {
				min = d;
			}
		}
		System.out.println("d(" + p.toString() + ") = " + min);
		return min;
	}

	/**
	 * Gibt den Abstand zweier Punkte zur�ck.
	 * 
	 * @param p
	 *            Punkt 1
	 * @param q
	 *            Punkt 2
	 * @return Abstand
	 */
	public static double d(Point p, Point q) {
		double d = Point.distance(p.x, p.y, q.x, q.y);
		System.out.println("d(" + p.toString() + ", " + q.toString() + ") = " + d);
		return d;
	}

	/**
	 * Gibt den gr��ten Abstand zweier Punktemengen voneinander zur�ck. Dieser
	 * bezeichnet den gr��ten der kleinsten Punkteabst�nde zueinander.
	 * 
	 * @param P
	 *            Punktemenge 1
	 * @param Q
	 *            Punktemenge 2
	 * @return gr��te der kleinsten Punkteabst�nde.
	 */
	public static double dhdorff(LinkedList<Point> P, LinkedList<Point> Q) {
		double max1 = dhdorffGerichtet(P, Q);
		double max2 = dhdorffGerichtet(Q, P);

		System.out.println("dhdorffG(P,Q) = " + max1);
		System.out.println("dhdorffG(Q,P) = " + max2);
		return (max1 > max2 ? max1 : max2);
	}

	/**
	 * Gibt den gr��ten Abstand von Punktemenge 1 zu Punktemenge 2 zur�ck.
	 * Dieser bezeichnet den gr��ten der kleinsten Punkteabst�nde zueinander.
	 * 
	 * @param P
	 *            Punktemenge 1
	 * @param Q
	 *            Punktemenge 2
	 * @return gr��te der kleinsten Punkteabst�nde.
	 */
	public static double dhdorffGerichtet(LinkedList<Point> P, LinkedList<Point> Q) {
		double max = Double.MIN_VALUE;
		for (Point p : P) {
			double d = d(p, Q);
			if (d > max) {
				max = d;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		LinkedList<Point> P = new LinkedList<>();
		P.add(new Point(1, 2));
		P.add(new Point(5, 3));
		P.add(new Point(1, 4));

		LinkedList<Point> Q = new LinkedList<>();
		Q.add(new Point(2, 1));
		Q.add(new Point(3, 5));
		Q.add(new Point(4, 1));

		System.out.println("dhdorff(P, Q) = " + dhdorff(P, Q));
	}

}
