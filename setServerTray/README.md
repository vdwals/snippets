setServerTray
=========

When developing with different Server-Environments, I created this tray-icon-application to switch between different environments fast. It is very specific to a project in the past, but still can used as example if I need similar functionality in the future, therefore it's stored as snippet.
