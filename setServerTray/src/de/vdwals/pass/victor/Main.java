/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.pass.victor;

import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Desktop;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;

import de.vdwals.pass.victor.model.Settings;
import de.vdwals.pass.victor.view.SettingsGUI;

public class Main {
	private static Main INSTANCE;

	public static Main getInstance() {
		return INSTANCE;
	}

	public static void main(String[] args) {
		Main.INSTANCE = new Main();
	}

	public static void openWebpage(String uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if ((desktop != null) && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(new URI(uri));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/* GUI Elements */
	private CheckboxMenuItem[] boxes;
	private MenuItem settings, sec, exit;

	private Menu logins;

	private ActionListener actions;

	private HashMap<CheckboxMenuItem, String[]> itemLoginMap;
	private PopupMenu popup;
	private int activeSettingIndex = 0;

	private SettingsGUI gui;

	private List<ServerSetting> lstServerSettings;

	private HashMap<String, Process> activeProcesses;

	private File ppl, guvnor, serverSettings;

	private Main() {
		// Check the SystemTray is supported
		if (!SystemTray.isSupported()) {
			System.out.println("SystemTray is not supported");
			return;
		}

		init();

		this.popup = new PopupMenu();

		BufferedImage frame = null;
		try {
			frame = ImageIO.read(Main.class.getResource("images/Save16.gif"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		final TrayIcon trayIcon = new TrayIcon(frame);
		final SystemTray tray = SystemTray.getSystemTray();

		// Add components to pop-up menu
		setServers();
		setServerSettings();

		trayIcon.setPopupMenu(this.popup);

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("TrayIcon could not be added.");
		}
	}

	private void createCheckBoxGroup(CheckboxMenuItem[] checkboxGroup) {
		ItemListener il = arg0 -> {
			for (CheckboxMenuItem checkboxMenuItem : checkboxGroup) {
				checkboxMenuItem.setState(false);
			}
			((CheckboxMenuItem) arg0.getSource()).setState(true);

			String serverSetting = ((CheckboxMenuItem) arg0.getSource()).getLabel();

			setServerSettings(getSetting(serverSetting));
		};

		for (CheckboxMenuItem checkboxMenuItem : checkboxGroup) {
			checkboxMenuItem.addItemListener(il);
		}
	}

	/**
	 * @return the actions
	 */
	public ActionListener getActions() {
		if (this.actions == null) {
			this.actions = arg0 -> {
				if (arg0.getSource() == getSettings()) {
					Main.this.gui.getWindow().setVisible(true);

				} else if (arg0.getSource() == getSec()) {
					openWebpage(getSetting(getSelectedBox()).getSecURL());

				} else if (arg0.getSource() == getExit()) {
					if (Main.this.activeProcesses != null) {
						for (String name : Main.this.activeProcesses.keySet()) {
							Main.this.activeProcesses.get(name).destroy();
						}
					}

					System.exit(0);

				}
			};
		}
		return this.actions;
	}

	/**
	 * @return the exit
	 */
	private MenuItem getExit() {
		if (this.exit == null) {
			this.exit = new MenuItem("Exit");
			this.exit.addActionListener(getActions());
		}
		return this.exit;
	}

	/**
	 * @return the logins
	 */
	private Menu getLogins() {
		if (this.logins == null) {
			this.logins = new Menu("Logins");
		}
		return this.logins;
	}

	/**
	 * @return the sec
	 */
	private MenuItem getSec() {
		if (this.sec == null) {
			this.sec = new MenuItem("Security");
			this.sec.addActionListener(getActions());
		}
		return this.sec;
	}

	private String getSelectedBox() {
		for (CheckboxMenuItem checkboxMenuItem : this.boxes) {
			if (checkboxMenuItem.getState())
				return checkboxMenuItem.getLabel();
		}
		return "";
	}

	/**
	 * @return the gillerBoxes
	 */
	private CheckboxMenuItem[] getServerBoxes() {
		if (this.boxes == null) {
			this.boxes = new CheckboxMenuItem[this.lstServerSettings.size()];

			for (int i = 0; i < this.boxes.length; i++) {
				this.boxes[i] = new CheckboxMenuItem(this.lstServerSettings.get(i).getTitel());
			}

			createCheckBoxGroup(this.boxes);

			ItemListener il = arg0 -> {
				String serverSetting = ((CheckboxMenuItem) arg0.getSource()).getLabel();

				setServerSettings(getSetting(serverSetting));
			};

			for (CheckboxMenuItem checkboxMenuItem : this.boxes) {
				checkboxMenuItem.addItemListener(il);
			}

		}
		return this.boxes;
	}

	private ServerSetting getSetting(String titel) {
		for (int i = 0; i < getServerBoxes().length; i++) {
			if (getServerBoxes()[i].getLabel().equals(titel))
				return this.lstServerSettings.get(i);
		}
		return null;
	}

	/**
	 * @return the settings
	 */
	private MenuItem getSettings() {
		if (this.settings == null) {
			this.settings = new MenuItem("Settings");
			this.settings.addActionListener(getActions());
		}
		return this.settings;
	}

	private void init() {
		this.activeProcesses = new HashMap<>();

		File[] lastFiles = Settings.getLastFiles();
		this.ppl = lastFiles[0];
		this.guvnor = lastFiles[1];
		this.serverSettings = lastFiles[2];

		if ((this.serverSettings != null) && this.serverSettings.exists()) {
			this.lstServerSettings = ServerSetting.getServerSettings(this.serverSettings);
		} else {
			this.lstServerSettings = new LinkedList<>();
		}

		this.gui = new SettingsGUI(this.lstServerSettings);
		this.gui.getPathPPL().setText(this.ppl.getAbsolutePath());
		this.gui.getPathGuvnor().setText(this.guvnor.getAbsolutePath());
		this.gui.getPathServerSettings().setText(this.serverSettings.getAbsolutePath());
	}

	private void runScriptAction(String command) throws IOException {
		runScriptAction(command, "flug", "a");
	}

	private void runScriptAction(String command, String name, String hotkey) throws IOException {
		File script = new File(name + ".ahk");
		script.createNewFile();

		BufferedWriter bw = new BufferedWriter(new FileWriter(script));
		bw.write("^!" + hotkey + "::");
		bw.newLine();
		bw.write("Send " + command);
		bw.newLine();
		bw.write("return");
		bw.close();

		if ((this.activeProcesses != null) && this.activeProcesses.containsKey(name)) {
			this.activeProcesses.get(name).destroy();
		}

		this.activeProcesses.put(name, Runtime.getRuntime().exec("autohotkey.exe " + name + ".ahk"));
	}

	private void runScriptAction(String[] login) throws IOException {
		runScriptAction(login[0] + "{Tab}" + login[1].replaceAll("!", "{!}") + "{Enter}", "login", "s");
	}

	/**
	 * @param guvnor
	 *            the guvnor to set
	 */
	public void setGuvnor(File guvnor) {
		Settings.setLastFolder(this.ppl, guvnor, this.serverSettings);
		this.guvnor = guvnor;
	}

	private void setLoginButtons(ServerSetting setting) {
		getLogins().removeAll();
		this.itemLoginMap = new HashMap<>();

		boolean neu = true;

		CheckboxMenuItem[] items = new CheckboxMenuItem[setting.getLogins().size()];
		int index = 0;

		for (String[] login : setting.getLogins()) {
			CheckboxMenuItem user = new CheckboxMenuItem(login[0]);
			this.itemLoginMap.put(user, login);

			items[index++] = user;
			user.addActionListener(getActions());
			getLogins().add(user);

			if (neu) {
				try {
					runScriptAction(login);
					neu = false;
					user.setState(true);
				} catch (IOException e) {
				}
			}
		}

		ItemListener il = arg0 -> {
			try {
				runScriptAction(Main.this.itemLoginMap.get(arg0.getSource()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		};

		for (CheckboxMenuItem checkboxMenuItem : items) {
			checkboxMenuItem.addItemListener(il);
		}

		createCheckBoxGroup(items);
	}

	/**
	 * @param ppl
	 *            the ppl to set
	 */
	public void setPpl(File ppl) {
		Settings.setLastFolder(ppl, this.guvnor, this.serverSettings);
		this.ppl = ppl;
	}

	public void setServers() {
		for (int i = 0; i < getServerBoxes().length; i++) {
			if (getServerBoxes()[i].getState()) {
				this.activeSettingIndex = i;
				break;
			}
		}

		this.popup.removeAll();

		// Add components to pop-up menu
		this.popup.add(getSettings());
		this.popup.add(getSec());
		this.popup.addSeparator();
		for (CheckboxMenuItem checkboxMenuItem : getServerBoxes()) {
			this.popup.add(checkboxMenuItem);
		}
		this.popup.addSeparator();
		this.popup.add(getLogins());
		this.popup.add(getExit());
	}

	private void setServerSettings() {
		if ((this.lstServerSettings.size() > this.activeSettingIndex) && (this.activeSettingIndex >= 0)
				&& (getServerBoxes().length > this.activeSettingIndex)) {
			setServerSettings(this.lstServerSettings.get(this.activeSettingIndex));

			for (int i = 0; i < getServerBoxes().length; i++) {
				getServerBoxes()[i].setState(false);
			}
			getServerBoxes()[this.activeSettingIndex].setState(true);
		}
	}

	/**
	 * @param ppl
	 *            the ppl to set
	 */
	public void setServerSettings(File serverSettings) {
		Settings.setLastFolder(this.ppl, this.guvnor, serverSettings);
		this.serverSettings = serverSettings;

		this.lstServerSettings = ServerSetting.getServerSettings(serverSettings);
	}

	private void setServerSettings(ServerSetting setting) {
		if ((this.ppl == null) || (this.guvnor == null) || (setting == null))
			return;

		setLoginButtons(setting);
		try {
			runScriptAction(setting.getAhCommand());
		} catch (IOException e1) {
		}

		String[] patterns = new String[4];
		patterns[0] = ServerSetting.PREFIX_DB;
		patterns[1] = ServerSetting.PREFIX_OID;
		patterns[2] = ServerSetting.PREFIX_MASCHINE;
		patterns[3] = ServerSetting.PREFIX_URL;

		try {
			Scanner scan = new Scanner(this.ppl);

			// Clear File
			StringBuilder file = new StringBuilder();
			while (scan.hasNextLine()) {
				String line = scan.nextLine();

				boolean lineContainsPattern = false;
				for (int i = 0; (i < (patterns.length - 1)) && !lineContainsPattern; i++) {
					lineContainsPattern = line.contains(patterns[i]);
				}

				if (!lineContainsPattern) {
					file.append(line + System.lineSeparator());
				}
			}

			// Attach Settings
			file.append(ServerSetting.PREFIX_DB + "=" + setting.getDB() + System.lineSeparator());
			file.append(ServerSetting.PREFIX_OID + "=" + setting.getOID() + System.lineSeparator());
			file.append(ServerSetting.PREFIX_MASCHINE + "=" + setting.getMaschine() + System.lineSeparator());

			BufferedWriter bw = new BufferedWriter(new FileWriter(this.ppl));
			bw.write(file.toString());
			bw.close();
			scan.close();

			scan = new Scanner(this.guvnor);

			// Clear File
			file = new StringBuilder();
			while (scan.hasNextLine()) {
				String line = scan.nextLine();

				if (!line.contains(patterns[3])) {
					file.append(line + System.lineSeparator());
				}
			}

			file.append(ServerSetting.PREFIX_URL + "=" + setting.getURL() + System.lineSeparator());

			bw = new BufferedWriter(new FileWriter(this.guvnor));
			bw.write(file.toString());
			bw.close();
			scan.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void storeSettings() {
		ServerSetting.storeServerSettings(this.serverSettings, this.lstServerSettings);

		setServers();

		setServerSettings();
	}

}
