/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.pass.victor.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import de.vdwals.pass.victor.Main;
import de.vdwals.pass.victor.ServerSetting;

public class SettingsGUI {
	private JTextField titel, DB, OID, Maschine, URL, secURL, ahCommand;
	private JButton delete, add, close;
	private JTree settingsTree;
	private JFrame window;

	private ServerSetting activeSettings = null;
	private String activeLogin = null;

	private JTextField pathPPL, pathGuvnor, pathServerSettings;
	private JButton selectPPL, selectGuvnor, selectServerSettings;
	private ActionListener buttonActions;
	private TreeSelectionListener treeAction;

	public SettingsGUI(List<ServerSetting> lstServerSettings) {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();

		if (lstServerSettings != null) {
			for (ServerSetting serverSetting : lstServerSettings) {
				DefaultMutableTreeNode settingNode = new DefaultMutableTreeNode(serverSetting);
				root.add(settingNode);

				for (String[] string : serverSetting.getLogins()) {
					settingNode.add(new DefaultMutableTreeNode(string[0]));
				}
			}
		}

		this.settingsTree = new JTree(root);
		this.settingsTree.addTreeSelectionListener(getTreeAction());

		this.window = new JFrame("Server Settings");
		this.window.setLayout(new BorderLayout(5, 5));

		JPanel left = new JPanel(new BorderLayout(5, 5));
		this.window.add(left, BorderLayout.WEST);

		JScrollPane tree = new JScrollPane(this.settingsTree, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		left.add(tree, BorderLayout.CENTER);
		JPanel buttons = new JPanel(new GridLayout(1, 0, 5, 5));
		left.add(buttons, BorderLayout.SOUTH);

		buttons.add(getAdd());
		buttons.add(getDelete());

		JPanel right = new JPanel(new BorderLayout(5, 5));
		this.window.add(right, BorderLayout.CENTER);

		JPanel settings = new JPanel(new BorderLayout(5, 5));
		right.add(settings, BorderLayout.NORTH);

		JPanel beschreibung = new JPanel(new GridLayout(0, 1, 5, 5));
		settings.add(beschreibung, BorderLayout.WEST);
		JPanel felder = new JPanel(new GridLayout(0, 1, 5, 5));
		settings.add(felder, BorderLayout.CENTER);

		beschreibung.add(new JLabel("Titel:"));
		felder.add(getTitel());

		beschreibung.add(new JLabel("DB:"));
		felder.add(getPanel(getDB(), ServerSetting.PREFIX_DB));

		beschreibung.add(new JLabel("OID:"));
		felder.add(getPanel(getOID(), ServerSetting.PREFIX_OID));

		beschreibung.add(new JLabel("Machine:"));
		felder.add(getPanel(getMaschine(), ServerSetting.PREFIX_MASCHINE));

		beschreibung.add(new JLabel("URL:"));
		felder.add(getPanel(getURL(), ServerSetting.PREFIX_URL));

		beschreibung.add(new JLabel("Security-Component-Link:"));
		felder.add(getSecURL());

		beschreibung.add(new JLabel("STRG + ALT + A:"));
		felder.add(getPanel(getAhCommand(), "Send"));
		beschreibung.add(new JLabel(""));
		felder.add(new JLabel("return"));

		JPanel bottom = new JPanel(new BorderLayout(5, 5));
		right.add(bottom, BorderLayout.SOUTH);

		bottom.add(getClose(), BorderLayout.EAST);

		JPanel main = new JPanel(new GridLayout(3, 1, 20, 20));
		this.window.add(main, BorderLayout.NORTH);

		JPanel feld1 = new JPanel(new BorderLayout(5, 5));
		main.add(feld1);
		feld1.add(getPathPPL(), BorderLayout.CENTER);
		feld1.add(getSelectPPL(), BorderLayout.EAST);

		JPanel feld2 = new JPanel(new BorderLayout(5, 5));
		main.add(feld2);
		feld2.add(getPathGuvnor(), BorderLayout.CENTER);
		feld2.add(getSelectGuvnor(), BorderLayout.EAST);

		JPanel feld3 = new JPanel(new BorderLayout(5, 5));
		main.add(feld3);
		feld3.add(getPathServerSettings(), BorderLayout.CENTER);
		feld3.add(getSelectServerSettings(), BorderLayout.EAST);

		this.window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.window.pack();
	}

	private void addLogin() {
		JPanel input = new JPanel(new BorderLayout(5, 5));
		JPanel labels = new JPanel(new GridLayout(2, 0));
		input.add(labels, BorderLayout.WEST);

		JPanel textfields = new JPanel(new GridLayout(2, 0));
		input.add(textfields, BorderLayout.EAST);

		JTextField name = new JTextField(20);
		JPasswordField passwort = new JPasswordField();
		textfields.add(name);
		textfields.add(passwort);

		labels.add(new JLabel("Name: "));
		labels.add(new JLabel("Passwort: "));

		Object[] options = { "Ok", "Cancel" };
		int result = JOptionPane.showOptionDialog(this.window, input, "Add new login", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

		if (result == 0) {
			String[] login = new String[2];
			login[0] = name.getText();
			login[1] = new String(passwort.getPassword());
			this.activeSettings.getLogins().add(login);

			saveSettings();
		}
	}

	private void addServerSetting() {
		JPanel input = new JPanel(new BorderLayout(5, 5));
		JPanel labels = new JPanel(new GridLayout(0, 1));
		input.add(labels, BorderLayout.WEST);

		JPanel textfields = new JPanel(new GridLayout(0, 1));
		input.add(textfields, BorderLayout.EAST);

		JTextField name = new JTextField(20);
		textfields.add(name);

		labels.add(new JLabel("Name: "));

		Object[] options = { "Ok", "Cancel" };
		JOptionPane.showOptionDialog(this.window, input, "Add new Server", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
	}

	private JButton generateDefaultButton(String label) {
		JButton button = new JButton(label);
		button.addActionListener(getButtonActions());
		return button;
	}

	/**
	 * @return the add
	 */
	private JButton getAdd() {
		if (this.add == null) {
			this.add = generateDefaultButton("Add");
		}
		return this.add;
	}

	/**
	 * @return the ahCommand
	 */
	private JTextField getAhCommand() {
		if (this.ahCommand == null) {
			this.ahCommand = new JTextField(20);
		}
		return this.ahCommand;
	}

	/**
	 * @return the buttonActions
	 */
	private ActionListener getButtonActions() {
		if (this.buttonActions == null) {
			this.buttonActions = arg0 -> {
				if ((arg0.getSource() == getSelectPPL()) || (arg0.getSource() == getSelectGuvnor())
						|| (arg0.getSource() == getSelectServerSettings())) {
					JFileChooser fc = new JFileChooser();

					int state = fc.showOpenDialog(null);

					if (state == JFileChooser.APPROVE_OPTION)
						if (arg0.getSource() == getSelectPPL()) {
							getPathPPL().setText(fc.getSelectedFile().getAbsolutePath());
							Main.getInstance().setPpl(fc.getSelectedFile());
						} else if (arg0.getSource() == getSelectServerSettings()) {
							getPathServerSettings().setText(fc.getSelectedFile().getAbsolutePath());
							Main.getInstance().setServerSettings(fc.getSelectedFile());
						} else {
							getPathGuvnor().setText(fc.getSelectedFile().getAbsolutePath());
							Main.getInstance().setGuvnor(fc.getSelectedFile());
						}

				} else if (arg0.getSource() == getClose()) {
					saveSettings();
					SettingsGUI.this.window.dispose();
				}

				else if (arg0.getSource() == getAdd()) {
					if (SettingsGUI.this.activeSettings != null) {
						addLogin();
					} else if (SettingsGUI.this.activeLogin == null) {
						addServerSetting();
					}
				} else if (arg0.getSource() == getDelete()) {
					if (SettingsGUI.this.activeSettings != null) {
						// settingsTree.get
					}
				}
			};
		}
		return this.buttonActions;
	}

	/**
	 * @return the close
	 */
	private JButton getClose() {
		if (this.close == null) {
			this.close = generateDefaultButton("Close");
		}
		return this.close;
	}

	/**
	 * @return the dB
	 */
	private JTextField getDB() {
		if (this.DB == null) {
			this.DB = new JTextField(20);
		}
		return this.DB;
	}

	/**
	 * @return the delete
	 */
	private JButton getDelete() {
		if (this.delete == null) {
			this.delete = generateDefaultButton("Delete");
		}
		return this.delete;
	}

	/**
	 * @return the maschine
	 */
	private JTextField getMaschine() {
		if (this.Maschine == null) {
			this.Maschine = new JTextField(20);
		}
		return this.Maschine;
	}

	/**
	 * @return the oID
	 */
	private JTextField getOID() {
		if (this.OID == null) {
			this.OID = new JTextField(20);
		}
		return this.OID;
	}

	/**
	 * @return the dB
	 */
	private JPanel getPanel(JTextField textfield, String label) {
		JPanel panel = new JPanel(new BorderLayout(5, 5));
		panel.add(new JLabel(label), BorderLayout.WEST);
		panel.add(textfield, BorderLayout.CENTER);
		return panel;
	}

	/**
	 * @return the pathToTarget
	 */
	public JTextField getPathGuvnor() {
		if (this.pathGuvnor == null) {
			this.pathGuvnor = new JTextField(50);
		}
		return this.pathGuvnor;
	}

	/**
	 * @return the pathToSource
	 */
	public JTextField getPathPPL() {
		if (this.pathPPL == null) {
			this.pathPPL = new JTextField(50);
		}
		return this.pathPPL;
	}

	/**
	 * @return the pathServerSettings
	 */
	public JTextField getPathServerSettings() {
		if (this.pathServerSettings == null) {
			this.pathServerSettings = new JTextField(50);
		}
		return this.pathServerSettings;
	}

	/**
	 * @return the secURL
	 */
	private JTextField getSecURL() {
		if (this.secURL == null) {
			this.secURL = new JTextField(20);
		}
		return this.secURL;
	}

	/**
	 * @return the selectTarget
	 */
	private JButton getSelectGuvnor() {
		if (this.selectGuvnor == null) {
			this.selectGuvnor = generateDefaultButton("...");
		}
		return this.selectGuvnor;
	}

	/**
	 * @return the selectSource
	 */
	private JButton getSelectPPL() {
		if (this.selectPPL == null) {
			this.selectPPL = generateDefaultButton("...");
		}
		return this.selectPPL;
	}

	/**
	 * @return the selectServerSettings
	 */
	private JButton getSelectServerSettings() {
		if (this.selectServerSettings == null) {
			this.selectServerSettings = generateDefaultButton("...");
		}
		return this.selectServerSettings;
	}

	/**
	 * @return the titel
	 */
	private JTextField getTitel() {
		if (this.titel == null) {
			this.titel = new JTextField();
			this.titel.setEditable(false);
		}

		return this.titel;
	}

	/**
	 * Erzeugt den Treeselection listener
	 *
	 * @return treeAction
	 */
	private TreeSelectionListener getTreeAction() {
		if (this.treeAction == null) {
			this.treeAction = arg0 -> {
				if ((arg0.getSource() == SettingsGUI.this.settingsTree)
						&& (SettingsGUI.this.settingsTree.getSelectionPath() != null)) {

					saveSettings();

					final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) SettingsGUI.this.settingsTree
							.getSelectionPath().getLastPathComponent();

					// Pruefe, ob eine Episode markiert ist
					if (selectedNode.getUserObject() instanceof ServerSetting) {

						setSettings((ServerSetting) selectedNode.getUserObject());
						this.activeLogin = null;

					} else if (selectedNode.getUserObject() instanceof String) {

						setSettings(null);
						this.activeLogin = (String) selectedNode.getUserObject();
					}

				}
				SettingsGUI.this.settingsTree.validate();
			};
		}
		return this.treeAction;
	}

	/**
	 * @return the uRL
	 */
	private JTextField getURL() {
		if (this.URL == null) {
			this.URL = new JTextField(20);
		}
		return this.URL;
	}

	/**
	 * @return the window
	 */
	public JFrame getWindow() {
		return this.window;
	}

	private void saveSettings() {
		if (this.activeSettings != null) {
			this.activeSettings.setAhCommand(getAhCommand().getText());
			this.activeSettings.setDB(getDB().getText());
			this.activeSettings.setMaschine(getMaschine().getText());
			this.activeSettings.setOID(getOID().getText());
			this.activeSettings.setSecURL(getSecURL().getText());
			this.activeSettings.setURL(getURL().getText());

			Main.getInstance().storeSettings();
		}
	}

	private void setSettings(ServerSetting setting) {
		this.activeSettings = setting;

		if (setting == null) {
			getTitel().setText("");
			getDB().setText("");
			getOID().setText("");
			getMaschine().setText("");
			getSecURL().setText("");
			getURL().setText("");
			getAhCommand().setText("");
		} else {
			getTitel().setText(setting.getTitel());
			getDB().setText(setting.getDB());
			getOID().setText(setting.getOID());
			getMaschine().setText(setting.getMaschine());
			getSecURL().setText(setting.getSecURL());
			getURL().setText(setting.getURL());
			getAhCommand().setText(setting.getAhCommand());
		}
	}
}
