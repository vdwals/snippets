/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.vdwals.pass.victor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ServerSetting {
	public static final String PREFIX_DB = "com.pass.framework.ppl.URL",
			PREFIX_OID = "com.pass.framework.ppl.OIDProvider", PREFIX_MASCHINE = "com.pass.framework.ppl.ServiceWork",
			PREFIX_URL = "serverUrl";

	public static List<ServerSetting> getServerSettings(File f) {
		List<ServerSetting> settings = new LinkedList<>();
		try {
			Scanner scan = new Scanner(f);

			while (scan.hasNextLine()) {
				String[] line = scan.nextLine().split("\",\"");

				if (line.length == 8) {
					String name = (line[0].length() > 0) ? line[0].replace("\"", "") : "";
					String db = (line[1].length() > 0) ? line[1].replace("\"", "") : "";
					String oid = (line[2].length() > 0) ? line[2].replace("\"", "") : "";
					String maschine = (line[3].length() > 0) ? line[3].replace("\"", "") : "";
					String url = (line[4].length() > 0) ? line[4].replace("\"", "") : "";
					String secURL = (line[5].length() > 0) ? line[5].replace("\"", "") : "";
					String login = (line[6].length() > 0) ? line[6].replace("\"", "") : "";
					String command = (line[7].length() > 0) ? line[7].replace("\"", "") : "";

					settings.add(new ServerSetting(name, db, oid, maschine, url, secURL, login, command));
				}
			}
			scan.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return settings;
	}

	public static void storeServerSettings(File f, List<ServerSetting> serverSettings) {
		if ((f == null) || (serverSettings == null) || (serverSettings.size() == 0))
			return;

		try {
			f.createNewFile();
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			for (ServerSetting serverSetting : serverSettings) {
				// "msTA","jdbc\:sqlserver\://10.100.0.71\:1433;ProgramName\=;SelectMethod\=cursor;autoReconnect\=true;DatabaseName\=","HiLo_LongObject_LongDatabase","VICTOR_MS_TA","http://10.100.0.209:8080/guvnor/org.drools.guvnor.Guvnor/package","http://10.0.6.172:9091/msvictorsec","ntravelerfifteen,ntravelerfifteen","ams{Tab}stn{Tab}{+}55{Tab}8{Tab
				// 2}8{Tab 7}{Up}"
				bw.write("\"");
				bw.write(serverSetting.titel);
				bw.write("\",\"");
				bw.write(serverSetting.DB);
				bw.write("\",\"");
				bw.write(serverSetting.OID);
				bw.write("\",\"");
				bw.write(serverSetting.Maschine);
				bw.write("\",\"");
				bw.write(serverSetting.URL);
				bw.write("\",\"");
				bw.write(serverSetting.secURL);
				bw.write("\",\"");

				StringBuilder logins = new StringBuilder();
				for (String[] login : serverSetting.logins) {
					logins.append(login[0]);
					logins.append(",");
					logins.append(login[1]);
					logins.append(",");
				}
				logins.setLength(logins.length() - 1);

				bw.write(logins.toString());
				bw.write("\",\"");
				bw.write(serverSetting.ahCommand);
				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String titel, DB, OID, Maschine, URL, secURL, ahCommand;

	private List<String[]> logins;

	private ServerSetting(String name, String db, String oid, String maschine, String url, String secURL, String logins,
			String command) {
		this.titel = name;
		this.DB = db;
		this.OID = oid;
		this.Maschine = maschine;
		this.URL = url;
		this.secURL = secURL;
		this.ahCommand = command;

		this.logins = new LinkedList<>();

		String[] usersAndPws = logins.split(",");
		for (int i = 0; i < usersAndPws.length; i += 2) {
			String[] userAndPw = new String[2];

			userAndPw[0] = usersAndPws[i];
			userAndPw[1] = usersAndPws[i + 1];

			this.logins.add(userAndPw);
		}
	}

	/**
	 * @return the ahCommand
	 */
	public String getAhCommand() {
		return this.ahCommand;
	}

	/**
	 * @return the dB
	 */
	public String getDB() {
		return this.DB;
	}

	public List<String[]> getLogins() {
		return this.logins;
	}

	/**
	 * @return the maschine
	 */
	public String getMaschine() {
		return this.Maschine;
	}

	/**
	 * @return the oID
	 */
	public String getOID() {
		return this.OID;
	}

	/**
	 * @return the secURL
	 */
	public String getSecURL() {
		return this.secURL;
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return this.titel;
	}

	/**
	 * @return the uRL
	 */
	public String getURL() {
		return this.URL;
	}

	/**
	 * @param ahCommand
	 *            the ahCommand to set
	 */
	public void setAhCommand(String ahCommand) {
		this.ahCommand = ahCommand;
	}

	/**
	 * @param dB
	 *            the dB to set
	 */
	public void setDB(String dB) {
		this.DB = dB;
	}

	/**
	 * @param logins
	 *            the logins to set
	 */
	public void setLogins(List<String[]> logins) {
		this.logins = logins;
	}

	/**
	 * @param maschine
	 *            the maschine to set
	 */
	public void setMaschine(String maschine) {
		this.Maschine = maschine;
	}

	/**
	 * @param oID
	 *            the oID to set
	 */
	public void setOID(String oID) {
		this.OID = oID;
	}

	/**
	 * @param secURL
	 *            the secURL to set
	 */
	public void setSecURL(String secURL) {
		this.secURL = secURL;
	}

	/**
	 * @param uRL
	 *            the uRL to set
	 */
	public void setURL(String uRL) {
		this.URL = uRL;
	}

	@Override
	public String toString() {
		return getTitel();
	}
}
