Updater
=========

This small code-snippet is also used in many other applications from me. It reads a version file on a server to determine if the calling Application needs and update. If an update is available, it downloads the new .jar files, replaces the existing one and restarts the application.
It also showed the Update-Log if available.

All past servers are down and I haven't used it for years, but I'll keep it until I will be able to use it again.
